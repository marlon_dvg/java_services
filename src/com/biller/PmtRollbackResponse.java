package com.biller;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="PmtRollbackResponse")
@XmlAccessorType (XmlAccessType.NONE)
public class PmtRollbackResponse extends Response implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="PartnerAuthCode", required=true, type=String.class)
	private String PartnerAuthCode;

	public String getPartnerAuthCode() {
		return PartnerAuthCode;
	}

	public void setPartnerAuthCode(String partnerAuthCode) {
		PartnerAuthCode = partnerAuthCode;
	}

	public PmtRollbackResponse() {
		super();
	}

}
