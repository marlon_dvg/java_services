package com.biller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="PaidInvoices")
@XmlAccessorType (XmlAccessType.NONE)
public class PaidInvoices implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="AgreementId", required=false, type=Integer.class)
	private int AgreementId;
	
	@XmlElement(name="InvoiceId", required=true, type=String.class)
	private String InvoiceId;
	
	@XmlElement(name="PaidValue", required=true, type=BigDecimal.class)
	private BigDecimal PaidValue;
	
	@XmlElement(name="BankSrc", required=true, type=String.class)
	private String BankSrc;
	
	@XmlElement(name="BankAuthCode", required=true, type=Integer.class)
	private int BankAuthCode;
	
	@XmlElement(name="ValuesDetail", required=false, type=ValuesDetail.class)
	private ArrayList<ValuesDetail> ValuesDetail;
	
	public int getAgreementId() {
		return AgreementId;
	}
	
	public void setAgreementId(int agreementId) {
		AgreementId = agreementId;
	}
	
	public String getInvoiceId() {
		return InvoiceId;
	}
	
	public void setInvoiceId(String invoiceId) {
		InvoiceId = invoiceId;
	}

	public BigDecimal getPaidValue() {
		return PaidValue;
	}

	public void setPaidValue(BigDecimal paidValue) {
		PaidValue = paidValue;
	}

	public ArrayList<ValuesDetail> getValuesDetail() {
		return ValuesDetail;
	}

	public void setValuesDetail(ArrayList<ValuesDetail> valuesDetail) {
		ValuesDetail = valuesDetail;
	}

	public String getBankSrc() {
		return BankSrc;
	}
	
	public void setBankSrc(String bankSrc) {
		BankSrc = bankSrc;
	}
	
	public int getBankAuthCode() {
		return BankAuthCode;
	}
	
	public void setBankAuthCode(int bankAuthCode) {
		BankAuthCode = bankAuthCode;
	}

	public PaidInvoices() {
		super();
	}
	
	
}
