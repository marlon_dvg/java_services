package com.biller;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Reference")
@XmlAccessorType (XmlAccessType.NONE)
public class Reference implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="Name", required=true, type=String.class)
	private String Name;
	
	@XmlElement(name="Message", required=true, type=String.class)
	private String Message;

	public Reference() {
	}

	public String getName() {
		return Name;
	}
	
	public void setName(String name) {
		this.Name = name;
	}
	
	public String getMessage() {
		return Message;
	}
	
	public void setMessage(String message) {
		this.Message = message;
	}

}
