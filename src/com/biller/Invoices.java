package com.biller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Invoices")
@XmlAccessorType (XmlAccessType.NONE)
public class Invoices implements Serializable {

	private static final long serialVersionUID = 1L;	
	
	@XmlElement(name="InvoiceId", required=true, type=String.class)
	private String InvoiceId;
		
	@XmlElement(name="TotalValue", required=true, type=BigDecimal.class)
	private BigDecimal TotalValue;
	
	@XmlElement(name="ExpirationDate", required=true, type=Date.class)
	private Date ExpirationDate;
	
	@XmlElement(name="EndPaymentDate", required=true, type=Date.class)
	private Date EndPaymentDate;
	
	@XmlElement(name="ValuesDetail", required=false, type=ValuesDetail.class)
	private ArrayList<ValuesDetail> ValuesDetail;
	
	@XmlElement(name="AdditionalData", required=false, type=AdditionalData.class)
	private ArrayList<AdditionalData> AdditionalData;

	public String getInvoiceId() {
		return InvoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		InvoiceId = invoiceId;
	}

	public BigDecimal getTotalValue() {
		return TotalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		TotalValue = totalValue;
	}

	public Date getExpirationDate() {
		return ExpirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		ExpirationDate = expirationDate;
	}

	public Date getEndPaymentDate() {
		return EndPaymentDate;
	}

	public void setEndPaymentDate(Date endPaymentDate) {
		EndPaymentDate = endPaymentDate;
	}

	public ArrayList<ValuesDetail> getValuesDetail() {
		return ValuesDetail;
	}

	public void setValuesDetail(ArrayList<ValuesDetail> valuesDetail) {
		ValuesDetail = valuesDetail;
	}

	public ArrayList<AdditionalData> getAdditionalData() {
		return AdditionalData;
	}

	public void setAdditionalData(ArrayList<AdditionalData> additionalData) {
		AdditionalData = additionalData;
	}

	public Invoices() {
		super();
	}

}
