package com.biller;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Response")
@XmlAccessorType (XmlAccessType.NONE)
public class Response implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="Status", required=true, type=String.class)	
	private String Status;
	
	@XmlElement(name="RequestId", required=true, type=String.class)	
	private String RequestId;
	
	@XmlElement(name="Message", required=true, type=String.class)	
	private String Message;

	public Response() {
		super();
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getRequestId() {
		return RequestId;
	}

	public void setRequestId(String requestId) {
		RequestId = requestId;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

}
