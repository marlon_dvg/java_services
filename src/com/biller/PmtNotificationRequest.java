package com.biller;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="PmtNotificationRequest")
@XmlAccessorType (XmlAccessType.NONE)
public class PmtNotificationRequest  extends Request implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="PaidInvoices", required=true, type=PaidInvoices.class)
	private ArrayList<PaidInvoices> PaidInvoices;

	public ArrayList<PaidInvoices> getPaidInvoices() {
		return PaidInvoices;
	}

	public void setPaidInvoices(ArrayList<PaidInvoices> paidInvoices) {
		PaidInvoices = paidInvoices;
	}

	public PmtNotificationRequest() {
		super();
	}

}
