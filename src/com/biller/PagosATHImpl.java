package com.biller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(name = "onlinebilling", portName="OnlinebillingPort", serviceName="OnlinebillingService",targetNamespace="http://biller.com/onlinebilling")
public class PagosATHImpl implements IPagosATH {

	@Override
	@WebMethod(operationName="getBill", action="http://biller.com/onlinebilling/getBill")
	@WebResult(name="BillResponse")
	public BillResponse getBill(@WebParam(name="BillRequest") BillRequest  BillRequest) {		
		PagosATH pagosATH = new PagosATH();
		return pagosATH.getBill(BillRequest);
	}

	@Override
	@WebMethod(operationName="sendPmtNotification", action="http://biller.com/onlinebilling/sendPmtNotification")
	@WebResult(name="PmtNotificationResponse")
	public PmtNotificationResponse sendPmtNotification(@WebParam(name="PmtNotificationRequest") PmtNotificationRequest PmtNotificationRequest) {
	    
		PmtNotificationResponse res = new PmtNotificationResponse();
		PagosATH pagosATH = new PagosATH();
		
		if(PmtNotificationRequest.getPaidInvoices().isEmpty()){
			
			res.setStatus("82");
			res.setMessage("Factura no existe");
			res.setRequestId("RequestId");
			
			return res;
		}
		
		for(PaidInvoices p : PmtNotificationRequest.getPaidInvoices()){			
			res = pagosATH.sendPmtNotification(PmtNotificationRequest.getRequestId(), p.getInvoiceId(), 
					p.getAgreementId(), PmtNotificationRequest.getInqDate(), PmtNotificationRequest.getCurrentDatetime(), 
					p.getPaidValue(), p.getBankSrc(), p.getBankAuthCode());
		}		
		
		return res;
	}

	@Override
	@WebMethod(operationName="sendPmtRollback", action="http://biller.com/onlinebilling/sendPmtRollback")
	@WebResult(name="PmtRollbackResponse")
	public PmtRollbackResponse sendPmtRollback(@WebParam(name="PmtRollbackRequest")PmtRollbackRequest PmtRollbackRequest) {
		
		PmtRollbackResponse res = new PmtRollbackResponse(); 
		PagosATH pagosATH = new PagosATH();
		
		if(PmtRollbackRequest.getPaidInvoices().isEmpty()){
			
			res.setStatus("1");
			res.setMessage("Error al reversar");
			res.setRequestId("RequestId");
			
			return res;
		}
		
		for(PaidInvoices p : PmtRollbackRequest.getPaidInvoices()){			
			res = pagosATH.sendPmtRollback(PmtRollbackRequest.getRequestId(), p.getInvoiceId(), p.getAgreementId(), PmtRollbackRequest.getInqDate(), PmtRollbackRequest.getCurrentDatetime(), p.getPaidValue(), p.getBankSrc(), p.getBankAuthCode());
		}	
		
		return res;
	}

}
