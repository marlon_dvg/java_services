@XmlSchema(
	namespace = "http://biller.com/onlinebilling",
	elementFormDefault = javax.xml.bind.annotation.XmlNsForm.UNQUALIFIED,
    xmlns = { 
    		@XmlNs(prefix = "wsdl", namespaceURI="http://schemas.xmlsoap.org/wsdl/"),
            @XmlNs(prefix = "bons0", namespaceURI="http://facturador.com/onlinebilling"),
    		@XmlNs(prefix = "soap", namespaceURI="http://schemas.xmlsoap.org/wsdl/soap/"),
    		@XmlNs(prefix = "tns", namespaceURI="http://biller.com/onlinebilling"),
            @XmlNs(prefix = "xsd", namespaceURI="http://www.w3.org/2001/XMLSchema")   		
    }
)
package com.biller;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;