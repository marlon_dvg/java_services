package com.biller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(name = "onlinebilling", portName="OnlinebillingPort", serviceName="OnlinebillingService",targetNamespace="http://biller.com/onlinebilling")
public interface IPagosATH {	
	@WebMethod(operationName="getBill", action="http://biller.com/onlinebilling/getBill")
	@WebResult(name="BillResponse")
	public BillResponse getBill(@WebParam(name="BillRequest") BillRequest  BillRequest) ;
	
	@WebMethod(operationName="sendPmtNotification", action="http://biller.com/onlinebilling/sendPmtNotification")
	@WebResult(name="PmtNotificationResponse")
	public PmtNotificationResponse sendPmtNotification(@WebParam(name="PmtNotificationRequest") PmtNotificationRequest  PmtNotificationRequest) ;
	
	@WebMethod(operationName="sendPmtRollback", action="http://biller.com/onlinebilling/sendPmtRollback")
	@WebResult(name="PmtRollbackResponse")
	public PmtRollbackResponse sendPmtRollback(@WebParam(name="PmtRollbackRequest") PmtRollbackRequest  PmtRollbackRequest);		
}
