package com.biller;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="ValuesDetail")
@XmlAccessorType (XmlAccessType.NONE)
public class ValuesDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="Description", required=true, type=String.class)
	private String Description;
	
	@XmlElement(name="Value", required=true, type=BigDecimal.class)
	private BigDecimal Value;
	
	@XmlElement(name="Class", required=false, type=String.class)
	private String Class;

	public String getClass1() {
		return Class;
	}

	public void setClass1(String class1) {
		this.Class = class1;
	}

	public String getDescription() {
		return Description;
	}
	
	public void setDescription(String description) {
		Description = description;
	}
	
	public BigDecimal getValue() {
		return Value;
	}
	
	public void setValue(BigDecimal value) {
		Value = value;
	}

	public ValuesDetail() {
		super();
	}

}
