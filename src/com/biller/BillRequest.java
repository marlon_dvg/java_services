package com.biller;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="BillRequest")
@XmlAccessorType (XmlAccessType.NONE)
public class BillRequest extends Request implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="SearchType", required=false, type=Integer.class)
	private int SearchType;
	
	@XmlElement(name="InvoiceId", required=true, type=String.class)
	private String InvoiceId;
	
	@XmlElement(name="AgreementId", required=false, type=Integer.class)
	private int AgreementId;
	
	@XmlElement(name="InqPeriod", required=false, type=String.class)
	private String InqPeriod;
	
	@XmlElement(name="Reference", required=false, type=Reference.class)
	private ArrayList<Reference> Reference;	
	
	public int getSearchType() {
		return SearchType;
	}

	public void setSearchType(int searchType) {
		SearchType = searchType;
	}

	public String getInvoiceId() {
		return InvoiceId;
	}

	public void setInvoiceId(String InvoiceId) {
		this.InvoiceId = InvoiceId;
	}

	public int getAgreementId() {
		return AgreementId;
	}

	public void setAgreementId(int agreementId) {
		AgreementId = agreementId;
	}

	public String getInqPeriod() {
		return InqPeriod;
	}

	public void setInqPeriod(String inqPeriod) {
		InqPeriod = inqPeriod;
	}

	public ArrayList<Reference> getReference() {
		return Reference;
	}

	public void setReference(ArrayList<Reference> reference) {
		Reference = reference;
	}

	public BillRequest() {
		super();
	}

}
