package com.biller;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="AdditionalData")
@XmlAccessorType (XmlAccessType.NONE)
public class AdditionalData implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="Name", required=true, type=String.class)
	public String Name;
	
	@XmlElement(name="Message", required=true, type=String.class)
	public String Message;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public AdditionalData() {
		super();
	}

}
