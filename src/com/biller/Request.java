package com.biller;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Request")
@XmlAccessorType (XmlAccessType.NONE)
public class Request implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="RequestId", required=true, type=String.class)
	private String RequestId;
	
	@XmlElement(name="CurrentDatetime", required=false, type=Date.class)
	private Date CurrentDatetime;
	
	@XmlElement(name="InqDate", required=true, type=Date.class)
	private Date InqDate;

	public String getRequestId() {
		return RequestId;
	}

	public void setRequestId(String requestId) {
		RequestId = requestId;
	}

	public Date getCurrentDatetime() {
		return CurrentDatetime;
	}

	public void setCurrentDatetime(Date currentDatetime) {
		CurrentDatetime = currentDatetime;
	}

	public Date getInqDate() {
		return InqDate;
	}

	public void setInqDate(Date inqDate) {
		InqDate = inqDate;
	}

	public Request() {
		super();
	}

}
