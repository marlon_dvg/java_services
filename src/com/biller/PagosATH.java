package com.biller;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;

import javax.naming.InitialContext;
import com.sun.appserv.jdbc.DataSource;

public class PagosATH {

	public BillResponse getBill(BillRequest BillRequest) {
		
		Connection connection = null;
		CallableStatement callableStatement = null;
		InitialContext ic = null;
		DataSource dataSource = null;
		ResultSet resultSet = null;
		BillResponse BillResponse = null;
		
		try {
			
			ic = new InitialContext();
			dataSource = (DataSource) ic.lookup("SQLServer2005_raptor");			
			connection = dataSource.getConnection();

			callableStatement = connection.prepareCall("EXECUTE ws.Proc_Get_Bill_Pagos_Ath2 ?, ?, ?, ?, ?, ?, ?");
			callableStatement.setString("requestId",  BillRequest.getRequestId());
			callableStatement.setLong("searchType", BillRequest.getSearchType());
			callableStatement.setString("invoiceId", BillRequest.getInvoiceId());
			callableStatement.setInt("agreementId", BillRequest.getAgreementId());
			callableStatement.setString("inqDate", BillRequest.getInqDate().toString());
			callableStatement.setString("currentDatetime", BillRequest.getCurrentDatetime().toString());
			callableStatement.setString("inqPeriod", BillRequest.getInqPeriod());

			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				
				BillResponse = new BillResponse();
				
				BillResponse.setRequestId(resultSet.getString("REQUESTID"));
				BillResponse.setMessage(resultSet.getString("MENSAJE"));
				int status = Integer.parseInt(resultSet.getString("STATUS"));
				
				BillResponse.setStatus( String.valueOf( status) );

				if( status == 0 || status == 83 ){					
					Date fecha_vencimiento = new Date();
					fecha_vencimiento = resultSet.getDate("FECHA_VENCIMIENTO");
					
					ArrayList<Invoices> invoices = new ArrayList<Invoices>();
					Invoices InvoicesItem = new Invoices();
					InvoicesItem.setInvoiceId(resultSet.getString("NUMERO_FACTURA"));
					InvoicesItem.setTotalValue(resultSet.getBigDecimal("VALOR_TOTAL"));
					InvoicesItem.setExpirationDate(fecha_vencimiento);
					InvoicesItem.setEndPaymentDate(fecha_vencimiento);

					invoices.add(InvoicesItem);
					BillResponse.setInvoices(invoices);
				}			
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (resultSet != null)
					resultSet.close();
			} catch (Exception exception2) {
			}

			try {
				if (callableStatement != null)
					callableStatement.close();
			} catch (Exception exception2) {

			}

			try {
				if (connection != null)
					connection.close();
			} catch (Exception e) {
			}

		}

		return BillResponse;
	}

	public PmtNotificationResponse sendPmtNotification(String requestId, String invoiceId, int agreementId, Date inqDate,
			Date currentDatetime, BigDecimal paidValue, String bankSrc, int bankAuthCode){

		Connection connection = null;
		CallableStatement callableStatement = null;
		InitialContext ic = null;
		DataSource dataSource = null;
		ResultSet resultSet = null;

		PmtNotificationResponse res = null;

		try {
			ic = new InitialContext();
			dataSource = (DataSource)ic.lookup("SQLServer2005_raptor");			
			connection = dataSource.getConnection();

			callableStatement = connection.prepareCall("EXECUTE ws.Proc_Guardar_Pago_Ath ?,?,?,?,?,?,?,?,?,?,? ");

			callableStatement.setString("requestId", requestId);
			callableStatement.setString("invoiceId", invoiceId);
			callableStatement.setInt("agreementId", agreementId);
			callableStatement.setString("inqDate", inqDate.toString());
			callableStatement.setString("currentDatetime", currentDatetime.toString());
			callableStatement.setBigDecimal("paidValue", paidValue);
			callableStatement.setString("bankSrc", bankSrc);
			callableStatement.setInt("bankAuthCode", bankAuthCode);

			callableStatement.registerOutParameter("status", Types.VARCHAR);
			callableStatement.registerOutParameter("mensaje", Types.VARCHAR);
			callableStatement.registerOutParameter("partnerAuthCode", Types.VARCHAR);

			callableStatement.execute();
			
			String status = callableStatement.getString("status");

			res = new PmtNotificationResponse();
			
			System.out.println("res "+status);
			if(status!=null && !status.equals("")) {
				res.setRequestId(requestId);
				res.setStatus(status);
				res.setMessage(callableStatement.getString("mensaje"));
				res.setPartnerAuthCode(String.valueOf(bankAuthCode));

			} else {
				res.setRequestId(requestId);
				res.setStatus("1");
				res.setMessage("Error inesperado");

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
			} catch (Exception exception2) {
			}
			try {
				if (callableStatement != null)
					callableStatement.close();
			} catch (Exception exception2) {
			}
			try {
				if (connection != null)
					connection.close();
			} catch (Exception e) {
			}
		}

		return res;

	}

	public PmtRollbackResponse sendPmtRollback(String requestId, String invoiceId, int agreementId, Date inqDate,
			Date currentDatetime, BigDecimal paidValue, String bankSrc, int bankAuthCode) {
		
		Connection connection = null;
		CallableStatement callableStatement = null;
		InitialContext ic = null;
		DataSource dataSource = null;
		ResultSet resultSet = null;
		
		PmtRollbackResponse res = null;

		try {
			ic = new InitialContext();
			dataSource = (DataSource)ic.lookup("SQLServer2005_raptor");			
			connection = dataSource.getConnection();

			callableStatement = connection.prepareCall("EXECUTE ws.Proc_Get_Reverso_Pago_Ath ?,?,?,?,?,?,?,?,?,?,? ");

			callableStatement.setString("requestId", requestId);
			callableStatement.setString("invoiceId", invoiceId);
			callableStatement.setInt("agreementId", agreementId);
			callableStatement.setString("inqDate", inqDate.toString());
			callableStatement.setString("currentDatetime", currentDatetime.toString());
			callableStatement.setBigDecimal("paidValue", paidValue);
			callableStatement.setString("bankSrc", bankSrc);
			callableStatement.setInt("bankAuthCode", bankAuthCode);

			callableStatement.registerOutParameter("status", Types.VARCHAR);
			callableStatement.registerOutParameter("mensaje", Types.VARCHAR);
			callableStatement.registerOutParameter("partnerAuthCode", Types.VARCHAR);

			callableStatement.execute();
			
			String status = callableStatement.getString("status");

			res = new PmtRollbackResponse();
			
			if(status!=null && !status.equals("")) {

				res.setRequestId(requestId);
				res.setStatus(status);
				res.setMessage(callableStatement.getString("mensaje"));
				res.setPartnerAuthCode(String.valueOf(bankAuthCode));

			} else {
				res.setRequestId(requestId);
				res.setStatus("1");
				res.setMessage("Error al reversar");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
			} catch (Exception exception2) {
			}
			try {
				if (callableStatement != null)
					callableStatement.close();
			} catch (Exception exception2) {
			}
			try {
				if (connection != null)
					connection.close();
			} catch (Exception e) {
			}
		}

		return res;
	}
}
