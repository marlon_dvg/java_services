/**
 * CargaLeadsLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package co.com.peoplecontact.webservices;

public class CargaLeadsLocator extends org.apache.axis.client.Service implements co.com.peoplecontact.webservices.CargaLeads {

    public CargaLeadsLocator() {
    }


    public CargaLeadsLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CargaLeadsLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CargaLeadsSoap
    private java.lang.String CargaLeadsSoap_address = "http://181.49.168.203/CargaLeads/CargaLeads.asmx";

    public java.lang.String getCargaLeadsSoapAddress() {
        return CargaLeadsSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CargaLeadsSoapWSDDServiceName = "CargaLeadsSoap";

    public java.lang.String getCargaLeadsSoapWSDDServiceName() {
        return CargaLeadsSoapWSDDServiceName;
    }

    public void setCargaLeadsSoapWSDDServiceName(java.lang.String name) {
        CargaLeadsSoapWSDDServiceName = name;
    }

    public co.com.peoplecontact.webservices.CargaLeadsSoap getCargaLeadsSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CargaLeadsSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCargaLeadsSoap(endpoint);
    }

    public co.com.peoplecontact.webservices.CargaLeadsSoap getCargaLeadsSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            co.com.peoplecontact.webservices.CargaLeadsSoapStub _stub = new co.com.peoplecontact.webservices.CargaLeadsSoapStub(portAddress, this);
            _stub.setPortName(getCargaLeadsSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCargaLeadsSoapEndpointAddress(java.lang.String address) {
        CargaLeadsSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (co.com.peoplecontact.webservices.CargaLeadsSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                co.com.peoplecontact.webservices.CargaLeadsSoapStub _stub = new co.com.peoplecontact.webservices.CargaLeadsSoapStub(new java.net.URL(CargaLeadsSoap_address), this);
                _stub.setPortName(getCargaLeadsSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CargaLeadsSoap".equals(inputPortName)) {
            return getCargaLeadsSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://peoplecontact.com.co/webservices/", "CargaLeads");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://peoplecontact.com.co/webservices/", "CargaLeadsSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CargaLeadsSoap".equals(portName)) {
            setCargaLeadsSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
