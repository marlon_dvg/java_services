package co.com.peoplecontact.webservices;

public class CargaLeadsSoapProxy implements co.com.peoplecontact.webservices.CargaLeadsSoap {
  private String _endpoint = null;
  private co.com.peoplecontact.webservices.CargaLeadsSoap cargaLeadsSoap = null;
  
  public CargaLeadsSoapProxy() {
    _initCargaLeadsSoapProxy();
  }
  
  public CargaLeadsSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initCargaLeadsSoapProxy();
  }
  
  private void _initCargaLeadsSoapProxy() {
    try {
      cargaLeadsSoap = (new co.com.peoplecontact.webservices.CargaLeadsLocator()).getCargaLeadsSoap();
      if (cargaLeadsSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)cargaLeadsSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)cargaLeadsSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (cargaLeadsSoap != null)
      ((javax.xml.rpc.Stub)cargaLeadsSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public co.com.peoplecontact.webservices.CargaLeadsSoap getCargaLeadsSoap() {
    if (cargaLeadsSoap == null)
      _initCargaLeadsSoapProxy();
    return cargaLeadsSoap;
  }
  
  public java.lang.String version(java.lang.String idCampana) throws java.rmi.RemoteException{
    if (cargaLeadsSoap == null)
      _initCargaLeadsSoapProxy();
    return cargaLeadsSoap.version(idCampana);
  }
  
  public int insertarRegistro(java.lang.String idCampana, java.lang.String nombre, java.lang.String telefono, java.lang.String observaciones) throws java.rmi.RemoteException{
    if (cargaLeadsSoap == null)
      _initCargaLeadsSoapProxy();
    return cargaLeadsSoap.insertarRegistro(idCampana, nombre, telefono, observaciones);
  }
  
  
}