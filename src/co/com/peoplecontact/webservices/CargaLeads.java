/**
 * CargaLeads.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package co.com.peoplecontact.webservices;

public interface CargaLeads extends javax.xml.rpc.Service {
	
    public java.lang.String getCargaLeadsSoapAddress();
    
    public co.com.peoplecontact.webservices.CargaLeadsSoap getCargaLeadsSoap() throws javax.xml.rpc.ServiceException;
    
    public co.com.peoplecontact.webservices.CargaLeadsSoap getCargaLeadsSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    
}
