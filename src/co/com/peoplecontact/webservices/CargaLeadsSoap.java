/**
 * CargaLeadsSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package co.com.peoplecontact.webservices;

public interface CargaLeadsSoap extends java.rmi.Remote {
    public java.lang.String version(java.lang.String idCampana) throws java.rmi.RemoteException;
    public int insertarRegistro(java.lang.String idCampana, java.lang.String nombre, java.lang.String telefono, java.lang.String observaciones) throws java.rmi.RemoteException;
}
