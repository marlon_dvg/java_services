/*
 * LogsManager.java
 *
 * Created on 8 de febrero de 2007, 09:09 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package co.com.util;

import java.io.File;
import java.util.Hashtable;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
 
/**
 *
 * @author  Edwin H.
 */ 
public class LogsManager {
    
    private static Logger logger = null;
    private static Hashtable hLoggers=new Hashtable();
        
    /** Creates a new instance of LogsManager */
    public LogsManager() {
    	
    }
     
     public static Logger getInstance(){
         logger = (Logger) hLoggers.get("log");
        if ( logger == null ) {
            System.out.println("Iniciando instancia del Log");
            try{
                logger = Logger.getLogger("CMI");
                //PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+log+".properties");
                
                logger = Logger.getLogger("CMIID");
               //File f = new File( "log"); 
               //System.out.println(System.getProperty("ROOT"));
               //f.mkdirs();
                
                
                File f = null;
                if (File.separator.equalsIgnoreCase("/")) {
                	f = new File("/Log_Ecommerce_Amelissa.log");
                }else{
                	f = new File("C:\\Log_Ecommerce_Amelissa.log");
                }
               
               PatternLayout pLayout = new PatternLayout(); 
               pLayout.setConversionPattern("[%-2d{dd/MM/yy HH:mm:ss}] - %-5p - %m%n"); 
               DailyRollingFileAppender appender = new DailyRollingFileAppender( 
                     pLayout, f.getAbsolutePath(), "'.'yyyy-MM-dd"); 
               appender.setName("log-cmi"); 
               logger.addAppender(appender); 
               logger.setLevel(Level.ALL); 
               
               ConsoleAppender consola=new ConsoleAppender(pLayout);
               logger.addAppender(consola);
               
               hLoggers.put("log", logger);
               
            }catch(Exception e){
                System.out.println("Error subiendo el manager del logger: "+e.toString());
            }
        }
        return logger;
    }
}