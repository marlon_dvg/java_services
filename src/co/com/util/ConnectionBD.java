package co.com.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * @author edwinh
 *
 */
public class ConnectionBD {
	
	/**
	 * Se conecta con la base de datos utilizando los parametros proporcionados en el archivo de configuración via JDNI
	 * @return Connection Conexión activa con la base de datos.
	 * @throws NamingException 
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public Connection getConnectionDB() throws NamingException, ClassNotFoundException, SQLException{        
		
        String driver;
        String url;
        String bd;
        String ip;
        String usuario;
        String clave;
        String puerto;
        
        Connection con = null;
         
        Context env = (Context)new InitialContext().lookup("java:comp/env");
        
        driver  = (String)env.lookup("driver");
        url     = (String)env.lookup("url");
        bd      = (String)env.lookup("basedatos");
        ip      = (String)env.lookup("ipserver");
        clave   = (String)env.lookup("clave");
        puerto  = (String)env.lookup("puerto");
        usuario = (String)env.lookup("usuario");
        
        String stringUrl = url + ip + ";databaseName=" + bd + ";";
		
		Class.forName(driver);
		con = DriverManager.getConnection(stringUrl, usuario, clave);
        
        return con;
    }
	
	
	public static Connection getConnectionSQLServerEcommerce() {
		
		Connection conn = null;
		
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
			String connectionURL = "jdbc:sqlserver://172.17.23.21;databaseName=ecommerce;";
			conn = DriverManager.getConnection(connectionURL,"Java","Prueba12");
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		return conn;
	}
	
	
	public static Connection getConnectionSQLServerAmelissa() {
		
		Connection conn = null;
		
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
			String connectionURL = "jdbc:sqlserver://172.17.23.21;databaseName=amelissaAux;";
			conn = DriverManager.getConnection(connectionURL,"Java","Prueba12");
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		return conn;
	}
	
	public static Connection getConnectionSQLServerAmelissaPruebas() {
		
		Connection conn = null;
		
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
			String connectionURL = "jdbc:sqlserver://172.17.23.21;databaseName=amelissaAux;";
			conn = DriverManager.getConnection(connectionURL,"Java3","Prueba12");
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		return conn;
	}
	
	public static void main(String srgs[]){
		
		System.out.println("INIT CONNECT");
		getConnectionSQLServerEcommerce();
		System.out.println("ENDING CONNECT");
		
	}
}
