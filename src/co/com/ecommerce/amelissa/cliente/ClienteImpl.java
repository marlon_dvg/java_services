package co.com.ecommerce.amelissa.cliente;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.jws.WebMethod;
import javax.jws.WebService;

import co.com.util.ConnectionBD;

@WebService(endpointInterface = "co.com.ecommerce.amelissa.cliente.Cliente", name = "Cliente", portName="ClienteImplPort", serviceName="ClienteImplService")
public class ClienteImpl implements Cliente {
	
	@Override
	@WebMethod(operationName="cargaCliente")
	public boolean cargaCliente(ClienteDTO clienteDTO) {		
		return false;
	}
	
	@Override
	@SuppressWarnings("static-access")
	@WebMethod(operationName="consultaCliente")
	public boolean consultaCliente(String id) {
		
		boolean existe = false;
		
		Connection connection = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		
		ConnectionBD conn = new ConnectionBD();
		connection = conn.getConnectionSQLServerEcommerce();
		
		try {
			callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Get_Clientprofiledata ? ");
			callableStatement.setString("id", id);
			
			resultSet = callableStatement.executeQuery();
			
			while(resultSet.next()){
				existe = true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return existe;
	}
}
