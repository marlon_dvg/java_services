package co.com.ecommerce.amelissa.cliente;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client{
	
	public static void main(String[] args) throws Exception {
		
		URL url = new URL("http://servicios.amelissa.com:8085/Services/ClienteImplService");
		
        QName qname = new QName("http://cliente.amelissa.ecommerce.com.co/", "ClienteImplService");
        
        Service service = Service.create(url, qname);
        Cliente mensaje = service.getPort(Cliente.class);
        
        ClienteDTO clienteDTO = new ClienteDTO();
        
        boolean exito = false;
        
        /*
        exito = mensaje.cargaCliente(clienteDTO);
        System.out.println(exito);
        exito = mensaje.consultaCliente("23139286");
        System.out.println(exito);
        */
        
        ClienteImpl clienteImpl = new ClienteImpl();
        System.out.println(clienteImpl.consultaCliente("1000805318"));
        
		/*
		URL url = new URL("http://dinfppaedwinh:8282/Services/HellowWorldServiceService");
		
        //QName qname = new QName("http://amelissa.ecommerce.com.co/", "HellowWorldServiceService");
		QName qname = new QName("http://amelissa.ecommerce.com.co/", "HellowWorldServiceService");
        
        Service service = Service.create(url, qname);
        
        //HellowWorldServiceService mensaje = service.getPort(HellowWorldServiceService.class);
        service.getPort(qname,"");
        */
    }
}
