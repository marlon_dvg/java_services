package co.com.ecommerce.amelissa.cliente;

public class AmelistaDTO {
	
	private String saldoAmelista;
	private String cedulaAmelista;
	private String nombreAmelista;
	private String fechaVencimento;
	private String cedulaGerenteZona;
	private String nombreGerenteZona;
	private String primerTelefonoGerenteZona;
	private String segundoTelefonoGerenteZona;
	private String primerCelularGerenteZona;
	private String segundoCelularGerenteZona;
	private String fechaMomentos;
	private String fechaCambiosDevolucuiones;
	private String tipoCedula;
	private String estadoAmelista;
	private String saldoFinanciamiento;
	private String puntaje;
	private String fechaMomentosSiguiente;
	private String tipoIngreso;
	private String estadoPedido;
	private String fechaEntregaPedido;
	private String fechaIngresoProximoPedido;
	
	public String getSaldoAmelista() {
		return saldoAmelista;
	}
	public void setSaldoAmelista(String saldoAmelista) {
		this.saldoAmelista = saldoAmelista;
	}
	public String getCedulaAmelista() {
		return cedulaAmelista;
	}
	public void setCedulaAmelista(String cedulaAmelista) {
		this.cedulaAmelista = cedulaAmelista;
	}
	public String getNombreAmelista() {
		return nombreAmelista;
	}
	public void setNombreAmelista(String nombreAmelista) {
		this.nombreAmelista = nombreAmelista;
	}
	public String getFechaVencimento() {
		return fechaVencimento;
	}
	public void setFechaVencimento(String fechaVencimento) {
		this.fechaVencimento = fechaVencimento;
	}
	public String getCedulaGerenteZona() {
		return cedulaGerenteZona;
	}
	public void setCedulaGerenteZona(String cedulaGerenteZona) {
		this.cedulaGerenteZona = cedulaGerenteZona;
	}
	public String getNombreGerenteZona() {
		return nombreGerenteZona;
	}
	public void setNombreGerenteZona(String nombreGerenteZona) {
		this.nombreGerenteZona = nombreGerenteZona;
	}
	public String getPrimerTelefonoGerenteZona() {
		return primerTelefonoGerenteZona;
	}
	public void setPrimerTelefonoGerenteZona(String primerTelefonoGerenteZona) {
		this.primerTelefonoGerenteZona = primerTelefonoGerenteZona;
	}
	public String getSegundoTelefonoGerenteZona() {
		return segundoTelefonoGerenteZona;
	}
	public void setSegundoTelefonoGerenteZona(String segundoTelefonoGerenteZona) {
		this.segundoTelefonoGerenteZona = segundoTelefonoGerenteZona;
	}
	public String getPrimerCelularGerenteZona() {
		return primerCelularGerenteZona;
	}
	public void setPrimerCelularGerenteZona(String primerCelularGerenteZona) {
		this.primerCelularGerenteZona = primerCelularGerenteZona;
	}
	public String getSegundoCelularGerenteZona() {
		return segundoCelularGerenteZona;
	}
	public void setSegundoCelularGerenteZona(String segundoCelularGerenteZona) {
		this.segundoCelularGerenteZona = segundoCelularGerenteZona;
	}
	public String getFechaMomentos() {
		return fechaMomentos;
	}
	public void setFechaMomentos(String fechaMomentos) {
		this.fechaMomentos = fechaMomentos;
	}
	public String getFechaCambiosDevolucuiones() {
		return fechaCambiosDevolucuiones;
	}
	public void setFechaCambiosDevolucuiones(String fechaCambiosDevolucuiones) {
		this.fechaCambiosDevolucuiones = fechaCambiosDevolucuiones;
	}
	public String getTipoCedula() {
		return tipoCedula;
	}
	public void setTipoCedula(String tipoCedula) {
		this.tipoCedula = tipoCedula;
	}
	public String getEstadoAmelista() {
		return estadoAmelista;
	}
	public void setEstadoAmelista(String estadoAmelista) {
		this.estadoAmelista = estadoAmelista;
	}
	public String getSaldoFinanciamiento() {
		return saldoFinanciamiento;
	}
	public void setSaldoFinanciamiento(String saldoFinanciamiento) {
		this.saldoFinanciamiento = saldoFinanciamiento;
	}
	public String getPuntaje() {
		return puntaje;
	}
	public void setPuntaje(String puntaje) {
		this.puntaje = puntaje;
	}
	public String getFechaMomentosSiguiente() {
		return fechaMomentosSiguiente;
	}
	public void setFechaMomentosSiguiente(String fechaMomentosSiguiente) {
		this.fechaMomentosSiguiente = fechaMomentosSiguiente;
	}
	public String getTipoIngreso() {
		return tipoIngreso;
	}
	public void setTipoIngreso(String tipoIngreso) {
		this.tipoIngreso = tipoIngreso;
	}
	public String getEstadoPedido() {
		return estadoPedido;
	}
	public void setEstadoPedido(String estadoPedido) {
		this.estadoPedido = estadoPedido;
	}
	public String getFechaEntregaPedido() {
		return fechaEntregaPedido;
	}
	public void setFechaEntregaPedido(String fechaEntregaPedido) {
		this.fechaEntregaPedido = fechaEntregaPedido;
	}
	public String getFechaIngresoProximoPedido() {
		return fechaIngresoProximoPedido;
	}
	public void setFechaIngresoProximoPedido(String fechaIngresoProximoPedido) {
		this.fechaIngresoProximoPedido = fechaIngresoProximoPedido;
	}
}
