package co.com.ecommerce.amelissa.cliente;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Cliente {

	@WebMethod(operationName="cargaCliente")
	@WebResult(name="cargaClienteResponse")
	public boolean cargaCliente(ClienteDTO clienteDTO);
	
	@WebMethod(operationName="consultaCliente")
	@WebResult(name="consultaClienteResponse")
	public boolean consultaCliente(String id);
	
}