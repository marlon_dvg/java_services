package co.com.ecommerce.amelissa.inventario;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client{
	
	public static void main(String[] args) throws Exception {
		/*
		URL url = new URL("http://servicios.amelissa.com:8085/Services/InventarioImplService");
        QName qname = new QName("http://inventario.amelissa.ecommerce.com.co/", "InventarioImplService");
        Service service = Service.create(url, qname);
        Inventario mensaje = service.getPort(Inventario.class);
        */
        //InventarioDTO inventarioDTO = new InventarioDTO();
		InventarioImpl mensaje = new InventarioImpl(); 
		InventarioDTO[] inventarioDTO = mensaje.actualizarInventario();
        
        for (int i = 0; i < inventarioDTO.length; i++) {
        	System.out.println("");
        	System.out.println("SKU " + inventarioDTO[i].getItemId() + " CANTIDAD " + inventarioDTO[i].getQuantity());
		}
    }
}
