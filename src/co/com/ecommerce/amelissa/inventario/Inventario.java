package co.com.ecommerce.amelissa.inventario;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Inventario{
	
	@WebMethod(operationName="actualizarInventario")
	@WebResult(name="actualizarInventarioResponse")
	public InventarioDTO[] actualizarInventario();
	
	@WebMethod(operationName="confirmarInventario")
	@WebResult(name="confirmarInventarioResponse")
	public boolean confirmarInventario(InventarioDTO[] info);
	
}