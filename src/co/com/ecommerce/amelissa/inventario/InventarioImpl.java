package co.com.ecommerce.amelissa.inventario;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import co.com.ecommerce.amelissa.facturacion.dto.OrderTrakingDTO;
import co.com.util.ConnectionBD;

@WebService(endpointInterface = "co.com.ecommerce.amelissa.inventario.Inventario", name = "Inventario", portName="InventarioImplPort", serviceName="InventarioImplService")
public class InventarioImpl implements Inventario {
	
	@Override
	@WebMethod(operationName="actualizarInventario")
	public InventarioDTO[] actualizarInventario() {
		
		Connection connection = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		
		ConnectionBD conn = new ConnectionBD();
		connection = conn.getConnectionSQLServerEcommerce();
		
		InventarioDTO[] info = null;
		
		try {
			callableStatement = connection.prepareCall("EXECUTE inventario.Proc_Carga_Inventario ",ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
			resultSet = callableStatement.executeQuery();
			
			resultSet.last();
			int row = resultSet.getRow();
			info = new InventarioDTO[row];
			resultSet.beforeFirst();
			
			int index = 0;
			while (resultSet.next()) {
				InventarioDTO inventarioDTO = new InventarioDTO();
				inventarioDTO.setItemId(resultSet.getLong("SKU"));
				inventarioDTO.setQuantity(resultSet.getLong("DISPONIBLE"));
				inventarioDTO.setWareHouseId(resultSet.getString("WAREHOUSEID"));
				
				info[index++] = inventarioDTO;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return info;
	}
	
	@Override
	@WebMethod(operationName="confirmarInventario")
	public boolean confirmarInventario(InventarioDTO[] info) {
		
		Connection connection = null;
		CallableStatement callableStatement = null;
		
		ConnectionBD conn = new ConnectionBD();
		connection = conn.getConnectionSQLServerEcommerce();
		
		Timestamp fechaIngreso = new Timestamp(new java.util.Date().getTime());
		
		try {
			
			for (int i = 0; i < info.length; i++) {
				
				InventarioDTO inventarioDTO = info[i];
				
				callableStatement = connection.prepareCall("EXECUTE inventario.Proc_Actualizar_Inventario_Tienda_Amelissa ?,?,?,? ");
				callableStatement.setLong("item_id", inventarioDTO.getItemId());
				callableStatement.setLong("quatity",inventarioDTO.getQuantity());
				callableStatement.setString("wherehouseid",inventarioDTO.getWareHouseId());
				callableStatement.setTimestamp("fecha_creacion",fechaIngreso);
				
				callableStatement.execute();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public static void main(String srgs[]) {
		
		InventarioImpl inv = new InventarioImpl();
		InventarioDTO inventario[] = inv.actualizarInventario();
		System.out.println("Tama�o : " + inventario.length);
		/*
		if (inventario.length > 3) {
			System.out.println("ENTRO AL IF DE ELEMENTOS");
			InventarioDTO inventarioDTO = inventario[1]; 
			inventarioDTO.setQuantity(999999);
		}
		*/
		inv.confirmarInventario(inventario);
		System.out.println("TERMINA");
		
	}
	
}
