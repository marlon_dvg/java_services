package co.com.ecommerce.amelissa.facturacion;

import co.com.ecommerce.amelissa.facturacion.dto.AdditionalInfo;
import co.com.ecommerce.amelissa.facturacion.dto.Address;
import co.com.ecommerce.amelissa.facturacion.dto.ClientProfileData;
import co.com.ecommerce.amelissa.facturacion.dto.DeliveryIds;
import co.com.ecommerce.amelissa.facturacion.dto.Dimension;
import co.com.ecommerce.amelissa.facturacion.dto.ItemAttachment;
import co.com.ecommerce.amelissa.facturacion.dto.Items;
import co.com.ecommerce.amelissa.facturacion.dto.LogisticsInfo;
import co.com.ecommerce.amelissa.facturacion.dto.OrdenDTO;
import co.com.ecommerce.amelissa.facturacion.dto.PackageAttachment;
import co.com.ecommerce.amelissa.facturacion.dto.PaymentData;
import co.com.ecommerce.amelissa.facturacion.dto.Payments;
import co.com.ecommerce.amelissa.facturacion.dto.PriceTags;
import co.com.ecommerce.amelissa.facturacion.dto.RateAndBenefitsIdentifiers;
import co.com.ecommerce.amelissa.facturacion.dto.RatesAndBenefitsData;
import co.com.ecommerce.amelissa.facturacion.dto.Sellers;
import co.com.ecommerce.amelissa.facturacion.dto.ShippingData;
import co.com.ecommerce.amelissa.facturacion.dto.Slas;
import co.com.ecommerce.amelissa.facturacion.dto.Totals;
import co.com.ecommerce.amelissa.facturacion.dto.Transactions;

public class Client_Crud{
	
	public static void main(String[] args) throws Exception {
		
		//URL url = new URL("http://172.17.22.106:8282/Services/FacturacionImplService");
		//URL url = new URL("http://localhost:8282/Services/FacturacionImplService");
		
		/*
		URL url = new URL("http://amelissavirtual.amelissa.com:8084/Services/FacturacionImplService");
        QName qname = new QName("http://facturacion.amelissa.ecommerce.com.co/", "FacturacionImplService");
        Service service = Service.create(url, qname);
        Facturacion mensaje = service.getPort(Facturacion.class);
        */
        
        OrdenDTO orden = new OrdenDTO();
		
		orden.setCommercialConditionData("CommercialConditionData");
		orden.setGiftRegistryData("GiftRegistryData");
		orden.setCallCenterOperatorData("CallCenterOperatorData");
		orden.setOpenTextField("OpenTextField");
		orden.setOrderGroup("OrderGroup");
		orden.setValue("21548");
		orden.setLastMessage("LastMessage");
		orden.setMerchantName("MerchantName");
		orden.setMarketplaceOrderId("MarketplaceOrderId");
		orden.setSalesChannel("SalesChannel");
		orden.setOrderId("OrderId");
		orden.setLastChange("LastChange");
		orden.setRoundingError("RoundingError");
		orden.setChangesAttachment("ChangesAttachment");
		orden.setStatus("Status");
		orden.setMarketplaceServicesEndpoint("MarketplaceServicesEndpoint");
		orden.setOrigin("Origin");
		orden.setSellerOrderId("SellerOrderId");
		orden.setHostname("Hostname");
		orden.setMarketingData("MarketingData");
		orden.setAffiliateId("AffiliateId");
		orden.setOrderFormId("OrderFormId");
		orden.setFollowUpEmail("FollowUpEmail");
		orden.setStatusDescription("StatusDescription");
		orden.setCreationDate("CreationDate");
		orden.setSequence("Sequence");
        
        /////////////////////////////////////
		Totals[] totals = new Totals[2];
		
		Totals totals1 = new Totals();
		totals1.setId("1");
		totals1.setName("name 1");
		totals1.setValue("value 1");
		
		Totals totals2 = new Totals();
		totals2.setId("2");
		totals2.setName("name 2");
		totals2.setValue("value 2");
		
		totals[0] = totals1;
		totals[1] = totals2;
		
		orden.setTotals(totals);
		/////////////////////////////////////
		
		Items[] items = new Items[1];
		
		Items item1 = new Items();
		
		item1.setImageUrl("ImageUrl 1");
    	item1.setMeasurementUnit("MeasurementUnit 1");
    	item1.setPreSaleDate("PreSaleDate 1");
    	item1.setRefId("RefId 1");
    	item1.setUniqueId("UniqueId 1");
    	item1.setId("55167");
    	item1.setListPrice("ListPrice 1");
    	item1.setShippingPrice("ShippingPrice 1");
    	item1.setName("Name 1");
    	item1.setQuantity("2");
    	item1.setCommission("Commission 1");
    	item1.setUnitMultiplier("UnitMultiplier 1");
    	item1.setManualPrice("ManualPrice 1");
    	item1.setDetailUrl("DetailUrl 1");
    	item1.setLockId("LockId 1");
    	item1.setSellerSku("SellerSku 1");
    	item1.setPriceValidUntil("PriceValidUntil 1");
    	item1.setSeller("Seller 1");
    	item1.setProductId("ProductId 1");
    	item1.setIsGift("IsGift 1");
    	item1.setEan("Ean 1");
    	item1.setSellingPrice("SellingPrice 1");
    	item1.setPrice("Price 1");
    	item1.setTax("Tax 1");
    	
    	///////////////////////////////////////////////////////
    	ItemAttachment itemAttachment1 = new ItemAttachment();
    	itemAttachment1.setIdItem(item1.getId());
    	itemAttachment1.setContent("Content 1");
    	itemAttachment1.setName("Name 1");
    	
    	item1.setItemAttachment(itemAttachment1);
    	
        ///////////////////////////////////////////////////////
    	PriceTags[] priceTags = new PriceTags[1];
    	
    	PriceTags priceTags1 = new PriceTags();
    	priceTags1.setIdItem(item1.getId());
    	priceTags1.setName("Name 1");
    	priceTags1.setValue("Value 1");
    	priceTags1.setIsPercentual("IsPercentual 1");
    	priceTags1.setIdentifier("Identifier 1");
		
    	priceTags[0] = priceTags1;
    	
    	item1.setPriceTags(priceTags);
    	
    	//////////////////
    	AdditionalInfo additionalInfo = new AdditionalInfo();
    	
    	additionalInfo.setIdItem(item1.getId());
		additionalInfo.setCategoriesIds("CategoriesIds");
		additionalInfo.setOfferingType("OfferingType");
		additionalInfo.setCommercialConditionId("CommercialConditionId");
		additionalInfo.setProductClusterId("ProductClusterId");
		additionalInfo.setBrandId("BrandId");
		additionalInfo.setBrandName("BrandName");
		additionalInfo.setOfferingInfo("OfferingInfo");
		additionalInfo.setOfferingTypeId("OfferingTypeId");
		
		item1.setAdditionalInfo(additionalInfo);
		
		Dimension dimension = new Dimension();
		dimension.setIdItem(item1.getId());
		dimension.setCubicweight("Cubicweight");
		dimension.setHeight("Height");
		dimension.setLength("Length");
		dimension.setWeight("Weight");
		dimension.setWidth("Width");
		
		additionalInfo.setDimension(dimension);
		
    	items[0] = item1;
    	
    	orden.setItems(items);
    	
    	ClientProfileData clientProfileData = new ClientProfileData();
    	
    	clientProfileData.setIdOrden("IdOrden");
    	clientProfileData.setLastName("HINCAPIE GARCIA");
    	clientProfileData.setPhone("3014454244");
    	clientProfileData.setDocument("71272245");
    	clientProfileData.setDocumentType("CC");
    	clientProfileData.setEmail("eahincapie@gmail.com");
    	clientProfileData.setCorporatePhone("4449141");
    	clientProfileData.setUserProfileId("UserProfileId");
    	clientProfileData.setCorporateDocument("CorporateDocument");
    	clientProfileData.setTradeName("C&F INTERNATIONAL SAS");
    	clientProfileData.setIsCorporate("IsCorporate");
    	clientProfileData.setStateInscription("StateInscription");
    	clientProfileData.setCorporateName("CorporateName");
    	clientProfileData.setFirstName("EDWIN ALFONSO");
    	
    	orden.setClientProfileData(clientProfileData);
    	
    	RatesAndBenefitsData ratesAndBenefitsData = new RatesAndBenefitsData();
    	RateAndBenefitsIdentifiers[] rateAndBenefitsIdentifiers = new RateAndBenefitsIdentifiers[2];
    	
    	RateAndBenefitsIdentifiers rateAndBenefitsIdentifier1 = new RateAndBenefitsIdentifiers();
    	rateAndBenefitsIdentifier1.setId("Id 1");
    	rateAndBenefitsIdentifier1.setDescription("Description 1");
    	rateAndBenefitsIdentifier1.setName("Name 1");
    	rateAndBenefitsIdentifier1.setFeatured("Featured 1");
    	
    	RateAndBenefitsIdentifiers rateAndBenefitsIdentifier2 = new RateAndBenefitsIdentifiers();
    	rateAndBenefitsIdentifier2.setId("Id 2");
    	rateAndBenefitsIdentifier2.setDescription("Description 2");
    	rateAndBenefitsIdentifier2.setName("Name 2");
    	rateAndBenefitsIdentifier2.setFeatured("Featured 2");
    	
    	rateAndBenefitsIdentifiers[0] = rateAndBenefitsIdentifier1;
    	rateAndBenefitsIdentifiers[1] = rateAndBenefitsIdentifier2;
    	
    	ratesAndBenefitsData.setId("1");
    	ratesAndBenefitsData.setRateAndBenefitsIdentifiers(rateAndBenefitsIdentifiers);
    	
    	orden.setRatesAndBenefitsData(ratesAndBenefitsData);
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
		/////////////////////////
    	ShippingData shippingData = new ShippingData();
	    
	    shippingData.setId("1");
	    shippingData.setTrackingHints("TrackingHints");
	    
		Address address = new Address();
		
		address.setComplement("setComplement");
		address.setPostalCode("setPostalCode");
		address.setStreet("setStreet");
		address.setNeighborhood("setNeighborhood");
		address.setState("setState");
		address.setReceiverName("setReceiverName");
		address.setNumber("setNumber");
		address.setAddressType("setAddressType");
		address.setAddressId("setAddressId");
		address.setCity("setCity");
		address.setCountry("setCountry");
		address.setReference("setReference");
		
		
		LogisticsInfo[] logisticsInfo = new LogisticsInfo[1];
		
		LogisticsInfo logistics = new LogisticsInfo();
		    	
    	logistics.setSelectedSla("setSelectedSla");
    	logistics.setDeliveryWindow("setSelectedSla");
    	logistics.setShippingEstimate("setShippingEstimate");
    	logistics.setItemIndex("setItemIndex");
    	logistics.setSellingPrice("setSellingPrice");
    	logistics.setPrice("setPrice");
    	logistics.setLockTTL("setLockTTL");
    	logistics.setListPrice("setListPrice");
    	logistics.setShippingEstimateDate("setShippingEstimateDate");
    	logistics.setDeliveryCompany("setDeliveryCompany");
		
    	logisticsInfo[0] = logistics;
    	
		DeliveryIds[] deliveryIds = new DeliveryIds[1];
		
		
		DeliveryIds delivery = new DeliveryIds();
		
		delivery.setCourierId("setCourierId");
		delivery.setQuantity("setQuantity");
		delivery.setWarehouseId("setWarehouseId");
		delivery.setDockId("setDockId");
		delivery.setCourierName("setCourierName");
		
		deliveryIds[0] = delivery;
		
		Slas[] slas = new Slas[1];
		
		Slas sla = new Slas();
		
		sla.setId("Id");
		sla.setPrice("Price");
		sla.setName("Name");
		sla.setDeliveryWindow("DeliveryWindow");
		sla.setShippingEstimate("ShippingEstimate");
		
		slas[0] = sla;
		
		String[] string = new String[2];
		string[0] = "UNO";
		string[1] = "DOS";
	    
		logistics.setDeliveryIds(deliveryIds);
		logistics.setSlas(slas);
		logistics.setShipsTo(string);
	    
		shippingData.setLogisticsInfo(logisticsInfo);
		shippingData.setAddress(address);
	    
	    
	    orden.setShippingData(shippingData);
	    
	    
	    
    	
    	
		/////////////////////////
    	PaymentData paymentData = new PaymentData();
    	Transactions[] transactions = new Transactions[1];
    	
    	Transactions transaction = new Transactions();
    	
    	transaction.setIsActive("true");
    	transaction.setTransactionId("2edwinh654asd654");
    	
    	Payments[] payments = new Payments[1];
    	
    	Payments payment = new Payments();
    	payment.setGiftCardCaption("GiftCardCaption");
    	payment.setReferenceValue("ReferenceValue");
    	payment.setConnectorResponses("ConnectorResponses");
    	payment.setRedemptionCode("RedemptionCode");
    	payment.setGiftCardId("GiftCardId");
    	payment.setUrl("Url");
    	payment.setExpireYear("ExpireYear");
    	payment.setInstallments("Installments");
    	payment.setId(transaction.getTransactionId());
    	payment.setLastDigits("LastDigits");
    	payment.setPaymentSystemName("PaymentSystemName");
    	payment.setCardHolder("CardHolder");
    	payment.setFirstDigits("FirstDigits");
    	payment.setValue("Value");
    	payment.setPaymentSystem("PaymentSystem");
    	payment.setGiftCardName("GiftCardName");
    	payment.setGroup("Group");
    	payment.setCvv2("Cvv2");
    	payment.setDueDate("DueDate");
    	payment.setExpireMonth("ExpireMonth");
    	payment.setCardNumber("CardNumber");
    	
    	payments[0] = payment;
    	
    	transaction.setPayments(payments);
    	
    	transactions[0] = transaction;
    	
    	paymentData.setTransactions(transactions);
    	
    	orden.setPaymentData(paymentData);
    	
    	/////////////////////////
    	PackageAttachment packageAttachment = new PackageAttachment();
    	String[] packages = new String[3];
    	String string1 = "string 1";
    	String string2 = "string 2";
    	String string3 = "string 3";
    	
    	packages[0] = string1;
    	packages[1] = string2;
    	packages[2] = string3;
    	
    	packageAttachment.setPackages(packages);
    	
    	orden.setPackageAttachment(packageAttachment);
    	
        //////////////////
    	Sellers[] sellers = new Sellers[1];
    	Sellers seller = new Sellers();
    	seller.setId("Seller id");
    	seller.setLogo("Seller logo");
    	seller.setName("Seller name");
    	
    	sellers[0] = seller;
    	
    	orden.setSellers(sellers);
    	
    	boolean exito = false;
    	
        //exito = mensaje.envioOrden(orden);
        
        FacturacionImpl fac = new FacturacionImpl();
        exito = fac.envioOrden(orden);
        
    	//logger = LogsManager.getInstance();
		//logger.info("Probando logguer ");
        System.out.println(" " + exito);
        
    }
}
