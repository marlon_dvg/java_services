package co.com.ecommerce.amelissa.facturacion;

import java.util.LinkedList;
import java.util.Queue;

import co.com.ecommerce.amelissa.facturacion.dto.OrdenDTO;

public class Cola {

	public Cola() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		
		//clasica();
		
		lista();
		
    }
	
	public static void clasica() {
        /*Creamos la Cola Indicando el tipo de dato*/
        Queue<Integer> cola=new LinkedList<Integer>();
        /*Insertamos datos*/
            cola.offer(3);
            cola.add(14);
            cola.offer(12);
            cola.add(7);
            cola.offer(10);
        /*Impresion de la Cola llena con los datos*/
        System.out.println("Cola llena: " + cola);
        /*Estructura repetitiva para desencolar*/
        while(cola.poll()!=null){//Desencolamos y el valor se compara con null
            System.out.println(cola.peek());//Muestra el nuevo Frente
        }
        /*Muestra null debido a que la cola ya esta vacia*/
        System.out.println(cola.peek());     
    }
	
	public static void lista() {
        ColaPedido cola = ColaPedido.getSingletonInstance();//Usamos LinkedList
            cola.offer(1);
            cola.add(2);
            cola.offer(3);
            cola.add(4);
            cola.offer(5);
            cola.add(6);
            cola.add("cadena");
            
        System.out.println("Cola llena: " + cola);
        
        while ( cola.poll() != null ) {
            System.out.println( cola.peek() );
        }
        
        System.out.println( cola.peek() );
    }
	
}
