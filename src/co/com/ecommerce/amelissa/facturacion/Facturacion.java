package co.com.ecommerce.amelissa.facturacion;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import co.com.ecommerce.amelissa.facturacion.dto.OrdenDTO;
import co.com.ecommerce.amelissa.facturacion.dto.OrderTrakingDTO;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Facturacion {
	
	@WebMethod(operationName="envioOrden")
	@WebResult(name="envioOrdenResponse")
	public boolean envioOrden(OrdenDTO ordenDTO);
	
	/*@WebMethod
	public OrdenDTO envioOrdenRespuesta();*/
	
	@WebMethod(operationName="consultaOrden")
	@WebResult(name="consultaOrdenResponse")
	public OrderTrakingDTO consultaOrden(String nroPedido) throws Exception;
	
}
