package co.com.ecommerce.amelissa.facturacion.threads;

public class Recurrencia {
	
	private int[] intentos;
		
	public Recurrencia(int[] intentos) {
		super();
		this.intentos = intentos;
	}

	public int[] getIntentos() {
		return intentos;
	}

	public void setIntentos(int[] intentos) {
		this.intentos = intentos;
	}
	
}
