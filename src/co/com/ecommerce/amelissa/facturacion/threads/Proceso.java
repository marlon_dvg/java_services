package co.com.ecommerce.amelissa.facturacion.threads;

import co.com.ecommerce.amelissa.facturacion.dto.OrdenDTO;

public class Proceso {
	
	private OrdenDTO orden;
	
	public void procesarCompra(Recurrencia recurrencia, long timeStamp) {
		
		System.out.println("Inicia la carga del pedido: " + (System.currentTimeMillis() - timeStamp) / 1000 + "seg");
		int posicionActual = 0;
		
		IngresarPedidosEJB ingresarPedidosEJB = new IngresarPedidosEJB();
		
		for (int i = 0; i < recurrencia.getIntentos().length; i++) {
			posicionActual = i;
			try {
	    		
	    		try {
	    			ingresarPedidosEJB.addIngresarPedidos(orden);
	    			i = recurrencia.getIntentos().length;
				} catch (Exception e) {
					e.printStackTrace();
				}
	    		
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			this.esperarXsegundos(recurrencia.getIntentos()[posicionActual]);
			
			System.out.println("Procesado el pedido " + (i + 1) + " ->Tiempo: " + (System.currentTimeMillis() - timeStamp) / 1000 + "seg");
		}
		
		System.out.println("Finaliza la carga del pedido: " + (System.currentTimeMillis() - timeStamp) / 1000 + "seg");
	}
	
	private void esperarXsegundos(int segundos) {
		try {
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	public OrdenDTO getOrden() {
		return orden;
	}

	public void setOrden(OrdenDTO orden) {
		this.orden = orden;
	}
	
}
