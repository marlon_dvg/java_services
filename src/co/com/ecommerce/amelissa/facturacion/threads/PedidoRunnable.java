package co.com.ecommerce.amelissa.facturacion.threads;

public class PedidoRunnable implements Runnable {
	
	private Recurrencia recurrencia;
	private Proceso proceso;
	private long initialTime;
	
	public PedidoRunnable (Recurrencia recurrencia, Proceso proceso, long initialTime) {
		this.proceso = proceso;
		this.recurrencia = recurrencia;
		this.initialTime = initialTime;
	}

	@Override
	public void run() {
		this.proceso.procesarCompra(this.recurrencia, this.initialTime);
	}
	
}
