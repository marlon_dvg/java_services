package co.com.ecommerce.amelissa.facturacion.dto;

public class Slas {
	private String id;
	private String name;
	private String shippingEstimate;
	private String deliveryWindow;
	private String price;
	private String deliveryChannel;
	private PickupStoreInfo pickupStoreInfo;
	private String polygonName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeliveryWindow() {
		return deliveryWindow;
	}

	public void setDeliveryWindow(String deliveryWindow) {
		this.deliveryWindow = deliveryWindow;
	}

	public String getShippingEstimate() {
		return shippingEstimate;
	}

	public void setShippingEstimate(String shippingEstimate) {
		this.shippingEstimate = shippingEstimate;
	}

	public String getDeliveryChannel() {
		return deliveryChannel;
	}

	public void setDeliveryChannel(String deliveryChannel) {
		this.deliveryChannel = deliveryChannel;
	}

	public PickupStoreInfo getPickupStoreInfo() {
		return pickupStoreInfo;
	}

	public void setPickupStoreInfo(PickupStoreInfo pickupStoreInfo) {
		this.pickupStoreInfo = pickupStoreInfo;
	}

	public String getPolygonName() {
		return polygonName;
	}

	public void setPolygonName(String polygonName) {
		this.polygonName = polygonName;
	}

	@Override
	public String toString() {
		return "Slas [id=" + id + ", name=" + name + ", shippingEstimate=" + shippingEstimate + ", deliveryWindow="
				+ deliveryWindow + ", price=" + price + ", deliveryChannel=" + deliveryChannel + ", pickupStoreInfo="
				+ pickupStoreInfo + ", polygonName=" + polygonName + "]";
	}	
}