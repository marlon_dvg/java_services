package co.com.ecommerce.amelissa.facturacion.dto;

import java.util.Arrays;

public class OrdenDTO {
	private String orderId;
	private String sequence;
	private String marketplaceOrderId;
	private String marketplaceServicesEndpoint;
	private String sellerOrderId;
	private String origin;
	private String affiliateId;
	private String salesChannel;
	private String merchantName;
	private String status;
	private String statusDescription;
	private String value;
	private String creationDate;
	private String lastChange;
	private String orderGroup;
	private Totals[] totals;
	private Items[] items;
	private String[] marketplaceItems;
	private ClientProfileData clientProfileData;
	private String giftRegistryData;
	private String marketingData;
	private RatesAndBenefitsData ratesAndBenefitsData;
	private ShippingData shippingData;
	private PaymentData paymentData;
	private PackageAttachment packageAttachment;
	private Sellers[] sellers;
	private String callCenterOperatorData;
	private String followUpEmail;
	private String lastMessage;
	private String hostname;
	private String invoiceData;
	private String changesAttachment;
	private String openTextField;
	private String roundingError;
	private String orderFormId;
	private String commercialConditionData;
	private String isCompleted;
	private String customData;
	private StorePreferencesData storePreferencesData;
	private String allowCancellation;
	private String allowEdition;
	private String isCheckedIn;
	private MarketPlace marketplace;
	private String authorizedDate;
	private String invoicedDate;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getMarketplaceOrderId() {
		return marketplaceOrderId;
	}

	public void setMarketplaceOrderId(String marketplaceOrderId) {
		this.marketplaceOrderId = marketplaceOrderId;
	}

	public String getMarketplaceServicesEndpoint() {
		return marketplaceServicesEndpoint;
	}

	public void setMarketplaceServicesEndpoint(String marketplaceServicesEndpoint) {
		this.marketplaceServicesEndpoint = marketplaceServicesEndpoint;
	}

	public String getSellerOrderId() {
		return sellerOrderId;
	}

	public void setSellerOrderId(String sellerOrderId) {
		this.sellerOrderId = sellerOrderId;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getLastChange() {
		return lastChange;
	}

	public void setLastChange(String lastChange) {
		this.lastChange = lastChange;
	}

	public String getOrderGroup() {
		return orderGroup;
	}

	public void setOrderGroup(String orderGroup) {
		this.orderGroup = orderGroup;
	}

	public Totals[] getTotals() {
		return totals;
	}

	public void setTotals(Totals[] totals) {
		this.totals = totals;
	}

	public Items[] getItems() {
		return items;
	}

	public void setItems(Items[] items) {
		this.items = items;
	}

	public String[] getMarketplaceItems() {
		return marketplaceItems;
	}

	public void setMarketplaceItems(String[] marketplaceItems) {
		this.marketplaceItems = marketplaceItems;
	}

	public ClientProfileData getClientProfileData() {
		return clientProfileData;
	}

	public void setClientProfileData(ClientProfileData clientProfileData) {
		this.clientProfileData = clientProfileData;
	}

	public String getGiftRegistryData() {
		return giftRegistryData;
	}

	public void setGiftRegistryData(String giftRegistryData) {
		this.giftRegistryData = giftRegistryData;
	}

	public String getMarketingData() {
		return marketingData;
	}

	public void setMarketingData(String marketingData) {
		this.marketingData = marketingData;
	}

	public RatesAndBenefitsData getRatesAndBenefitsData() {
		return ratesAndBenefitsData;
	}

	public void setRatesAndBenefitsData(RatesAndBenefitsData ratesAndBenefitsData) {
		this.ratesAndBenefitsData = ratesAndBenefitsData;
	}

	public ShippingData getShippingData() {
		return shippingData;
	}

	public void setShippingData(ShippingData shippingData) {
		this.shippingData = shippingData;
	}

	public PaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(PaymentData paymentData) {
		this.paymentData = paymentData;
	}

	public PackageAttachment getPackageAttachment() {
		return packageAttachment;
	}

	public void setPackageAttachment(PackageAttachment packageAttachment) {
		this.packageAttachment = packageAttachment;
	}

	public Sellers[] getSellers() {
		return sellers;
	}

	public void setSellers(Sellers[] sellers) {
		this.sellers = sellers;
	}

	public String getCallCenterOperatorData() {
		return callCenterOperatorData;
	}

	public void setCallCenterOperatorData(String callCenterOperatorData) {
		this.callCenterOperatorData = callCenterOperatorData;
	}

	public String getFollowUpEmail() {
		return followUpEmail;
	}

	public void setFollowUpEmail(String followUpEmail) {
		this.followUpEmail = followUpEmail;
	}

	public String getLastMessage() {
		return lastMessage;
	}

	public void setLastMessage(String lastMessage) {
		this.lastMessage = lastMessage;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getInvoiceData() {
		return invoiceData;
	}

	public void setInvoiceData(String invoiceData) {
		this.invoiceData = invoiceData;
	}

	public String getChangesAttachment() {
		return changesAttachment;
	}

	public void setChangesAttachment(String changesAttachment) {
		this.changesAttachment = changesAttachment;
	}

	public String getOpenTextField() {
		return openTextField;
	}

	public void setOpenTextField(String openTextField) {
		this.openTextField = openTextField;
	}

	public String getRoundingError() {
		return roundingError;
	}

	public void setRoundingError(String roundingError) {
		this.roundingError = roundingError;
	}

	public String getOrderFormId() {
		return orderFormId;
	}

	public void setOrderFormId(String orderFormId) {
		this.orderFormId = orderFormId;
	}

	public String getCommercialConditionData() {
		return commercialConditionData;
	}

	public void setCommercialConditionData(String commercialConditionData) {
		this.commercialConditionData = commercialConditionData;
	}

	public String getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getCustomData() {
		return customData;
	}

	public void setCustomData(String customData) {
		this.customData = customData;
	}

	public StorePreferencesData getStorePreferencesData() {
		return storePreferencesData;
	}

	public void setStorePreferencesData(StorePreferencesData storePreferencesData) {
		this.storePreferencesData = storePreferencesData;
	}

	public String getAllowCancellation() {
		return allowCancellation;
	}

	public void setAllowCancellation(String allowCancellation) {
		this.allowCancellation = allowCancellation;
	}

	public String getAllowEdition() {
		return allowEdition;
	}

	public void setAllowEdition(String allowEdition) {
		this.allowEdition = allowEdition;
	}

	public String getIsCheckedIn() {
		return isCheckedIn;
	}

	public void setIsCheckedIn(String isCheckedIn) {
		this.isCheckedIn = isCheckedIn;
	}

	public MarketPlace getMarketplace() {
		return marketplace;
	}

	public void setMarketplace(MarketPlace marketplace) {
		this.marketplace = marketplace;
	}

	public String getAuthorizedDate() {
		return authorizedDate;
	}

	public void setAuthorizedDate(String authorizedDate) {
		this.authorizedDate = authorizedDate;
	}

	public String getInvoicedDate() {
		return invoicedDate;
	}

	public void setInvoicedDate(String invoicedDate) {
		this.invoicedDate = invoicedDate;
	}

	@Override
	public String toString() {
		return "OrdenDTO [orderId=" + orderId + ", sequence=" + sequence + ", marketplaceOrderId=" + marketplaceOrderId
				+ ", marketplaceServicesEndpoint=" + marketplaceServicesEndpoint + ", sellerOrderId=" + sellerOrderId
				+ ", origin=" + origin + ", affiliateId=" + affiliateId + ", salesChannel=" + salesChannel
				+ ", merchantName=" + merchantName + ", status=" + status + ", statusDescription=" + statusDescription
				+ ", value=" + value + ", creationDate=" + creationDate + ", lastChange=" + lastChange + ", orderGroup="
				+ orderGroup + ", totals=" + Arrays.toString(totals) + ", items=" + Arrays.toString(items)
				+ ", marketplaceItems=" + Arrays.toString(marketplaceItems) + ", clientProfileData=" + clientProfileData
				+ ", giftRegistryData=" + giftRegistryData + ", marketingData=" + marketingData
				+ ", ratesAndBenefitsData=" + ratesAndBenefitsData + ", shippingData=" + shippingData + ", paymentData="
				+ paymentData + ", packageAttachment=" + packageAttachment + ", sellers=" + Arrays.toString(sellers)
				+ ", callCenterOperatorData=" + callCenterOperatorData + ", followUpEmail=" + followUpEmail
				+ ", lastMessage=" + lastMessage + ", hostname=" + hostname + ", invoiceData=" + invoiceData
				+ ", changesAttachment=" + changesAttachment + ", openTextField=" + openTextField + ", roundingError="
				+ roundingError + ", orderFormId=" + orderFormId + ", commercialConditionData="
				+ commercialConditionData + ", isCompleted=" + isCompleted + ", customData=" + customData
				+ ", storePreferencesData=" + storePreferencesData + ", allowCancellation=" + allowCancellation
				+ ", allowEdition=" + allowEdition + ", isCheckedIn=" + isCheckedIn + ", marketplace=" + marketplace
				+ ", authorizedDate=" + authorizedDate + ", invoicedDate=" + invoicedDate + "]";
	}
}
