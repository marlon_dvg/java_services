package co.com.ecommerce.amelissa.facturacion.dto;

public class DeliveryIds {
	private String courierId;
	private String courierName;
	private String dockId;
	private String quantity;
	private String warehouseId;

	public String getCourierId() {
		return courierId;
	}

	public void setCourierId(String courierId) {
		this.courierId = courierId;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getDockId() {
		return dockId;
	}

	public void setDockId(String dockId) {
		this.dockId = dockId;
	}

	public String getCourierName() {
		return courierName;
	}

	public void setCourierName(String courierName) {
		this.courierName = courierName;
	}

	@Override
	public String toString() {
		return "ClassPojo [courierId = " + courierId + ", quantity = " + quantity + ", warehouseId = " + warehouseId
				+ ", dockId = " + dockId + ", courierName = " + courierName + "]";
	}
}
