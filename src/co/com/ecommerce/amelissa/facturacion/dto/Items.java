package co.com.ecommerce.amelissa.facturacion.dto;

import java.util.Arrays;

public class Items {	
	private String uniqueId;
	private String id;
	private String productId;
	private String ean;
	private String lockId;
	private ItemAttachment itemAttachment;
	private String[] attachments;
	private String quantity;
	private String seller;
	private String name;
	private String refId;
	private String price;
	private String listPrice;
	private String manualPrice;
	private PriceTags[] priceTags;
	private String imageUrl;
	private String detailUrl;
	private String[] components;
	private String[] bundleItems;
	private String[] params;
	private String[] offerings;
	private String sellerSku;
	private String priceValidUntil;
	private String commission;
	private String tax;
	private String preSaleDate;
	private AdditionalInfo additionalInfo;
	private String measurementUnit;
	private String unitMultiplier;
	private String sellingPrice;
	private String isGift;
	private String shippingPrice;
	private String rewardValue; 
	private String freightCommission;
	private String priceDefinitions; 
	private String taxCode;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public String getPreSaleDate() {
		return preSaleDate;
	}

	public void setPreSaleDate(String preSaleDate) {
		this.preSaleDate = preSaleDate;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getListPrice() {
		return listPrice;
	}

	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}

	public String getShippingPrice() {
		return shippingPrice;
	}

	public void setShippingPrice(String shippingPrice) {
		this.shippingPrice = shippingPrice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PriceTags[] getPriceTags() {
		return priceTags;
	}

	public void setPriceTags(PriceTags[] priceTags) {
		this.priceTags = priceTags;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String[] getAttachments() {
		return attachments;
	}

	public void setAttachments(String[] attachments) {
		this.attachments = attachments;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getUnitMultiplier() {
		return unitMultiplier;
	}

	public void setUnitMultiplier(String unitMultiplier) {
		this.unitMultiplier = unitMultiplier;
	}

	public String[] getOfferings() {
		return offerings;
	}

	public void setOfferings(String[] offerings) {
		this.offerings = offerings;
	}

	public String getManualPrice() {
		return manualPrice;
	}

	public void setManualPrice(String manualPrice) {
		this.manualPrice = manualPrice;
	}

	public String getDetailUrl() {
		return detailUrl;
	}

	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}

	public String getLockId() {
		return lockId;
	}

	public void setLockId(String lockId) {
		this.lockId = lockId;
	}

	public String getSellerSku() {
		return sellerSku;
	}

	public void setSellerSku(String sellerSku) {
		this.sellerSku = sellerSku;
	}

	public ItemAttachment getItemAttachment() {
		return itemAttachment;
	}

	public void setItemAttachment(ItemAttachment itemAttachment) {
		this.itemAttachment = itemAttachment;
	}

	public String[] getParams() {
		return params;
	}

	public void setParams(String[] params) {
		this.params = params;
	}

	public String getPriceValidUntil() {
		return priceValidUntil;
	}

	public void setPriceValidUntil(String priceValidUntil) {
		this.priceValidUntil = priceValidUntil;
	}

	public AdditionalInfo getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(AdditionalInfo additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getIsGift() {
		return isGift;
	}

	public void setIsGift(String isGift) {
		this.isGift = isGift;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public String getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String[] getBundleItems() {
		return bundleItems;
	}

	public void setBundleItems(String[] bundleItems) {
		this.bundleItems = bundleItems;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String[] getComponents() {
		return components;
	}

	public void setComponents(String[] components) {
		this.components = components;
	}

	public String getRewardValue() {
		return rewardValue;
	}

	public void setRewardValue(String rewardValue) {
		this.rewardValue = rewardValue;
	}

	public String getFreightCommission() {
		return freightCommission;
	}

	public void setFreightCommission(String freightCommission) {
		this.freightCommission = freightCommission;
	}

	public String getPriceDefinitions() {
		return priceDefinitions;
	}

	public void setPriceDefinitions(String priceDefinitions) {
		this.priceDefinitions = priceDefinitions;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	@Override
	public String toString() {
		return "Items [uniqueId=" + uniqueId + ", id=" + id + ", productId=" + productId + ", ean=" + ean + ", lockId="
				+ lockId + ", itemAttachment=" + itemAttachment + ", attachments=" + Arrays.toString(attachments)
				+ ", quantity=" + quantity + ", seller=" + seller + ", name=" + name + ", refId=" + refId + ", price="
				+ price + ", listPrice=" + listPrice + ", manualPrice=" + manualPrice + ", priceTags="
				+ Arrays.toString(priceTags) + ", imageUrl=" + imageUrl + ", detailUrl=" + detailUrl + ", components="
				+ Arrays.toString(components) + ", bundleItems=" + Arrays.toString(bundleItems) + ", params="
				+ Arrays.toString(params) + ", offerings=" + Arrays.toString(offerings) + ", sellerSku=" + sellerSku
				+ ", priceValidUntil=" + priceValidUntil + ", commission=" + commission + ", tax=" + tax
				+ ", preSaleDate=" + preSaleDate + ", additionalInfo=" + additionalInfo + ", measurementUnit="
				+ measurementUnit + ", unitMultiplier=" + unitMultiplier + ", sellingPrice=" + sellingPrice
				+ ", isGift=" + isGift + ", shippingPrice=" + shippingPrice + ", rewardValue=" + rewardValue
				+ ", freightCommission=" + freightCommission + ", priceDefinitions=" + priceDefinitions + ", taxCode="
				+ taxCode + "]";
	}
}
