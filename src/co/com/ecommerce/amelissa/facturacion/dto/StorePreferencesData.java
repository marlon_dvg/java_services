package co.com.ecommerce.amelissa.facturacion.dto;

public class StorePreferencesData {
	private String countryCode;
	private String currencyCode;
	private CurrencyFormatInfo currencyFormatInfo;
	private String currencyLocale;
	private String currencySymbol;
	private String timeZone;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public CurrencyFormatInfo getCurrencyFormatInfo() {
		return currencyFormatInfo;
	}

	public void setCurrencyFormatInfo(CurrencyFormatInfo currencyFormatInfo) {
		this.currencyFormatInfo = currencyFormatInfo;
	}

	public String getCurrencyLocale() {
		return currencyLocale;
	}

	public void setCurrencyLocale(String currencyLocale) {
		this.currencyLocale = currencyLocale;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	@Override
	public String toString() {
		return "StorePreferencesData [countryCode=" + countryCode + ", currencyCode=" + currencyCode
				+ ", currencyFormatInfo=" + currencyFormatInfo + ", currencyLocale=" + currencyLocale
				+ ", currencySymbol=" + currencySymbol + ", timeZone=" + timeZone + "]";
	}
}
