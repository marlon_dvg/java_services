package co.com.ecommerce.amelissa.facturacion.dto.envia;


public class ListaBultos
{
	private String codigo;

	private String estado;

	private String vuelo;

	private String terrestre;

	private String referencia;

	private String numero;

	public String getCodigo ()
	{
		return codigo;
	}

	public void setCodigo (String codigo)
	{
		this.codigo = codigo;
	}

	public String getEstado ()
	{
		return estado;
	}

	public void setEstado (String estado)
	{
		this.estado = estado;
	}

	public String getVuelo ()
	{
		return vuelo;
	}

	public void setVuelo (String vuelo)
	{
		this.vuelo = vuelo;
	}

	public String getTerrestre ()
	{
		return terrestre;
	}

	public void setTerrestre (String terrestre)
	{
		this.terrestre = terrestre;
	}

	public String getReferencia ()
	{
		return referencia;
	}

	public void setReferencia (String referencia)
	{
		this.referencia = referencia;
	}

	public String getNumero ()
	{
		return numero;
	}

	public void setNumero (String numero)
	{
		this.numero = numero;
	}

}