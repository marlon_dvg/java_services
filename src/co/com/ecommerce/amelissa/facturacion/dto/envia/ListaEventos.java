package co.com.ecommerce.amelissa.facturacion.dto.envia;

public class ListaEventos {
	private String codigo;

	private String tipoEvento;

	private String deleNombre;

	private String deleCodigo;

	private String descripcion;

	private String fechaAlta;

	private String fechaEvento;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String getDeleNombre() {
		return deleNombre;
	}

	public void setDeleNombre(String deleNombre) {
		this.deleNombre = deleNombre;
	}

	public String getDeleCodigo() {
		return deleCodigo;
	}

	public void setDeleCodigo(String deleCodigo) {
		this.deleCodigo = deleCodigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getFechaEvento() {
		return fechaEvento;
	}

	public void setFechaEvento(String fechaEvento) {
		this.fechaEvento = fechaEvento;
	}
	
}
