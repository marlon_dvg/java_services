package co.com.ecommerce.amelissa.facturacion.dto.envia;

public class Envia
{
    private String nifQuienRecibe;

    private String codigoServicio;

    private String direccionDestinatario;

    private String nombreRemitente;

    private String direccionRemitente;

    private String tipoTrackingServicio;

    private String oficinaRemitente;

    private String codigoPostalDestinatario;

    private ListaBultos[] listaBultos;

    private String retornoEnvio;

    private String telefonoDestinatario;

    private String nombreQuienRecibe;

    private String fechaHoraEntrega;

    private String codigo;

    private String nitDestinatario;

    private String codigoRemitente;

    private String numeroEnvio;

    private String fechaHoraPrevistaEntrega;

    private String codigoPostalRemitente;

    private String contactoDestinatario;

    private String esMultipieza;

    private String poblacionRemitente;

    private String nombreDestinatario;

    private String kilos;

    private String numeroTotalPiezas;

    private ListaEventos[] listaEventos;

    private String nitRemitente;

    private String bultoCargaConVuelo;

    private String fechaHoraAdmision;

    private ListaIncidencias[] listaIncidencias;

    private String portes;

    private String rutaPod;

    private String rutasCartasPorte;

    private String numeroReferencia;

    private String poblacionDestinatario;

    private String descripcionServicio;

    private String nombreProvinciaDestinatario;

    public String getNifQuienRecibe ()
    {
        return nifQuienRecibe;
    }

    public void setNifQuienRecibe (String nifQuienRecibe)
    {
        this.nifQuienRecibe = nifQuienRecibe;
    }

    public String getCodigoServicio ()
    {
        return codigoServicio;
    }

    public void setCodigoServicio (String codigoServicio)
    {
        this.codigoServicio = codigoServicio;
    }

    public String getDireccionDestinatario ()
    {
        return direccionDestinatario;
    }

    public void setDireccionDestinatario (String direccionDestinatario)
    {
        this.direccionDestinatario = direccionDestinatario;
    }

    public String getNombreRemitente ()
    {
        return nombreRemitente;
    }

    public void setNombreRemitente (String nombreRemitente)
    {
        this.nombreRemitente = nombreRemitente;
    }

    public String getDireccionRemitente ()
    {
        return direccionRemitente;
    }

    public void setDireccionRemitente (String direccionRemitente)
    {
        this.direccionRemitente = direccionRemitente;
    }

    public String getTipoTrackingServicio ()
    {
        return tipoTrackingServicio;
    }

    public void setTipoTrackingServicio (String tipoTrackingServicio)
    {
        this.tipoTrackingServicio = tipoTrackingServicio;
    }

    public String getOficinaRemitente ()
    {
        return oficinaRemitente;
    }

    public void setOficinaRemitente (String oficinaRemitente)
    {
        this.oficinaRemitente = oficinaRemitente;
    }

    public String getCodigoPostalDestinatario ()
    {
        return codigoPostalDestinatario;
    }

    public void setCodigoPostalDestinatario (String codigoPostalDestinatario)
    {
        this.codigoPostalDestinatario = codigoPostalDestinatario;
    }

    public ListaBultos[] getListaBultos ()
    {
        return listaBultos;
    }

    public void setListaBultos (ListaBultos[] listaBultos)
    {
        this.listaBultos = listaBultos;
    }

    public String getRetornoEnvio ()
    {
        return retornoEnvio;
    }

    public void setRetornoEnvio (String retornoEnvio)
    {
        this.retornoEnvio = retornoEnvio;
    }

    public String getTelefonoDestinatario ()
    {
        return telefonoDestinatario;
    }

    public void setTelefonoDestinatario (String telefonoDestinatario)
    {
        this.telefonoDestinatario = telefonoDestinatario;
    }

    public String getNombreQuienRecibe ()
    {
        return nombreQuienRecibe;
    }

    public void setNombreQuienRecibe (String nombreQuienRecibe)
    {
        this.nombreQuienRecibe = nombreQuienRecibe;
    }

    public String getFechaHoraEntrega ()
    {
        return fechaHoraEntrega;
    }

    public void setFechaHoraEntrega (String fechaHoraEntrega)
    {
        this.fechaHoraEntrega = fechaHoraEntrega;
    }

    public String getCodigo ()
    {
        return codigo;
    }

    public void setCodigo (String codigo)
    {
        this.codigo = codigo;
    }

    public String getNitDestinatario ()
    {
        return nitDestinatario;
    }

    public void setNitDestinatario (String nitDestinatario)
    {
        this.nitDestinatario = nitDestinatario;
    }

    public String getCodigoRemitente ()
    {
        return codigoRemitente;
    }

    public void setCodigoRemitente (String codigoRemitente)
    {
        this.codigoRemitente = codigoRemitente;
    }

    public String getNumeroEnvio ()
    {
        return numeroEnvio;
    }

    public void setNumeroEnvio (String numeroEnvio)
    {
        this.numeroEnvio = numeroEnvio;
    }

    public String getFechaHoraPrevistaEntrega ()
    {
        return fechaHoraPrevistaEntrega;
    }

    public void setFechaHoraPrevistaEntrega (String fechaHoraPrevistaEntrega)
    {
        this.fechaHoraPrevistaEntrega = fechaHoraPrevistaEntrega;
    }

    public String getCodigoPostalRemitente ()
    {
        return codigoPostalRemitente;
    }

    public void setCodigoPostalRemitente (String codigoPostalRemitente)
    {
        this.codigoPostalRemitente = codigoPostalRemitente;
    }

    public String getContactoDestinatario ()
    {
        return contactoDestinatario;
    }

    public void setContactoDestinatario (String contactoDestinatario)
    {
        this.contactoDestinatario = contactoDestinatario;
    }

    public String getEsMultipieza ()
    {
        return esMultipieza;
    }

    public void setEsMultipieza (String esMultipieza)
    {
        this.esMultipieza = esMultipieza;
    }

    public String getPoblacionRemitente ()
    {
        return poblacionRemitente;
    }

    public void setPoblacionRemitente (String poblacionRemitente)
    {
        this.poblacionRemitente = poblacionRemitente;
    }

    public String getNombreDestinatario ()
    {
        return nombreDestinatario;
    }

    public void setNombreDestinatario (String nombreDestinatario)
    {
        this.nombreDestinatario = nombreDestinatario;
    }

    public String getKilos ()
    {
        return kilos;
    }

    public void setKilos (String kilos)
    {
        this.kilos = kilos;
    }

    public String getNumeroTotalPiezas ()
    {
        return numeroTotalPiezas;
    }

    public void setNumeroTotalPiezas (String numeroTotalPiezas)
    {
        this.numeroTotalPiezas = numeroTotalPiezas;
    }

    public ListaEventos[] getListaEventos ()
    {
        return listaEventos;
    }

    public void setListaEventos (ListaEventos[] listaEventos)
    {
        this.listaEventos = listaEventos;
    }

    public String getNitRemitente ()
    {
        return nitRemitente;
    }

    public void setNitRemitente (String nitRemitente)
    {
        this.nitRemitente = nitRemitente;
    }

    public String getBultoCargaConVuelo ()
    {
        return bultoCargaConVuelo;
    }

    public void setBultoCargaConVuelo (String bultoCargaConVuelo)
    {
        this.bultoCargaConVuelo = bultoCargaConVuelo;
    }

    public String getFechaHoraAdmision ()
    {
        return fechaHoraAdmision;
    }

    public void setFechaHoraAdmision (String fechaHoraAdmision)
    {
        this.fechaHoraAdmision = fechaHoraAdmision;
    }

    public ListaIncidencias[] getListaIncidencias ()
    {
        return listaIncidencias;
    }

    public void setListaIncidencias (ListaIncidencias[] listaIncidencias)
    {
        this.listaIncidencias = listaIncidencias;
    }

    public String getPortes ()
    {
        return portes;
    }

    public void setPortes (String portes)
    {
        this.portes = portes;
    }

    public String getRutaPod ()
    {
        return rutaPod;
    }

    public void setRutaPod (String rutaPod)
    {
        this.rutaPod = rutaPod;
    }

    public String getRutasCartasPorte ()
    {
        return rutasCartasPorte;
    }

    public void setRutasCartasPorte (String rutasCartasPorte)
    {
        this.rutasCartasPorte = rutasCartasPorte;
    }

    public String getNumeroReferencia ()
    {
        return numeroReferencia;
    }

    public void setNumeroReferencia (String numeroReferencia)
    {
        this.numeroReferencia = numeroReferencia;
    }

    public String getPoblacionDestinatario ()
    {
        return poblacionDestinatario;
    }

    public void setPoblacionDestinatario (String poblacionDestinatario)
    {
        this.poblacionDestinatario = poblacionDestinatario;
    }

    public String getDescripcionServicio ()
    {
        return descripcionServicio;
    }

    public void setDescripcionServicio (String descripcionServicio)
    {
        this.descripcionServicio = descripcionServicio;
    }

    public String getNombreProvinciaDestinatario ()
    {
        return nombreProvinciaDestinatario;
    }

    public void setNombreProvinciaDestinatario (String nombreProvinciaDestinatario)
    {
        this.nombreProvinciaDestinatario = nombreProvinciaDestinatario;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [nifQuienRecibe = "+nifQuienRecibe+", codigoServicio = "+codigoServicio+", direccionDestinatario = "+direccionDestinatario+", nombreRemitente = "+nombreRemitente+", direccionRemitente = "+direccionRemitente+", tipoTrackingServicio = "+tipoTrackingServicio+", oficinaRemitente = "+oficinaRemitente+", codigoPostalDestinatario = "+codigoPostalDestinatario+", listaBultos = "+listaBultos+", retornoEnvio = "+retornoEnvio+", telefonoDestinatario = "+telefonoDestinatario+", nombreQuienRecibe = "+nombreQuienRecibe+", fechaHoraEntrega = "+fechaHoraEntrega+", codigo = "+codigo+", nitDestinatario = "+nitDestinatario+", codigoRemitente = "+codigoRemitente+", numeroEnvio = "+numeroEnvio+", fechaHoraPrevistaEntrega = "+fechaHoraPrevistaEntrega+", codigoPostalRemitente = "+codigoPostalRemitente+", contactoDestinatario = "+contactoDestinatario+", esMultipieza = "+esMultipieza+", poblacionRemitente = "+poblacionRemitente+", nombreDestinatario = "+nombreDestinatario+", kilos = "+kilos+", numeroTotalPiezas = "+numeroTotalPiezas+", listaEventos = "+listaEventos+", nitRemitente = "+nitRemitente+", bultoCargaConVuelo = "+bultoCargaConVuelo+", fechaHoraAdmision = "+fechaHoraAdmision+", listaIncidencias = "+listaIncidencias+", portes = "+portes+", rutaPod = "+rutaPod+", rutasCartasPorte = "+rutasCartasPorte+", numeroReferencia = "+numeroReferencia+", poblacionDestinatario = "+poblacionDestinatario+", descripcionServicio = "+descripcionServicio+", nombreProvinciaDestinatario = "+nombreProvinciaDestinatario+"]";
    }
}