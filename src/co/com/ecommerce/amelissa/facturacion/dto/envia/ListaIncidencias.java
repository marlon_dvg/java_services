package co.com.ecommerce.amelissa.facturacion.dto.envia;

public class ListaIncidencias
{
    private String descripcion;

    private String nombreUsuarioAlta;

    private String nombreDelegacionAlta;

    private String descripcionOficinaAlta;

    private String codigoDelegacionAlta;

    private String codigo;

    private String id;

    private String cerrada;

    private String listaRespuestasIncidencia;

    private String devolverAsesorIncidente;

    private String codigoUsuarioAlta;

    private String fechaCierre;

    private String tipo;

    private String fechaAlta;

    private String iataOficinaAlta;

    private String codigoIncidencia;

    public String getDescripcion ()
    {
        return descripcion;
    }

    public void setDescripcion (String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getNombreUsuarioAlta ()
    {
        return nombreUsuarioAlta;
    }

    public void setNombreUsuarioAlta (String nombreUsuarioAlta)
    {
        this.nombreUsuarioAlta = nombreUsuarioAlta;
    }

    public String getNombreDelegacionAlta ()
    {
        return nombreDelegacionAlta;
    }

    public void setNombreDelegacionAlta (String nombreDelegacionAlta)
    {
        this.nombreDelegacionAlta = nombreDelegacionAlta;
    }

    public String getDescripcionOficinaAlta ()
    {
        return descripcionOficinaAlta;
    }

    public void setDescripcionOficinaAlta (String descripcionOficinaAlta)
    {
        this.descripcionOficinaAlta = descripcionOficinaAlta;
    }

    public String getCodigoDelegacionAlta ()
    {
        return codigoDelegacionAlta;
    }

    public void setCodigoDelegacionAlta (String codigoDelegacionAlta)
    {
        this.codigoDelegacionAlta = codigoDelegacionAlta;
    }

    public String getCodigo ()
    {
        return codigo;
    }

    public void setCodigo (String codigo)
    {
        this.codigo = codigo;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCerrada ()
    {
        return cerrada;
    }

    public void setCerrada (String cerrada)
    {
        this.cerrada = cerrada;
    }

    public String getListaRespuestasIncidencia ()
    {
        return listaRespuestasIncidencia;
    }

    public void setListaRespuestasIncidencia (String listaRespuestasIncidencia)
    {
        this.listaRespuestasIncidencia = listaRespuestasIncidencia;
    }

    public String getDevolverAsesorIncidente ()
    {
        return devolverAsesorIncidente;
    }

    public void setDevolverAsesorIncidente (String devolverAsesorIncidente)
    {
        this.devolverAsesorIncidente = devolverAsesorIncidente;
    }

    public String getCodigoUsuarioAlta ()
    {
        return codigoUsuarioAlta;
    }

    public void setCodigoUsuarioAlta (String codigoUsuarioAlta)
    {
        this.codigoUsuarioAlta = codigoUsuarioAlta;
    }

    public String getFechaCierre ()
    {
        return fechaCierre;
    }

    public void setFechaCierre (String fechaCierre)
    {
        this.fechaCierre = fechaCierre;
    }

    public String getTipo ()
    {
        return tipo;
    }

    public void setTipo (String tipo)
    {
        this.tipo = tipo;
    }

    public String getFechaAlta ()
    {
        return fechaAlta;
    }

    public void setFechaAlta (String fechaAlta)
    {
        this.fechaAlta = fechaAlta;
    }

    public String getIataOficinaAlta ()
    {
        return iataOficinaAlta;
    }

    public void setIataOficinaAlta (String iataOficinaAlta)
    {
        this.iataOficinaAlta = iataOficinaAlta;
    }

    public String getCodigoIncidencia ()
    {
        return codigoIncidencia;
    }

    public void setCodigoIncidencia (String codigoIncidencia)
    {
        this.codigoIncidencia = codigoIncidencia;
    }
}
