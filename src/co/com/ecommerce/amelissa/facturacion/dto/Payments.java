package co.com.ecommerce.amelissa.facturacion.dto;

public class Payments {
	private String id;
	private String paymentSystem;
	private String paymentSystemName;
	private String value;
	private String installments;
	private String referenceValue;
	private String cardHolder;
	private String cardNumber;
	private String firstDigits;
	private String lastDigits;
	private String cvv2;
	private String expireMonth;
	private String expireYear;
	private String url;
	private String giftCardId;
	private String giftCardName;
	private String giftCardCaption;
	private String redemptionCode;
	private String group;
	private String tid;
	private String dueDate;
	private String connectorResponses;

	public String getGiftCardCaption() {
		return giftCardCaption;
	}

	public void setGiftCardCaption(String giftCardCaption) {
		this.giftCardCaption = giftCardCaption;
	}

	public String getReferenceValue() {
		return referenceValue;
	}

	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}

	public String getConnectorResponses() {
		return connectorResponses;
	}

	public void setConnectorResponses(String connectorResponses) {
		this.connectorResponses = connectorResponses;
	}

	public String getRedemptionCode() {
		return redemptionCode;
	}

	public void setRedemptionCode(String redemptionCode) {
		this.redemptionCode = redemptionCode;
	}

	public String getGiftCardId() {
		return giftCardId;
	}

	public void setGiftCardId(String giftCardId) {
		this.giftCardId = giftCardId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getExpireYear() {
		return expireYear;
	}

	public void setExpireYear(String expireYear) {
		this.expireYear = expireYear;
	}

	public String getInstallments() {
		return installments;
	}

	public void setInstallments(String installments) {
		this.installments = installments;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastDigits() {
		return lastDigits;
	}

	public void setLastDigits(String lastDigits) {
		this.lastDigits = lastDigits;
	}

	public String getPaymentSystemName() {
		return paymentSystemName;
	}

	public void setPaymentSystemName(String paymentSystemName) {
		this.paymentSystemName = paymentSystemName;
	}

	public String getCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public String getFirstDigits() {
		return firstDigits;
	}

	public void setFirstDigits(String firstDigits) {
		this.firstDigits = firstDigits;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPaymentSystem() {
		return paymentSystem;
	}

	public void setPaymentSystem(String paymentSystem) {
		this.paymentSystem = paymentSystem;
	}

	public String getGiftCardName() {
		return giftCardName;
	}

	public void setGiftCardName(String giftCardName) {
		this.giftCardName = giftCardName;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getExpireMonth() {
		return expireMonth;
	}

	public void setExpireMonth(String expireMonth) {
		this.expireMonth = expireMonth;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	@Override
	public String toString() {
		return "ClassPojo [giftCardCaption = " + giftCardCaption + ", referenceValue = " + referenceValue
				+ ", connectorResponses = " + connectorResponses + ", redemptionCode = " + redemptionCode
				+ ", giftCardId = " + giftCardId + ", url = " + url + ", expireYear = " + expireYear
				+ ", installments = " + installments + ", id = " + id + ", lastDigits = " + lastDigits
				+ ", paymentSystemName = " + paymentSystemName + ", cardHolder = " + cardHolder + ", firstDigits = "
				+ firstDigits + ", value = " + value + ", paymentSystem = " + paymentSystem + ", giftCardName = "
				+ giftCardName + ", tid = " + tid + ", group = " + group + ", cvv2 = " + cvv2 + ", dueDate = " + dueDate
				+ ", expireMonth = " + expireMonth + ", cardNumber = " + cardNumber + "]";
	}
}
