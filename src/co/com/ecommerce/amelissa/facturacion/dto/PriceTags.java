package co.com.ecommerce.amelissa.facturacion.dto;

public class PriceTags {
	
	private String idItem;
	private String name;
	private String value;
	private String isPercentual;
	private String identifier;
	private String rawValue;

	public String getIdItem() {
		return idItem;
	}

	public void setIdItem(String idItem) {
		this.idItem = idItem;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIsPercentual() {
		return isPercentual;
	}

	public void setIsPercentual(String isPercentual) {
		this.isPercentual = isPercentual;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getRawValue() {
		return rawValue;
	}

	public void setRawValue(String rawValue) {
		this.rawValue = rawValue;
	}

	@Override
	public String toString() {
		return "ClassPojo [name = " + name + ", value = " + value + ", isPercentual = " + isPercentual
				+ ", identifier = " + identifier + "]";
	}
}
