package co.com.ecommerce.amelissa.facturacion.dto;

import java.util.Arrays;

public class Transactions {
	private String isActive;
	private String transactionId;
	private String merchantName;
	private Payments[] payments;

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Payments[] getPayments() {
		return payments;
	}

	public void setPayments(Payments[] payments) {
		this.payments = payments;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	@Override
	public String toString() {
		return "Transactions [isActive=" + isActive + ", transactionId=" + transactionId + ", merchantName="
				+ merchantName + ", payments=" + Arrays.toString(payments) + "]";
	}	
}