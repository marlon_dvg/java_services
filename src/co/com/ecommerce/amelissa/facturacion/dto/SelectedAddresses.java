package co.com.ecommerce.amelissa.facturacion.dto;

import java.util.Arrays;

public class SelectedAddresses {
	private String addressId;
	private String addressType;
	private String receiverName;
	private String street;
	private String number;
	private String complement;
	private String neighborhood;
	private String postalCode;
	private String city;
	private String state;
	private String country;
	private String reference;
	private String[] geoCoordinates;

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String[] getGeoCoordinates() {
		return geoCoordinates;
	}

	public void setGeoCoordinates(String[] geoCoordinates) {
		this.geoCoordinates = geoCoordinates;
	}

	@Override
	public String toString() {
		return "SelectedAddresses [addressId=" + addressId + ", addressType=" + addressType + ", receiverName="
				+ receiverName + ", street=" + street + ", number=" + number + ", complement=" + complement
				+ ", neighborhood=" + neighborhood + ", postalCode=" + postalCode + ", city=" + city + ", state="
				+ state + ", country=" + country + ", reference=" + reference + ", geoCoordinates="
				+ Arrays.toString(geoCoordinates) + "]";
	}
}
