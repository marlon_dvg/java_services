package co.com.ecommerce.amelissa.facturacion.dto;

public class CurrencyFormatInfo {
	private String CurrencyDecimalDigits;
	private String CurrencyDecimalSeparators;
	private String CurrencyGroupSeparator;
	private String CurrencyGroupSize;
	private String StartsWithCurrencySymbol;

	public String getCurrencyDecimalDigits() {
		return CurrencyDecimalDigits;
	}

	public void setCurrencyDecimalDigits(String currencyDecimalDigits) {
		CurrencyDecimalDigits = currencyDecimalDigits;
	}

	public String getCurrencyDecimalSeparators() {
		return CurrencyDecimalSeparators;
	}

	public void setCurrencyDecimalSeparators(String currencyDecimalSeparators) {
		CurrencyDecimalSeparators = currencyDecimalSeparators;
	}

	public String getCurrencyGroupSeparator() {
		return CurrencyGroupSeparator;
	}

	public void setCurrencyGroupSeparator(String currencyGroupSeparator) {
		CurrencyGroupSeparator = currencyGroupSeparator;
	}

	public String getCurrencyGroupSize() {
		return CurrencyGroupSize;
	}

	public void setCurrencyGroupSize(String currencyGroupSize) {
		CurrencyGroupSize = currencyGroupSize;
	}

	public String getStartsWithCurrencySymbol() {
		return StartsWithCurrencySymbol;
	}

	public void setStartsWithCurrencySymbol(String startsWithCurrencySymbol) {
		StartsWithCurrencySymbol = startsWithCurrencySymbol;
	}

	@Override
	public String toString() {
		return "CurrencyFormatInfo [CurrencyDecimalDigits=" + CurrencyDecimalDigits + ", CurrencyDecimalSeparators="
				+ CurrencyDecimalSeparators + ", CurrencyGroupSeparator=" + CurrencyGroupSeparator
				+ ", CurrencyGroupSize=" + CurrencyGroupSize + ", StartsWithCurrencySymbol=" + StartsWithCurrencySymbol
				+ "]";
	}
}
