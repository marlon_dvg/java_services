package co.com.ecommerce.amelissa.facturacion.dto;

import java.util.Arrays;

public class ShippingData {
	private String id;
	private Address address;
	private LogisticsInfo[] logisticsInfo;
	private String trackingHints;
	private SelectedAddresses[] selectedAddresses;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LogisticsInfo[] getLogisticsInfo() {
		return logisticsInfo;
	}

	public void setLogisticsInfo(LogisticsInfo[] logisticsInfo) {
		this.logisticsInfo = logisticsInfo;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getTrackingHints() {
		return trackingHints;
	}

	public void setTrackingHints(String trackingHints) {
		this.trackingHints = trackingHints;
	}

	public SelectedAddresses[] getSelectedAddresses() {
		return selectedAddresses;
	}

	public void setSelectedAddresses(SelectedAddresses[] selectedAddresses) {
		this.selectedAddresses = selectedAddresses;
	}

	@Override
	public String toString() {
		return "ShippingData [id=" + id + ", address=" + address + ", logisticsInfo=" + Arrays.toString(logisticsInfo)
				+ ", trackingHints=" + trackingHints + ", selectedAddresses=" + Arrays.toString(selectedAddresses)
				+ "]";
	}
}
