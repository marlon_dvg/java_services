package co.com.ecommerce.amelissa.facturacion.dto;

public class RateAndBenefitsIdentifiers {
	private String description;
	private String featured;
	private String id;
	private String name;
	private MatchedParameters matchedParameters;
	private String additionalInfo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFeatured() {
		return featured;
	}

	public void setFeatured(String featured) {
		this.featured = featured;
	}

	public MatchedParameters getMatchedParameters() {
		return matchedParameters;
	}

	public void setMatchedParameters(MatchedParameters matchedParameters) {
		this.matchedParameters = matchedParameters;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	@Override
	public String toString() {
		return "RateAndBenefitsIdentifiers [description=" + description + ", featured=" + featured + ", id=" + id
				+ ", name=" + name + ", matchedParameters=" + matchedParameters + ", additionalInfo=" + additionalInfo
				+ "]";
	}	
}