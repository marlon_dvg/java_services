package co.com.ecommerce.amelissa.facturacion.dto;

public class AdditionalInfo {

	private String idItem;
	private String brandName;
	private String brandId;
	private String categoriesIds;
	private String productClusterId;
	private String commercialConditionId;
	private Dimension dimension;
	private String offeringInfo;
	private String offeringType;
	private String offeringTypeId;

	public String getIdItem() {
		return idItem;
	}

	public void setIdItem(String idItem) {
		this.idItem = idItem;
	}

	public String getCategoriesIds() {
		return categoriesIds;
	}

	public void setCategoriesIds(String categoriesIds) {
		this.categoriesIds = categoriesIds;
	}

	public String getOfferingType() {
		return offeringType;
	}

	public void setOfferingType(String offeringType) {
		this.offeringType = offeringType;
	}

	public Dimension getDimension() {
		return dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public String getCommercialConditionId() {
		return commercialConditionId;
	}

	public void setCommercialConditionId(String commercialConditionId) {
		this.commercialConditionId = commercialConditionId;
	}

	public String getProductClusterId() {
		return productClusterId;
	}

	public void setProductClusterId(String productClusterId) {
		this.productClusterId = productClusterId;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getOfferingInfo() {
		return offeringInfo;
	}

	public void setOfferingInfo(String offeringInfo) {
		this.offeringInfo = offeringInfo;
	}

	public String getOfferingTypeId() {
		return offeringTypeId;
	}

	public void setOfferingTypeId(String offeringTypeId) {
		this.offeringTypeId = offeringTypeId;
	}

	@Override
	public String toString() {
		return "ClassPojo [categoriesIds = " + categoriesIds + ", offeringType = " + offeringType + ", dimension = "
				+ dimension + ", commercialConditionId = " + commercialConditionId + ", productClusterId = "
				+ productClusterId + ", brandId = " + brandId + ", brandName = " + brandName + ", offeringInfo = "
				+ offeringInfo + ", offeringTypeId = " + offeringTypeId + "]";
	}
}
