package co.com.ecommerce.amelissa.facturacion.dto;

public class RatesAndBenefitsData {
	private String id;
	private RateAndBenefitsIdentifiers[] rateAndBenefitsIdentifiers;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public RateAndBenefitsIdentifiers[] getRateAndBenefitsIdentifiers() {
		return rateAndBenefitsIdentifiers;
	}

	public void setRateAndBenefitsIdentifiers(RateAndBenefitsIdentifiers[] rateAndBenefitsIdentifiers) {
		this.rateAndBenefitsIdentifiers = rateAndBenefitsIdentifiers;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", rateAndBenefitsIdentifiers = " + rateAndBenefitsIdentifiers + "]";
	}
}