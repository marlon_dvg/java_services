package co.com.ecommerce.amelissa.facturacion.dto;

import java.util.Arrays;

public class LogisticsInfo {
	private String itemIndex;
	private String selectedSla;
	private String lockTTL;
	private String price;
	private String listPrice;
	private String sellingPrice;
	private String deliveryWindow;
	private String deliveryCompany;
	private String shippingEstimate;
	private String shippingEstimateDate;
	private Slas[] slas;
	private String[] shipsTo;
	private DeliveryIds[] deliveryIds;
	private String deliveryChannel;
	private PickupStoreInfo pickupStoreInfo;
	private String addressId;
	private String polygonName;

	public String getSelectedSla() {
		return selectedSla;
	}

	public void setSelectedSla(String selectedSla) {
		this.selectedSla = selectedSla;
	}

	public String getDeliveryWindow() {
		return deliveryWindow;
	}

	public void setDeliveryWindow(String deliveryWindow) {
		this.deliveryWindow = deliveryWindow;
	}

	public String getShippingEstimate() {
		return shippingEstimate;
	}

	public void setShippingEstimate(String shippingEstimate) {
		this.shippingEstimate = shippingEstimate;
	}

	public String getItemIndex() {
		return itemIndex;
	}

	public void setItemIndex(String itemIndex) {
		this.itemIndex = itemIndex;
	}

	public String getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public Slas[] getSlas() {
		return slas;
	}

	public void setSlas(Slas[] slas) {
		this.slas = slas;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getLockTTL() {
		return lockTTL;
	}

	public void setLockTTL(String lockTTL) {
		this.lockTTL = lockTTL;
	}

	public String getListPrice() {
		return listPrice;
	}

	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}

	public String getShippingEstimateDate() {
		return shippingEstimateDate;
	}

	public void setShippingEstimateDate(String shippingEstimateDate) {
		this.shippingEstimateDate = shippingEstimateDate;
	}

	public DeliveryIds[] getDeliveryIds() {
		return deliveryIds;
	}

	public void setDeliveryIds(DeliveryIds[] deliveryIds) {
		this.deliveryIds = deliveryIds;
	}

	public String getDeliveryCompany() {
		return deliveryCompany;
	}

	public void setDeliveryCompany(String deliveryCompany) {
		this.deliveryCompany = deliveryCompany;
	}

	public String[] getShipsTo() {
		return shipsTo;
	}

	public void setShipsTo(String[] shipsTo) {
		this.shipsTo = shipsTo;
	}

	public String getDeliveryChannel() {
		return deliveryChannel;
	}

	public void setDeliveryChannel(String deliveryChannel) {
		this.deliveryChannel = deliveryChannel;
	}

	public PickupStoreInfo getPickupStoreInfo() {
		return pickupStoreInfo;
	}

	public void setPickupStoreInfo(PickupStoreInfo pickupStoreInfo) {
		this.pickupStoreInfo = pickupStoreInfo;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getPolygonName() {
		return polygonName;
	}

	public void setPolygonName(String polygonName) {
		this.polygonName = polygonName;
	}

	@Override
	public String toString() {
		return "LogisticsInfo [itemIndex=" + itemIndex + ", selectedSla=" + selectedSla + ", lockTTL=" + lockTTL
				+ ", price=" + price + ", listPrice=" + listPrice + ", sellingPrice=" + sellingPrice
				+ ", deliveryWindow=" + deliveryWindow + ", deliveryCompany=" + deliveryCompany + ", shippingEstimate="
				+ shippingEstimate + ", shippingEstimateDate=" + shippingEstimateDate + ", slas="
				+ Arrays.toString(slas) + ", shipsTo=" + Arrays.toString(shipsTo) + ", deliveryIds="
				+ Arrays.toString(deliveryIds) + ", deliveryChannel=" + deliveryChannel + ", pickupStoreInfo="
				+ pickupStoreInfo + ", addressId=" + addressId + ", polygonName=" + polygonName + "]";
	}
}
