package co.com.ecommerce.amelissa.facturacion.dto;

public class ClientProfileData {
	private String idOrden;
	private String id;
	private String email;
	private String firstName;
	private String lastName;
	private String documentType;
	private String document;
	private String phone;
	private String corporateName;
	private String tradeName;
	private String corporateDocument;
	private String stateInscription;
	private String corporatePhone;
	private String isCorporate;
	private String userProfileId;
	private String customerClass;

	public String getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(String idOrden) {
		this.idOrden = idOrden;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCorporatePhone() {
		return corporatePhone;
	}

	public void setCorporatePhone(String corporatePhone) {
		this.corporatePhone = corporatePhone;
	}

	public String getUserProfileId() {
		return userProfileId;
	}

	public void setUserProfileId(String userProfileId) {
		this.userProfileId = userProfileId;
	}

	public String getCorporateDocument() {
		return corporateDocument;
	}

	public void setCorporateDocument(String corporateDocument) {
		this.corporateDocument = corporateDocument;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getIsCorporate() {
		return isCorporate;
	}

	public void setIsCorporate(String isCorporate) {
		this.isCorporate = isCorporate;
	}

	public String getStateInscription() {
		return stateInscription;
	}

	public void setStateInscription(String stateInscription) {
		this.stateInscription = stateInscription;
	}

	public String getCorporateName() {
		return corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getCustomerClass() {
		return customerClass;
	}

	public void setCustomerClass(String customerClass) {
		this.customerClass = customerClass;
	}

	@Override
	public String toString() {
		return "ClientProfileData [idOrden=" + idOrden + ", id=" + id + ", email=" + email + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", documentType=" + documentType + ", document=" + document + ", phone="
				+ phone + ", corporateName=" + corporateName + ", tradeName=" + tradeName + ", corporateDocument="
				+ corporateDocument + ", stateInscription=" + stateInscription + ", corporatePhone=" + corporatePhone
				+ ", isCorporate=" + isCorporate + ", userProfileId=" + userProfileId + ", customerClass="
				+ customerClass + "]";
	}
}
