package co.com.ecommerce.amelissa.facturacion.dto;

public class ItemAttachment
{
	private String idItem;
    private String content;
    private String name;

    public String getIdItem() {
		return idItem;
	}

	public void setIdItem(String idItem) {
		this.idItem = idItem;
	}

	public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [content = "+content+", name = "+name+"]";
    }
}
