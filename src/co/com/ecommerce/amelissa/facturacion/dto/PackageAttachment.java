package co.com.ecommerce.amelissa.facturacion.dto;

public class PackageAttachment
{
    private String[] packages;

    public String[] getPackages ()
    {
        return packages;
    }

    public void setPackages (String[] packages)
    {
        this.packages = packages;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [packages = "+packages+"]";
    }
}
	