package co.com.ecommerce.amelissa.facturacion.dto;

public class Totals {
	private String idOrden;
	private String id;
	private String name;
	private String value;

	public String getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(String idOrden) {
		this.idOrden = idOrden;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Totals [idOrden=" + idOrden + ", id=" + id + ", name=" + name + ", value=" + value + "]";
	}	
}
