package co.com.ecommerce.amelissa.facturacion.dto;

public class Sellers
{
    private String id;
    private String name;
    private String logo;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLogo ()
    {
        return logo;
    }

    public void setLogo (String logo)
    {
        this.logo = logo;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", logo = "+logo+", name = "+name+"]";
    }
}
