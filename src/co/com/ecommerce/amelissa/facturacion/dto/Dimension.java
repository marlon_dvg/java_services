package co.com.ecommerce.amelissa.facturacion.dto;

public class Dimension
{
	private String idItem;
    private String cubicweight;
    private String height;
    private String length;
    private String weight;
    private String width;

    public String getIdItem() {
		return idItem;
	}

	public void setIdItem(String idItem) {
		this.idItem = idItem;
	}

	public String getCubicweight ()
    {
        return cubicweight;
    }

    public void setCubicweight (String cubicweight)
    {
        this.cubicweight = cubicweight;
    }

    public String getWeight ()
    {
        return weight;
    }

    public void setWeight (String weight)
    {
        this.weight = weight;
    }

    public String getHeight ()
    {
        return height;
    }

    public void setHeight (String height)
    {
        this.height = height;
    }

    public String getWidth ()
    {
        return width;
    }

    public void setWidth (String width)
    {
        this.width = width;
    }

    public String getLength ()
    {
        return length;
    }

    public void setLength (String length)
    {
        this.length = length;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cubicweight = "+cubicweight+", weight = "+weight+", height = "+height+", width = "+width+", length = "+length+"]";
    }
}
