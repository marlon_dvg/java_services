package co.com.ecommerce.amelissa.facturacion.dto;

public class OrderTrakingDTO {
	
	private String numeroFactura;
	private int valorTotalFactura;
	private String fechaEmision;
	private String trackingNumber;
	private String trackingUrl;
	
	public String getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getTrackingUrl() {
		return trackingUrl;
	}
	public void setTrackingUrl(String trackingUrl) {
		this.trackingUrl = trackingUrl;
	}
	public int getValorTotalFactura() {
		return valorTotalFactura;
	}
	public void setValorTotalFactura(int valorTotalFactura) {
		this.valorTotalFactura = valorTotalFactura;
	}
}
