package co.com.ecommerce.amelissa.facturacion.dto;

public class PickupStoreInfo {
	private String additionalInfo;
	private String address;
	private String dockId;
	private String friendlyName;
	private String isPickupStore;

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDockId() {
		return dockId;
	}

	public void setDockId(String dockId) {
		this.dockId = dockId;
	}

	public String getFriendlyName() {
		return friendlyName;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public String getIsPickupStore() {
		return isPickupStore;
	}

	public void setIsPickupStore(String isPickupStore) {
		this.isPickupStore = isPickupStore;
	}

	@Override
	public String toString() {
		return "PickupStoreInfo [additionalInfo=" + additionalInfo + ", address=" + address + ", dockId=" + dockId
				+ ", friendlyName=" + friendlyName + ", isPickupStore=" + isPickupStore + "]";
	}
}
