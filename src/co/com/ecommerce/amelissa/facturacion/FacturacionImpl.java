package co.com.ecommerce.amelissa.facturacion;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
//import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.cert.X509Certificate;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.jws.WebMethod;
import javax.jws.WebService;
///////////////////////
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import co.com.ecommerce.amelissa.facturacion.dto.AdditionalInfo;
import co.com.ecommerce.amelissa.facturacion.dto.Address;
import co.com.ecommerce.amelissa.facturacion.dto.ClientProfileData;
import co.com.ecommerce.amelissa.facturacion.dto.CurrencyFormatInfo;
import co.com.ecommerce.amelissa.facturacion.dto.DeliveryIds;
import co.com.ecommerce.amelissa.facturacion.dto.Dimension;
import co.com.ecommerce.amelissa.facturacion.dto.ItemAttachment;
import co.com.ecommerce.amelissa.facturacion.dto.Items;
import co.com.ecommerce.amelissa.facturacion.dto.LogisticsInfo;
import co.com.ecommerce.amelissa.facturacion.dto.MarketPlace;
import co.com.ecommerce.amelissa.facturacion.dto.MatchedParameters;
import co.com.ecommerce.amelissa.facturacion.dto.OrdenDTO;
import co.com.ecommerce.amelissa.facturacion.dto.OrderTrakingDTO;
import co.com.ecommerce.amelissa.facturacion.dto.PackageAttachment;
import co.com.ecommerce.amelissa.facturacion.dto.PaymentData;
import co.com.ecommerce.amelissa.facturacion.dto.Payments;
import co.com.ecommerce.amelissa.facturacion.dto.PickupStoreInfo;
import co.com.ecommerce.amelissa.facturacion.dto.PriceTags;
import co.com.ecommerce.amelissa.facturacion.dto.RateAndBenefitsIdentifiers;
import co.com.ecommerce.amelissa.facturacion.dto.RatesAndBenefitsData;
import co.com.ecommerce.amelissa.facturacion.dto.SelectedAddresses;
import co.com.ecommerce.amelissa.facturacion.dto.Sellers;
import co.com.ecommerce.amelissa.facturacion.dto.ShippingData;
import co.com.ecommerce.amelissa.facturacion.dto.Slas;
import co.com.ecommerce.amelissa.facturacion.dto.StorePreferencesData;
import co.com.ecommerce.amelissa.facturacion.dto.Totals;
import co.com.ecommerce.amelissa.facturacion.dto.Transactions;
import co.com.ecommerce.amelissa.facturacion.dto.envia.Envia;
import co.com.util.ConnectionBD;
import co.com.util.LogsManager;
/////////////////////

@WebService(endpointInterface = "co.com.ecommerce.amelissa.facturacion.Facturacion", name = "Facturacion", portName="FacturacionImplPort", serviceName="FacturacionImplService")
public class FacturacionImpl implements Facturacion {
	
	private static Logger logger;
	
	@SuppressWarnings({ "static-access", "resource" })
	@Override
	@WebMethod(operationName="envioOrden")
	public boolean envioOrden(OrdenDTO orden) {
		
		Connection connection = null;
		CallableStatement callableStatement = null; 
		boolean exito = false;
		long identity = 0;
		
		logger = LogsManager.getInstance();
		logger.info("SERVICES ----- Inicia invocaci�n servicio " + orden.getOrderId());
		System.out.println("SERVICES ----- Inicia invocaci�n servicio " + orden.getOrderId());
		
		try{			
			ConnectionBD conn = new ConnectionBD();
			connection = conn.getConnectionSQLServerEcommerce();

			callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Orden ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ");
			
			callableStatement.registerOutParameter("id_orden", Types.INTEGER);
			
			String isCompleted = orden.getIsCompleted() == null ? "0": (orden.getIsCompleted().equals("true") ? "1" : "0");
			String allowCancellation = orden.getAllowCancellation() == null ? "0": (orden.getIsCompleted().equals("true") ? "1" : "0");
			String allowEdition = orden.getAllowEdition() == null ? "0": (orden.getIsCompleted().equals("true") ? "1" : "0");
			String isCheckedIn = orden.getIsCheckedIn() == null ? "0": (orden.getIsCompleted().equals("true") ? "1" : "0");
						
			callableStatement.setString("orderId",orden.getOrderId());
			callableStatement.setString("sequence",orden.getSequence());
			callableStatement.setString("marketplaceOrderId",orden.getMarketplaceOrderId());
			callableStatement.setString("marketplaceServicesEndpoint",orden.getMarketplaceServicesEndpoint());
			callableStatement.setString("sellerOrderId",orden.getSellerOrderId());
			callableStatement.setString("origin",orden.getOrigin());
			callableStatement.setString("affiliateId",orden.getAffiliateId());
			callableStatement.setString("salesChannel",orden.getSalesChannel());
			callableStatement.setString("merchantName",orden.getMerchantName());
			callableStatement.setString("status",orden.getStatus());			
			callableStatement.setString("statusDescription",orden.getStatusDescription());
			callableStatement.setString("value",orden.getValue());
			callableStatement.setString("creationDate",orden.getCreationDate());
			callableStatement.setString("lastChange",orden.getLastChange());
			callableStatement.setString("orderGroup",orden.getOrderGroup());
			callableStatement.setString("giftRegistryData",orden.getGiftRegistryData());
			callableStatement.setString("marketingData",orden.getMarketingData());
			callableStatement.setString("callCenterOperatorData",orden.getCallCenterOperatorData());
			callableStatement.setString("followUpEmail",orden.getFollowUpEmail());
			callableStatement.setString("lastMessage",orden.getLastMessage());
			callableStatement.setString("hostname",orden.getHostname());
			callableStatement.setString("invoiceData",orden.getInvoiceData());
			callableStatement.setString("changesAttachment",orden.getChangesAttachment());
			callableStatement.setString("openTextField",orden.getOpenTextField());
			callableStatement.setString("roundingError",orden.getRoundingError());
			callableStatement.setString("orderFormId",orden.getOrderFormId());
			callableStatement.setString("commercialConditionData",orden.getCommercialConditionData());			
			callableStatement.setString("isCompleted",isCompleted);
			callableStatement.setString("customData",orden.getCustomData());
			callableStatement.setString("allowCancellation",allowCancellation);
			callableStatement.setString("allowEdition",allowEdition);
			callableStatement.setString("isCheckedIn",isCheckedIn);
			callableStatement.setString("authorizedDate",orden.getAuthorizedDate());
			callableStatement.setString("invoicedDate",orden.getInvoicedDate());

			callableStatement.execute();
			
			identity = callableStatement.getLong("id_orden");
			
			//Totals
			Totals[] totals = orden.getTotals();			
			if (totals != null) {
				for (int i = 0; i < totals.length; i++) {
					
					callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Totals ?,?,?,? ");
					
					Totals total = totals[i];
					
					callableStatement.setLong("id_orden",identity);
					callableStatement.setString("id",total.getId());
					callableStatement.setString("name",total.getName());
					callableStatement.setString("value",total.getValue());
					
					callableStatement.execute();
				}
			}
			
			//Items
		    Items[] items = orden.getItems();
		    if (items != null) {
			    for (int i = 0; i < items.length; i++) {
			    	
			    	callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Items ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ");
			    	
			    	Items item = items[i];
			    	
			    	callableStatement.setLong("id_orden",identity);
			    	callableStatement.setString("uniqueId",item.getUniqueId());
			    	callableStatement.setString("id",item.getId());
			    	callableStatement.setString("productId",item.getProductId());
			    	callableStatement.setString("ean",item.getEan());
			    	callableStatement.setString("lockId",item.getLockId());
			    	callableStatement.setString("quantity",item.getQuantity());
			    	callableStatement.setString("seller",item.getSeller());
			    	callableStatement.setString("name",item.getName());
			    	callableStatement.setString("refId",item.getRefId());
			    	callableStatement.setString("price",item.getPrice());
			    	callableStatement.setString("listPrice",item.getListPrice());
			    	callableStatement.setString("manualPrice",item.getManualPrice());
			    	callableStatement.setString("imageUrl",item.getImageUrl());
			    	callableStatement.setString("detailUrl",item.getDetailUrl());
			    	callableStatement.setString("sellerSku",item.getSellerSku());
			    	callableStatement.setString("pricevalidUntil",item.getPriceValidUntil());
			    	callableStatement.setString("commission",item.getCommission());
			    	callableStatement.setString("tax",item.getTax());
			    	callableStatement.setString("preSaleDate",item.getPreSaleDate());
			    	callableStatement.setString("measurementUnit",item.getMeasurementUnit());
			    	callableStatement.setString("unitMultiplier",item.getUnitMultiplier());
			    	callableStatement.setString("sellingPrice",item.getSellingPrice());
			    	callableStatement.setString("isGift",item.getIsGift());
			    	callableStatement.setString("shippingPrice",item.getShippingPrice());
			    	callableStatement.setString("rewardValue",item.getRewardValue());
			    	callableStatement.setString("freightCommission",item.getFreightCommission());
			    	callableStatement.setString("priceDefinitions",item.getPriceDefinitions());
			    	callableStatement.setString("taxCode",item.getTaxCode());

					callableStatement.execute();
					
					ItemAttachment itemAttachment = item.getItemAttachment();
					
					if (itemAttachment != null) {
						callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Itemattachment ?,?,? ");
						
						callableStatement.setString("id_item",item.getId());
				    	callableStatement.setString("content",itemAttachment.getContent());
				    	callableStatement.setString("name",itemAttachment.getName());
				    	
				    	callableStatement.execute();
					}
										
					PriceTags[] priceTags = item.getPriceTags();
					if (priceTags != null) {
						for (int j = 0; j < priceTags.length; j++) {
							
							callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Pricetags ?,?,?,?,?,? ");
							
							PriceTags priceTag = priceTags[j];
							
							callableStatement.setString("id_item",item.getId());
							callableStatement.setString("name",priceTag.getName());
							callableStatement.setString("value",priceTag.getValue());
							callableStatement.setString("isPercentual",priceTag.getIsPercentual());
							callableStatement.setString("identifier",priceTag.getIdentifier());
							callableStatement.setString("rawValue",priceTag.getRawValue());
							
							callableStatement.execute();
						}
					}
					
					AdditionalInfo additionalInfo = item.getAdditionalInfo();
					if (additionalInfo != null) {
						
						callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_AdditionalInfo ?,?,?,?,?,?,?,?,? ");
						
						callableStatement.setString("id_item",item.getId());
						callableStatement.setString("brandName",additionalInfo.getBrandName());
						callableStatement.setString("brandId",additionalInfo.getBrandId());
						callableStatement.setString("categoriesIds",additionalInfo.getCategoriesIds());
						callableStatement.setString("productClusterId",additionalInfo.getProductClusterId());
						callableStatement.setString("commercialConditionId",additionalInfo.getCommercialConditionId());
						callableStatement.setString("offeringInfo",additionalInfo.getOfferingInfo());
						callableStatement.setString("offeringType",additionalInfo.getOfferingType());
						callableStatement.setString("offeringTypeId",additionalInfo.getOfferingTypeId());
						
						callableStatement.execute();
												
						Dimension dimension = additionalInfo.getDimension();
						if (dimension != null) {
							
							callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Dimension ?,?,?,?,?,? ");
							
							callableStatement.setString("id_item",item.getId());
							callableStatement.setString("cubicweight",dimension.getCubicweight());
							callableStatement.setString("height",dimension.getHeight());
							callableStatement.setString("length",dimension.getLength());
							callableStatement.setString("weight",dimension.getWeight());
							callableStatement.setString("width",dimension.getWidth());
							
							callableStatement.execute();
						}
					}
				}
			}

		    ClientProfileData clientProfileData = orden.getClientProfileData();
		    if (clientProfileData != null) {
		    	
		    	callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Clientprofiledata ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ");
		    	
		    	callableStatement.setLong("id_orden",identity);
		    	callableStatement.setString("id",clientProfileData.getIdOrden());
		    	callableStatement.setString("email",clientProfileData.getEmail());
		    	callableStatement.setString("firstName",clientProfileData.getFirstName());
		    	callableStatement.setString("lastName",clientProfileData.getLastName());
		    	callableStatement.setString("documentType",clientProfileData.getDocumentType());
		    	callableStatement.setString("document",clientProfileData.getDocument());
		    	callableStatement.setString("phone",clientProfileData.getPhone());
		    	callableStatement.setString("corporateName",clientProfileData.getCorporateName());
		    	callableStatement.setString("tradeName",clientProfileData.getTradeName());
		    	callableStatement.setString("corporateDocument",clientProfileData.getCorporateDocument());
		    	callableStatement.setString("stateInscription",clientProfileData.getStateInscription());
		    	callableStatement.setString("corporatePhone",clientProfileData.getCorporatePhone());
		    	callableStatement.setString("isCorporate",clientProfileData.getIsCorporate());
		    	callableStatement.setString("userProfileId",clientProfileData.getUserProfileId());
		    	callableStatement.setString("customerClass",clientProfileData.getCustomerClass());
		    	
		    	callableStatement.execute();
		    }
		    
		    RatesAndBenefitsData ratesAndBenefitsData = orden.getRatesAndBenefitsData();
		    
		    if (ratesAndBenefitsData != null) {
		    	
			    callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Ratesandbenefitsdata ?,? ");
		    	
		    	callableStatement.setLong("id_orden",identity);
		    	callableStatement.setString("id",ratesAndBenefitsData.getId());
			    
		    	callableStatement.execute();
		    	
			    RateAndBenefitsIdentifiers[] rateAndBenefitsIdentifiers = ratesAndBenefitsData.getRateAndBenefitsIdentifiers();
			    if (rateAndBenefitsIdentifiers != null) {
				    for (int j = 0; j < rateAndBenefitsIdentifiers.length; j++) {
				    	RateAndBenefitsIdentifiers rateAndBenefitsIdentifier = rateAndBenefitsIdentifiers[j];
				    	rateAndBenefitsIdentifier.getId();
				    	
				    	callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Rateandbenefitsidentifiers ?,?,?,?,? ");
				    	
				    	callableStatement.setString("id_rateandbenefitsdata",ratesAndBenefitsData.getId());
				    	callableStatement.setString("id",rateAndBenefitsIdentifier.getId());
				    	callableStatement.setString("description",rateAndBenefitsIdentifier.getDescription());
				    	callableStatement.setString("name",rateAndBenefitsIdentifier.getName());
				    	callableStatement.setString("featured",rateAndBenefitsIdentifier.getFeatured());
				    	
				    	callableStatement.execute();
				    	
				    	MatchedParameters matchedParameters = rateAndBenefitsIdentifier.getMatchedParameters();
				    	
				    	if(matchedParameters != null) {
				    		callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_MatchedParameters ?,?");
				    		callableStatement.setString("id_rateandbenefitsidentifier",rateAndBenefitsIdentifier.getId());
				    		callableStatement.setString("slalds", matchedParameters.getSlalds());
				    		
				    		callableStatement.execute();
				    	}
					}				    
			    }
		    }
		    
		    ShippingData shippingData = orden.getShippingData();
		    
		    if (shippingData != null) {
			    callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_ShippingData ?,?,? ");
		    	
		    	callableStatement.setLong("id_orden",identity);
		    	callableStatement.setString("id",shippingData.getId());
		    	callableStatement.setString("trackinghints",shippingData.getTrackingHints());
		    	
		    	callableStatement.execute();			    
		    	
			    Address address = shippingData.getAddress();
			    if (address != null) {
			    	callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Address ?,?,?,?,?,?,?,?,?,?,?,?,? ");
			    	
			    	callableStatement.setLong("id_orden",identity);
			    	callableStatement.setString("addresstype",address.getAddressType());
			    	callableStatement.setString("receivername",address.getReceiverName());
			    	callableStatement.setString("addressId",address.getAddressId());
			    	callableStatement.setString("postalcode",address.getPostalCode());
			    	callableStatement.setString("city",address.getCity());
			    	callableStatement.setString("state",address.getState());
			    	callableStatement.setString("country",address.getCountry());
			    	callableStatement.setString("street",address.getStreet());
			    	callableStatement.setString("number",address.getNumber());
			    	callableStatement.setString("neighborhood",address.getNeighborhood());
			    	callableStatement.setString("complement",address.getComplement());
			    	callableStatement.setString("reference",address.getReference());			    	
			    	
			    	callableStatement.execute();
			    	
			    	String[] geoCoordinates = address.getGeoCoordinates();
			    	if(geoCoordinates != null) {
			    		for (int i = 0; i < geoCoordinates.length; i++) {
							String coordinates = geoCoordinates[i];
							
							callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_GeoCoordinates ?,?,?");
							
							callableStatement.setLong("id_orden",identity);
							callableStatement.setString("addressId",address.getAddressId());
							callableStatement.setString("geoCoordinates",coordinates);
							
							callableStatement.execute();
						}
			    	}
			    }
			    
			    LogisticsInfo[] logisticsInfo = shippingData.getLogisticsInfo();
			    if (logisticsInfo != null) {
				    for (int i = 0; i < logisticsInfo.length; i++) {
				    	LogisticsInfo logistics = logisticsInfo[i];
				    	
				    	callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Logisticsinfo ?,?,?,?,?,?,?,?,?,?,?,?,?,? ");
				    	
				    	callableStatement.setLong("id_orden",identity);
				    	callableStatement.setString("itemindex",logistics.getItemIndex());
				    	callableStatement.setString("selectedsla",logistics.getSelectedSla());
				    	callableStatement.setString("lockttl",logistics.getLockTTL());
				    	callableStatement.setString("price",logistics.getPrice());
				    	callableStatement.setString("listprice",logistics.getListPrice());
				    	callableStatement.setString("sellingprice",logistics.getSellingPrice());
				    	callableStatement.setString("deliverywindow",logistics.getDeliveryWindow());
				    	callableStatement.setString("deliverycompany",logistics.getDeliveryCompany());
				    	callableStatement.setString("shippingestimate",logistics.getShippingEstimate());
				    	callableStatement.setString("shippingestimatedate",logistics.getShippingEstimateDate());
				    	callableStatement.setString("delyveryChannel",logistics.getDeliveryChannel());
				    	callableStatement.setString("addressId",logistics.getAddressId());
				    	callableStatement.setString("polygonName",logistics.getPolygonName());
				    	
				    	callableStatement.execute();
				    	
				    	Slas[] slas = logistics.getSlas();
				    	if (slas != null) {
					    	for (int j = 0; j < slas.length; j++) {
					    		Slas sla = slas[j];
					    		
					    		callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Slas ?,?,?,?,?,?,?,? ");
						    	
						    	callableStatement.setLong("id_orden",identity);
						    	callableStatement.setString("id",sla.getId());
						    	callableStatement.setString("name",sla.getName());
						    	callableStatement.setString("shippingEstimate",sla.getShippingEstimate());
						    	callableStatement.setString("deliveryWindow",sla.getDeliveryWindow());
						    	callableStatement.setString("price",sla.getPrice());
						    	callableStatement.setString("deliveryChannel",sla.getDeliveryChannel());
						    	callableStatement.setString("polygonName",sla.getPolygonName());
						    	
						    	callableStatement.execute();
						    	
						    	PickupStoreInfo pickupStoreInfo = sla.getPickupStoreInfo();
						    	if(pickupStoreInfo != null) {
						    		callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_PickupStoreInfo ?,?,?,?,?,?,? ");
							    	
						    		String isPickupStore = pickupStoreInfo.getIsPickupStore() == null ? "0": (pickupStoreInfo.getIsPickupStore().equals("true") ? "1" : "0");
						    		
							    	callableStatement.setLong("id_orden",identity);
							    	callableStatement.setString("sla_id",sla.getId());
							    	callableStatement.setString("additionalInfo",pickupStoreInfo.getAdditionalInfo());
							    	callableStatement.setString("address",pickupStoreInfo.getAddress());
							    	callableStatement.setString("dockId",pickupStoreInfo.getDockId());
							    	callableStatement.setString("friendlyName",pickupStoreInfo.getFriendlyName());
							    	callableStatement.setString("isPickupStore",isPickupStore);
							    	
							    	callableStatement.execute();
							    }						    							    	
							}
				    	}
				    	
				    	String[] string = logistics.getShipsTo();
				    	if (string != null) {
				    		for (int j = 0; j < string.length; j++) {
				    			callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Shipsto ?,? ");
				    			
				    			callableStatement.setLong("id_orden",identity);
				    			callableStatement.setString("value",string[j]);
				    			
				    			callableStatement.execute();
				    		}
				    	}
				    	
				    	DeliveryIds[] deliveryIds = logistics.getDeliveryIds();
				    	if (deliveryIds != null) {
					    	for (int j = 0; j < deliveryIds.length; j++) {
					    		DeliveryIds delivery = deliveryIds[j];
					    		
					    		callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Deliveryids ?,?,?,?,?,? ");
						    	
						    	callableStatement.setLong("id_orden",identity);
						    	callableStatement.setString("courierid",delivery.getCourierId());
						    	callableStatement.setString("quantity",delivery.getQuantity());
						    	callableStatement.setString("warehouseid",delivery.getWarehouseId());
						    	callableStatement.setString("dockid",delivery.getDockId());
						    	callableStatement.setString("couriername",delivery.getCourierName());
						    	
						    	callableStatement.execute();
							}
				    	}	
				    	
				    	PickupStoreInfo pickupStoreInfo2 = logistics.getPickupStoreInfo();
				    	if(pickupStoreInfo2 != null) {
				    		callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_PickupStoreInfo ?,?,?,?,?,?,? ");
					    	
				    		String isPickupStore2 = pickupStoreInfo2.getIsPickupStore() == null ? "0": (pickupStoreInfo2.getIsPickupStore().equals("true") ? "1" : "0");
				    		
					    	callableStatement.setLong("id_orden",identity);
					    	callableStatement.setString("sla_id",logistics.getAddressId());
					    	callableStatement.setString("additionalInfo",pickupStoreInfo2.getAdditionalInfo());
					    	callableStatement.setString("address",pickupStoreInfo2.getAddress());
					    	callableStatement.setString("dockId",pickupStoreInfo2.getDockId());
					    	callableStatement.setString("friendlyName",pickupStoreInfo2.getFriendlyName());
					    	callableStatement.setString("isPickupStore",isPickupStore2);
					    	
					    	callableStatement.execute();
					    }	
					}
			    }
			    
			    SelectedAddresses[] selectedAddresses = shippingData.getSelectedAddresses();
			    if(selectedAddresses != null) {
			    	for (int i = 0; i < selectedAddresses.length; i++) {
			    		SelectedAddresses selectedAddress = selectedAddresses[i];
			    		
			    		callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_SelectedAddresses ?,?,?,?,?,?,?,?,?,?,?,?,? ");
				    	
				    	callableStatement.setLong("id_orden",identity);
				    	callableStatement.setString("addressId",selectedAddress.getAddressId());
				    	callableStatement.setString("addressType",selectedAddress.getAddressType());
				    	callableStatement.setString("receiverName",selectedAddress.getReceiverName());
				    	callableStatement.setString("street",selectedAddress.getStreet());
				    	callableStatement.setString("number",selectedAddress.getNumber());
				    	callableStatement.setString("complement",selectedAddress.getComplement());
				    	callableStatement.setString("neighborhood",selectedAddress.getNeighborhood());
				    	callableStatement.setString("postalCode",selectedAddress.getPostalCode());
				    	callableStatement.setString("city",selectedAddress.getCity());
				    	callableStatement.setString("state",selectedAddress.getState());
				    	callableStatement.setString("country",selectedAddress.getCountry());
				    	callableStatement.setString("reference",selectedAddress.getReference());

				    	callableStatement.execute();
				    	
				    	String[] geoCoordinates2 = selectedAddress.getGeoCoordinates();
				    	if(geoCoordinates2 != null) {
				    		for (int j = 0; j < geoCoordinates2.length; j++) {
								String coordinates = geoCoordinates2[j];
								
								callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_GeoCoordinates ?,?,?");
								
								callableStatement.setLong("id_orden",identity);
								callableStatement.setString("addressId",selectedAddress.getAddressId());
								callableStatement.setString("geoCoordinates",coordinates);
								
								callableStatement.execute();
							}
				    	}
					}
			    }
		    }
		    		    
		    /*		    
		    String transactionID = "";
		    String formaPago     = "";
		    String franquicia    = "";
		    long valorPagado     = 0;
		    long idOrden         = 0;
		    long cedulaCliente   = 0;
		    
		    try {
		    	valorPagado      = Long.valueOf( orden.getValue().substring(0, (orden.getValue().length()-2) ) );
			} catch (Exception e) {
				valorPagado = 0;
				e.printStackTrace();
			}
		    
		    try {
		    	idOrden          = Long.valueOf( orden.getOrderGroup() );
			} catch (Exception e) {
				valorPagado = 0;
				e.printStackTrace();
			}
		    
		    try {
		    	cedulaCliente    = Long.valueOf(clientProfileData.getDocument());
			} catch (Exception e) {
				e.printStackTrace();
			}
			*/
		    
		    PaymentData paymentData = orden.getPaymentData();
		    if (paymentData != null) {
		    	
		    	Transactions[] transactions = paymentData.getTransactions();
		    	if (transactions != null) {
		    		
		    		for (int i = 0; i < transactions.length; i++) {
		    			
		    			Transactions transaction = transactions[i];
		    			//transactionID = transaction.getTransactionId();
		    			
			    		callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_PaymentData ?,?,? ");
				    	
			    		String isActive = transaction.getIsActive() == null ? "0" : transaction.getIsActive().equals("true") ? "1" : "0"; 
			    		
				    	callableStatement.setLong("id_orden",identity);
				    	callableStatement.setString("transactionid",transaction.getTransactionId());
				    	callableStatement.setString("isactive",isActive);
			    		
				    	callableStatement.execute();
				    	
			    		Payments[] payments = transaction.getPayments();
			    		if (payments != null) {
			    			for (int j = 0; j < payments.length; j++) {
			    				Payments payment = payments[j];
			    				
			    				//formaPago = payment.getPaymentSystem();
			    				//franquicia = payment.getPaymentSystemName();
			    				
			    				callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Payments ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ");
						    	
						    	callableStatement.setString("transactionid",transaction.getTransactionId());
						    	callableStatement.setString("id",payment.getId());
						    	callableStatement.setString("paymentsystem",payment.getPaymentSystem());
						    	callableStatement.setString("paymentsystemname",payment.getPaymentSystemName());
						    	callableStatement.setString("value",payment.getValue());
						    	callableStatement.setString("installments",payment.getInstallments());
						    	callableStatement.setString("referencevalue",payment.getReferenceValue());
						    	callableStatement.setString("cardholder",payment.getCardHolder());
						    	callableStatement.setString("cardnumber",payment.getCardNumber());
						    	callableStatement.setString("firstdigits",payment.getFirstDigits());
						    	callableStatement.setString("lastdigits",payment.getLastDigits());
						    	callableStatement.setString("cvv2",payment.getCvv2());
						    	callableStatement.setString("expiremonth",payment.getExpireMonth());
						    	callableStatement.setString("expireyear",payment.getExpireYear());
						    	callableStatement.setString("url",payment.getUrl());
						    	callableStatement.setString("giftcardid",payment.getGiftCardId());
						    	callableStatement.setString("giftcardname",payment.getGiftCardName());
						    	callableStatement.setString("giftcardcaption",payment.getGiftCardCaption());
						    	callableStatement.setString("redemptioncode",payment.getRedemptionCode());
						    	callableStatement.setString("group",payment.getGroup());
						    	callableStatement.setString("tid",payment.getTid());
						    	callableStatement.setString("duedate",payment.getDueDate());
						    	callableStatement.setString("connectorresponses",payment.getConnectorResponses());
						    							    	
						    	callableStatement.execute();
							}
			    		}
		    		}
		    	}
		    }
		    
		    PackageAttachment packageAttachment = orden.getPackageAttachment();
		    
		    if (packageAttachment != null) {
			    String[] packageAttachments = packageAttachment.getPackages();
			    
			    if (packageAttachments != null) {
			    	for (int j = 0; j < packageAttachments.length; j++) {
						
						callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_PackagesAttachment ?,? ");
				    	callableStatement.setLong("id_orden",identity);
				    	callableStatement.setString("packages",packageAttachments[j]);
				    	
				    	callableStatement.execute();
					}
			    }
		    }
		    
		    /////////////////
		    Sellers[] sellers = orden.getSellers();
		    if (sellers != null) {
			    for (int i = 0; i < sellers.length; i++) {
			    	Sellers seller = sellers[i];
			    	
			    	callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_Sellers ?,?,?,? ");
			    	callableStatement.setLong("id_orden",identity);
			    	callableStatement.setString("id",seller.getId());
			    	callableStatement.setString("logo",seller.getLogo());
			    	callableStatement.setString("name",seller.getName());
			    	
			    	callableStatement.execute();
				}
		    }
		    
		    StorePreferencesData storePreferencesData = orden.getStorePreferencesData(); 
		    if(storePreferencesData != null) {
		    	callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_StorePreferencesData ?,?,?,?,?,? ");
		    	callableStatement.setLong("id_orden",identity);
		    	callableStatement.setString("countryCode",storePreferencesData.getCountryCode());
		    	callableStatement.setString("currencyCode",storePreferencesData.getCurrencyCode());
		    	callableStatement.setString("currencyLocale",storePreferencesData.getCurrencyLocale());
		    	callableStatement.setString("currencySymbol",storePreferencesData.getCurrencySymbol());
		    	callableStatement.setString("timeZone",storePreferencesData.getTimeZone());
		    			    	
		    	callableStatement.execute();
		    	
		    	CurrencyFormatInfo currencyFormatInfo = storePreferencesData.getCurrencyFormatInfo();
		    	if(currencyFormatInfo != null) {
		    		callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_CurrencyFormatInfo ?,?,?,?,?,? ");
			    	callableStatement.setLong("id_orden",identity);
			    	callableStatement.setString("currencyDecimalDigits",currencyFormatInfo.getCurrencyDecimalDigits());
			    	callableStatement.setString("currencyDecimalSeparators",currencyFormatInfo.getCurrencyDecimalSeparators());
			    	callableStatement.setString("currencyGroupSeparator",currencyFormatInfo.getCurrencyGroupSeparator());
			    	callableStatement.setString("currencyGroupSize",currencyFormatInfo.getCurrencyGroupSize());
			    	callableStatement.setString("startsWithCurrencySymbol","1"/*currencyFormatInfo.getStartsWithCurrencySymbol()*/);
			    	
			    	callableStatement.execute();
		    	}
		    }
		    
		    MarketPlace marketPlace = orden.getMarketplace();
		    if(marketPlace != null) {
		    	callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Crud_MarketPlace ?,?,?,? ");
		    	callableStatement.setLong("id_orden",identity);
		    	callableStatement.setString("baseURL",marketPlace.getBaseURL());
		    	callableStatement.setString("isCertified",marketPlace.getIsCertified());
		    	callableStatement.setString("name",marketPlace.getName());
		    	
		    	callableStatement.execute();
		    }		    
		    
		    //VALIDAR SISTEMA DE PEDIDOS ACTIVO
		    boolean ingresaPedido = false;
		    try {
	        	
	        	callableStatement = connection.prepareCall(" EXECUTE util.Proc_Get_Ingresa_Pedido ? ");
	        	callableStatement.registerOutParameter("ingresa_pedido",Types.BOOLEAN);
	            callableStatement.execute();
	            
	            ingresaPedido = callableStatement.getBoolean("ingresa_pedido");
	            
	            System.out.println("INGRESA PEDIDO : " + ingresaPedido);
	            
			} catch (Exception e) {
				System.out.println("ERROR EL CONSULTAR CAMPANA");
				e.printStackTrace();
			}
		    
		    // Se finaliza el registro del pedido procedente desde la tienda
		    connection.commit();
		    
		    
		    if (ingresaPedido) {
		    	
		    	PedidoAmelissa amelissa = new PedidoAmelissa();
		    	amelissa.generarPedido(orden);
		    	
		    } else {
		    	
		    	connection = conn.getConnectionSQLServerEcommerce();
		    	try {
		        	
		        	callableStatement = connection.prepareCall(" EXECUTE amelissa.Proc_Crud_Orden_No_Guardada ? ");
		        	callableStatement.setLong("id_orden",identity);
		            callableStatement.execute();
		            
		            connection.commit();
		            
				} catch (Exception e) {
					System.out.println("ERROR AL CONSULTAR CAMPANA");
					e.printStackTrace();
				}
		    	System.out.println("NO GUARDO PEDIDO");
		    }
		    
		    //Movido al proceso para ejecutarlo despu�s de crear la amelista
		    
		    //INGRESAR PAGO CUENTA CORRIENTE 
	    	/*connection = conn.getConnectionSQLServerAmelissa();
	    	try {
	    		
	    		callableStatement = connection.prepareCall(" EXECUTE ws.Proc_Guardar_PagoWeb_Tienda_Amelissa ?,?,?,?,?,?,?,?,? ");
				callableStatement.setString("id_forma_pago", formaPago);                  //PaymentSystem
				callableStatement.setLong("valor_pagado", valorPagado);                   //Value
				callableStatement.setLong("ticketID", idOrden);                           //IdOrden
				callableStatement.setLong("cedula", cedulaCliente);                       //C�dula
				callableStatement.setString("franquicia", franquicia);                    //Franquicia - PaymentSystemName
				callableStatement.setString("codigo_banco", "");
				callableStatement.setString("nombre_banco", "");
				callableStatement.setString("codigo_transaccion", transactionID);         //TransactionID 
				callableStatement.setString("usuario", "TIENDAAMEL");
				
				callableStatement.execute();
	            
			} catch (Exception e) {
				System.out.println("ERROR INGRESANDO EL PAGO");
				e.printStackTrace();
			}*/
	    	
			//Retorno global de la invocaci�n del servicio
			exito = true;
			
		}catch(Exception e){
			
			e.printStackTrace();
			logger.error("ERROR Servicio " + e);
			System.out.println("SERVICES ----- ERROR Servicio  " + e.getMessage());
			
		}finally{
			
			try{
				if(callableStatement != null){
					callableStatement.close();
				}
			}catch(Exception e){}
			
			try{
				if(connection != null){
					connection.close();
				}
			}catch(Exception e){}
		}
		
		return exito;
	}
		
	@SuppressWarnings("static-access")
	@Override
	@WebMethod(operationName="consultaOrden")
	public OrderTrakingDTO consultaOrden(String nroPedido) throws Exception {
		
		Connection connection = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		
		ConnectionBD connBD = new ConnectionBD();
		connection = connBD.getConnectionSQLServerEcommerce();
		
		OrderTrakingDTO orderTrakingDTO = null;

		try {
			callableStatement = connection.prepareCall("EXECUTE amelissa.Proc_Get_Orden ? ");
			callableStatement.setString("orderid", nroPedido);
			
			resultSet = callableStatement.executeQuery();
			
			String referencia = null;
			String fechaCreacion = null;
			String numeroFactura = null;
			String numeroGuia = null;
			int valorFacturado = 0;
			
			while (resultSet.next()) {
				referencia     = resultSet.getString("REFERENCIA");
				fechaCreacion  = resultSet.getString("FECHA_CREACION");
				valorFacturado = resultSet.getInt("VALOR_FACTURADO");
				numeroFactura  = resultSet.getString("NUMERO_FACTURA");
				numeroGuia  = resultSet.getString("NUMERO_GUIA");
			}
			
			boolean enviaTracking = true;
			
			String url = "http://ws.coordinadora.com/embed/rastreo/1.0/rastreo.php";			
			String apikey = "81d9e2c4-2af1-11e9-b210-d663bd873d93";
			String clave = "D8!dbq7wGpwY?e";
			String datos = numeroGuia+clave;	
			String tok = sha256(datos);
			String tpl = "";
			
			url = url+"?guia="+numeroGuia+"&apikey="+apikey+"&tok="+tok+"&tpl="+tpl;
			System.out.println(url);
			
			if (enviaTracking) {
				orderTrakingDTO = new OrderTrakingDTO();
				orderTrakingDTO.setFechaEmision(fechaCreacion);
				orderTrakingDTO.setNumeroFactura(numeroFactura);				
				orderTrakingDTO.setTrackingNumber(numeroGuia);
				orderTrakingDTO.setTrackingUrl(url);
				orderTrakingDTO.setValorTotalFactura(valorFacturado);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("SERVICES - Inicia invocaci�n servicio " + e.getMessage());
		}
		
		return orderTrakingDTO;
	}
	
	public static String sha256(String base) {
	    try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        byte[] hash = digest.digest(base.getBytes("UTF-8"));
	        StringBuffer hexString = new StringBuffer();

	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }

	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}

}