package co.com.ecommerce.amelissa.facturacion;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import co.com.ecommerce.amelissa.facturacion.dto.OrderTrakingDTO;

public class Client_Get{
	
	public static void main(String[] args) throws Exception {
		
		//URL url = new URL("http://172.17.22.106:8282/Services/FacturacionImplService");
		//URL url = new URL("http://localhost:8282/Services/FacturacionImplService");
		URL url = new URL("http://localhost:8080/Services/FacturacionImplService");
		//URL url = new URL("http://amelissavirtual.amelissa.com:8084/Services/FacturacionImplService");		
		
        QName qname = new QName("http://facturacion.amelissa.ecommerce.com.co/", "FacturacionImplService");
         
        Service service = Service.create(url, qname);
        Facturacion mensaje = service.getPort(Facturacion.class);
        
        FacturacionImpl fac = new FacturacionImpl();
        OrderTrakingDTO orderTrakingDTO = fac.consultaOrden("725591237730-01");
        
        System.out.println(orderTrakingDTO.getFechaEmision());
        System.out.println(orderTrakingDTO.getNumeroFactura());
        System.out.println(orderTrakingDTO.getTrackingNumber());
        System.out.println(orderTrakingDTO.getTrackingUrl());
        System.out.println(orderTrakingDTO.getValorTotalFactura());
        
    }
}
