package co.com.ecommerce.amelissa.facturacion;

import co.com.ecommerce.amelissa.facturacion.dto.OrdenDTO;
import co.com.ecommerce.amelissa.facturacion.threads.Proceso;
import co.com.ecommerce.amelissa.facturacion.threads.Recurrencia;
import co.com.ecommerce.amelissa.facturacion.threads.PedidoRunnable;

public class PedidoAmelissa {
	
	public boolean generarPedido(OrdenDTO orden) {
		
		boolean exito = false;
		
		try {
    		
			Recurrencia recurrencia = new Recurrencia(new int[] {1});
			Proceso proceso = new Proceso();
			proceso.setOrden(orden);
			long initialTime = System.currentTimeMillis();
    		Runnable procesoPedido = new PedidoRunnable(recurrencia, proceso, initialTime);
    		new Thread(procesoPedido).start();
    		
    		exito = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return exito;
	}
}
