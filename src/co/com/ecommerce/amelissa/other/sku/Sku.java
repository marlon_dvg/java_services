package co.com.ecommerce.amelissa.other.sku;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Sku {
	
	@WebMethod
	public boolean actualizarSku(SkuDTO skuDTO);
	
}