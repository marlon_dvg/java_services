package co.com.ecommerce.amelissa.other.catalogo;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Catalogo{
	
	@WebMethod
	public boolean cargaCatalogo(CatalogoDTO catalogoDTO);
	
}