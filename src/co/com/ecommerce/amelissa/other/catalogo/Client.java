package co.com.ecommerce.amelissa.other.catalogo;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client{
	
	public static void main(String[] args) throws Exception {
		
		
		//URL url = new URL("http://172.17.23.32:8084/amelissa_portal_war/IvrSoapAmelissaImplService");
		URL url = new URL("http://localhost:8084/amelissa_portal_war/IvrSoapAmelissaImplService");
		
        QName qname = new QName("http://v2.ivr.amelissa.com.co/", "IvrSoapAmelissaImplService");
        
        Service service = Service.create(url, qname);
        Catalogo mensaje = service.getPort(Catalogo.class);
        
        CatalogoDTO catalogoDTO = new CatalogoDTO();
        
        boolean exito = mensaje.cargaCatalogo(catalogoDTO);
        
		/*
		URL url = new URL("http://dinfppaedwinh:8282/Services/HellowWorldServiceService");
		
        //QName qname = new QName("http://amelissa.ecommerce.com.co/", "HellowWorldServiceService");
		QName qname = new QName("http://amelissa.ecommerce.com.co/", "HellowWorldServiceService");
        
        Service service = Service.create(url, qname);
        
        //HellowWorldServiceService mensaje = service.getPort(HellowWorldServiceService.class);
        service.getPort(qname,"");
        */
        		
    }
}
