package co.com.ecommerce.amelissa.other.marca;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client{
	
	public static void main(String[] args) throws Exception {
		
		
		URL url = new URL("http://172.17.23.32:8084/amelissa_portal_war/IvrSoapAmelissaImplService");
		
        QName qname = new QName("http://v2.ivr.amelissa.com.co/", "IvrSoapAmelissaImplService");
        
        Service service = Service.create(url, qname);
        Marca mensaje = service.getPort(Marca.class);
        
        MarcaDTO marcaDTO = new MarcaDTO();
        
        boolean exito = mensaje.actualizacionMarca(marcaDTO);
        		
    }
}
