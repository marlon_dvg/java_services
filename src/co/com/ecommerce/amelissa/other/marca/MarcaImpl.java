package co.com.ecommerce.amelissa.other.marca;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.sql.DataSource;

@WebService(endpointInterface = "co.com.ecommerce.amelissa.marca.Marca")
public class MarcaImpl implements Marca {
	
	@Override
	public boolean actualizacionMarca(MarcaDTO marcaDTO) {
		
		/*
		InitialContext ic = null;
		DataSource dataSource = null;
		ResultSet resultSet = null;
		Connection connection = null;
		CallableStatement callableStatement = null; 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		AmelistaDTO amelistaDTO = null;
		
		try{
			
			ic = new InitialContext();
			dataSource = (DataSource)ic.lookup("SQLServer2005_raptor");
			
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall("EXECUTE ws.Proc_Ivr_getSaldoAmelista ?,? ");
			callableStatement.setString("cedula", cedula);
			callableStatement.setString("codigoCatalogo", "01");
			
			resultSet = callableStatement.executeQuery();
			
			while(resultSet.next()){
				
				amelistaDTO = new AmelistaDTO();
				
				amelistaDTO.setSaldoAmelista(resultSet.getString("SALDO_AMELISTA"));
				amelistaDTO.setCedulaAmelista(resultSet.getString("CEDULA_AMELISTA"));
				
				amelistaDTO.setNombreAmelista(resultSet.getString("NOMBRE_AMELISTA"));
				
				try {
					amelistaDTO.setFechaVencimento(sdf.format(resultSet.getDate("FECHA_VENCIMIENTO")));
				} catch (Exception e) {
					amelistaDTO.setFechaVencimento("");
					e.printStackTrace();
				}
				
				amelistaDTO.setCedulaGerenteZona(resultSet.getString("CEDULA_GERENTE_ZONA"));
				
				amelistaDTO.setNombreGerenteZona(resultSet.getString("NOMBRE_GERENTE_ZONA"));
				amelistaDTO.setPrimerTelefonoGerenteZona(resultSet.getString("PRIMER_TELEFONO_GERENTE_ZONA"));
				amelistaDTO.setSegundoTelefonoGerenteZona(resultSet.getString("SEGUNDO_TELEFONO_GERENTE_ZONA"));
				amelistaDTO.setPrimerCelularGerenteZona(resultSet.getString("PRIMER_CELULAR_GERENTE_ZONA"));
				amelistaDTO.setSegundoCelularGerenteZona(resultSet.getString("SEGUNDO_CELULAR_GERENTE_ZONA"));
				
				try {
					amelistaDTO.setFechaMomentos(sdf.format(resultSet.getDate("FECHA_MOMENTOS")));
				} catch (Exception e) {
					amelistaDTO.setFechaMomentos("");
					e.printStackTrace();
				}
				
				try {
					amelistaDTO.setFechaCambiosDevolucuiones(sdf.format(resultSet.getDate("FECHA_CAMBIOS_DEVOLUCIONES")));
				} catch (Exception e) {
					amelistaDTO.setFechaCambiosDevolucuiones("");
					e.printStackTrace();
				}
				
				amelistaDTO.setTipoCedula(resultSet.getString("TIPO_CEDULA"));
				amelistaDTO.setEstadoAmelista(resultSet.getString("ESTADO_AMELISTA"));
				amelistaDTO.setSaldoFinanciamiento(resultSet.getString("SALDO_FINANCIACION"));
				amelistaDTO.setPuntaje(resultSet.getString("PUNTAJE"));
				
				try {
					amelistaDTO.setFechaMomentosSiguiente(sdf.format(resultSet.getDate("FECHA_MOMENTOS_SIGUIENTE")));
				} catch (Exception e) {
					amelistaDTO.setFechaMomentosSiguiente("");
					e.printStackTrace();
				}
				
				amelistaDTO.setTipoIngreso(resultSet.getString("TIPO_INGRESO"));
				amelistaDTO.setEstadoPedido(resultSet.getString("ESTADO_PEDIDO"));
				
				try {
					amelistaDTO.setFechaEntregaPedido(sdf.format(resultSet.getDate("FECHA_ENTREGA_PEDIDO")));
				} catch (Exception e) {
					amelistaDTO.setFechaEntregaPedido("");
					e.printStackTrace();
				}
				try {
					amelistaDTO.setFechaIngresoProximoPedido(sdf.format(resultSet.getDate("FECHA_INGRESO_PROXIMO_PEDIDO")));
				} catch (Exception e) {
					amelistaDTO.setFechaIngresoProximoPedido("");
					e.printStackTrace();
				}
			}
			
			if (amelistaDTO == null) {
				throw new Exception("Ingreso");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(resultSet != null){
					resultSet.close();
				}
			}catch(Exception e){}
			
			try{
				if(callableStatement != null){
					callableStatement.close();
				}
			}catch(Exception e){}
			
			try{
				if(connection != null){
					connection.close();
				}
			}catch(Exception e){}
		}
		*/
		return true;
	}
	
	
}