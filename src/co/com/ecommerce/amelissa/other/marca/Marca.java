package co.com.ecommerce.amelissa.other.marca;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Marca{
	
	@WebMethod
	public boolean actualizacionMarca(MarcaDTO marcaDTO);
	
}