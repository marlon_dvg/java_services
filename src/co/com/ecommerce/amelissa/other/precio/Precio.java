package co.com.ecommerce.amelissa.other.precio;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Precio{
	
	@WebMethod
	public boolean actualizarPrecio(PrecioDTO precioDTO);
	
}