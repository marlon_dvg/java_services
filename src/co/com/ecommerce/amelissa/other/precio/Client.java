package co.com.ecommerce.amelissa.other.precio;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client{
	
	public static void main(String[] args) throws Exception {
		
		
		URL url = new URL("http://172.17.23.32:8084/amelissa_portal_war/IvrSoapAmelissaImplService");
		
        QName qname = new QName("http://v2.ivr.amelissa.com.co/", "IvrSoapAmelissaImplService");
        
        Service service = Service.create(url, qname);
        Precio mensaje = service.getPort(Precio.class);
        
        PrecioDTO precioDTO = new PrecioDTO();
        
        boolean exito = mensaje.actualizarPrecio(precioDTO);
        
    }
}
