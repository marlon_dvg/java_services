package co.com.ecommerce.amelissa.other.stock;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Stock{
	
	@WebMethod
	public boolean actualizarStock(StockDTO stockDTO);
	
}