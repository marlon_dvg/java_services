package co.com.ecommerce.amelissa.other.pedido;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Pedido {
	
	@WebMethod
	public boolean descargaPedido(PedidoDTO pedidoDTO);
	
}