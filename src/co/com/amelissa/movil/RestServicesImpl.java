package co.com.amelissa.movil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.naming.InitialContext;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/")
public class RestServicesImpl {

	public RestServicesImpl() {
	}
	
	/** The path to the folder where we want to store the uploaded files */
	private static final String UPLOAD_FOLDER = "c:/uploadedFiles/";

	@Context
	private UriInfo context;

	/**
	 * Returns text response to caller containing uploaded file location
	 * 
	 * @return error response in case of missing parameters an internal exception or
	 *         success response if file has been stored successfully
	 */

	@POST
	@Path("upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(

			@FormDataParam("file") InputStream uploadedInputStream,

			@FormDataParam("file") FormDataContentDisposition fileDetail) { // check if all form parameters are provided
		if (uploadedInputStream == null || fileDetail == null)
			return Response.status(400).entity("Invalid form data").build(); // create our destination folder, if it not
																				// exists
		try {
			createFolderIfNotExists(UPLOAD_FOLDER);
		} catch (SecurityException se) {
			return Response.status(500).entity("Can not create destination folder on server").build();
		}
		String uploadedFileLocation = UPLOAD_FOLDER + fileDetail.getFileName();
		try {
			saveToFile(uploadedInputStream, uploadedFileLocation);
		} catch (IOException e) {
			return Response.status(500).entity("Can not save file").build();
		}
		return Response.status(200).entity("File saved to " + uploadedFileLocation).build();
	}

	/**
	 * Utility method to save InputStream data to target location/file
	 * 
	 * @param inStream - InputStream to be saved
	 * @param target   - full path to destination file
	 */
	private void saveToFile(InputStream inStream, String target) throws IOException {
		OutputStream out = null;
		int read = 0;
		byte[] bytes = new byte[1024];
		out = new FileOutputStream(new File(target));
		while ((read = inStream.read(bytes)) != -1) {
			out.write(bytes, 0, read);
		}
		out.flush();
		out.close();
	}

	/**
	 * Creates a folder to desired location if it not already exists
	 * 
	 * @param dirName - full path to the folder
	 * @throws SecurityException - in case you don't have permission to create the
	 *                           folder
	 */
	private void createFolderIfNotExists(String dirName) throws SecurityException {
		File theDir = new File(dirName);
		if (!theDir.exists()) {
			theDir.mkdir();
		}
	}

	@GET
	@Path("get_amelista/{cedula}")
	@Produces(MediaType.APPLICATION_JSON)
	public Amelista getAmelista(@PathParam("cedula") String cedula) throws Exception {

		InitialContext ic = null;
		DataSource dataSource = null;
		Connection connection = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;

		Amelista amelista = new Amelista();

		try {
			ic = new InitialContext();
			dataSource = (DataSource) ic.lookup("SQLServer2005_raptor");

			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall("EXEC amelissa_movil.getAmelista ?");
			callableStatement.setString("cedula", cedula);

			resultSet = callableStatement.executeQuery();

			if (resultSet.next()) {
				amelista.setCodigo(resultSet.getLong("CODIGO"));
				amelista.setCedula(resultSet.getString("CEDULA"));
				amelista.setPrimerNombre(resultSet.getString("PRIMER_NOMBRE"));
				amelista.setPrimerApellido(resultSet.getString("PRIMER_APELLIDO"));
				amelista.setPrimerCelular(resultSet.getString("PRIMER_CELULAR"));
				amelista.setExiste(true);
			} else {
				amelista.setCodigo(0);
				amelista.setCedula("");
				amelista.setPrimerNombre("");
				amelista.setPrimerApellido("");
				amelista.setPrimerCelular("");
				amelista.setExiste(false);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (Exception e) {
			}
			try {
				if (callableStatement != null) {
					callableStatement.close();
				}
			} catch (Exception e) {
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
			}
		}

		return amelista;
	}

	@POST
	@Path("add_amelista")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addAmelista(Amelista amelista) throws Exception {

		InitialContext ic = null;
		DataSource dataSource = null;
		Connection connection = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;

		String mensaje = "";

		try {
			ic = new InitialContext();
			dataSource = (DataSource) ic.lookup("SQLServer2005_raptor");
			connection = dataSource.getConnection();

			callableStatement = connection.prepareCall("EXEC amelissa_movil.addAmelista ?,?,?,?,?");
			callableStatement.setString("cedula", amelista.getCedula());
			callableStatement.setString("primerNombre", amelista.getPrimerNombre());
			callableStatement.setString("primerApellido", amelista.getPrimerApellido());
			callableStatement.setString("primerCelular", amelista.getPrimerCelular());
			callableStatement.registerOutParameter("mensaje", Types.VARCHAR);

			callableStatement.execute();

			mensaje = callableStatement.getString("mensaje");

		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (Exception e) {
			}

			try {
				if (callableStatement != null) {
					callableStatement.close();
				}
			} catch (Exception e) {
			}

			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
			}
		}

		return mensaje;
	}

}
