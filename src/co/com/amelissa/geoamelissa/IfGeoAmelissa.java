package co.com.amelissa.geoamelissa;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;


@WebService
@SOAPBinding(style = Style.RPC)
public interface IfGeoAmelissa {

	@WebMethod 
	public String getLocalizacion(@WebParam(name="latitud")String latitud,@WebParam(name="longitud")String longitud,@WebParam(name="imei")String imei);
	
	@WebMethod
	public GeoAmelissa getConfiguracion(@WebParam(name="imei")String imei);
	//
}
