package co.com.amelissa.geoamelissa;

import java.sql.CallableStatement;
import java.sql.Connection;

import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.sql.DataSource;

@WebService(endpointInterface = "co.com.amelissa.geoamelissa.IfGeoAmelissa")
public class IfGeoAmelissaImp implements IfGeoAmelissa {

	InitialContext ic = null;
	DataSource dataSource = null;
	String result = "",hi="",hf="",fi="",ff="",d="",tn="",ie="";
	Connection connection = null;
	CallableStatement callableStatement = null;

	@Override
	public String getLocalizacion(String latitud, String longitud, String imei) {
		try {
			ic = new InitialContext();
			dataSource = (DataSource) ic.lookup("SQLServer2005_raptor");
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall("EXECUTE geo.Proc_Guardar_Posicion_Ad ?,?,?,? ");
			callableStatement.setString("latitud", latitud);
			callableStatement.setString("longitud", longitud);
			callableStatement.setString("imei", imei);
			callableStatement.registerOutParameter("id", java.sql.Types.INTEGER);
			callableStatement.execute();
			result = String.valueOf(callableStatement.getString("id"));
		} catch (Exception ex) {
			System.out.println("ErrorInsertando->" + ex);
			result = "error->" + ex;
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
				}
			} catch (Exception e) {
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
			}
		}

		return result;
	}

	@Override
	public GeoAmelissa getConfiguracion(String imei) {
		GeoAmelissa ga = new GeoAmelissa();
		try {
			ic = new InitialContext();
			dataSource = (DataSource) ic.lookup("SQLServer2005_raptor");
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall("EXECUTE geo.Proc_Consulta_Configuracion ?,?,?,?,?,?,?,? ");
			callableStatement.setString("IMEI",imei);
			
			callableStatement.registerOutParameter("HORA_INICIO", java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter("HORA_FIN", java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter("TIEMPO_NOTIFICACION", java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter("IMEI_EXCLUIDO", java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter("FECHA_INICIO", java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter("FECHA_FIN", java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter("DIAS", java.sql.Types.VARCHAR);
			callableStatement.execute();
			
			hi = String.valueOf(callableStatement.getString("HORA_INICIO"));
			hf = String.valueOf(callableStatement.getString("HORA_FIN"));			
			tn = String.valueOf(callableStatement.getString("TIEMPO_NOTIFICACION"));
			ie = String.valueOf(callableStatement.getString("IMEI_EXCLUIDO"));
			fi = String.valueOf(callableStatement.getString("FECHA_INICIO"));
			ff = String.valueOf(callableStatement.getString("FECHA_FIN"));
			d = String.valueOf(callableStatement.getString("DIAS"));
			result="BIEN";
		} catch (Exception ex) {
			System.out.println("ErrorInsertando->" + ex);
			result = "ErrorService->" + ex;
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
				}
			} catch (Exception e) {
			}

			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
			}
		}
		ga.setNombreAmelista("Edwin");
		ga.setHoraFin(hf);
		ga.setHoraInicio(hi);		
		ga.setTiempoLocalizacion(tn);
		ga.setImeiExcluido(ie);
		ga.setMensaje(result);
		ga.setImeiDispositivo(imei);
		ga.setFechaFin(ff);
		ga.setFechaInicio(fi);
		ga.setDias(d);

		return ga;
	}
}

