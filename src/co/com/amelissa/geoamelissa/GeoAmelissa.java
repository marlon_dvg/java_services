package co.com.amelissa.geoamelissa;

public class GeoAmelissa {
	
	private String latitud;
	private String longitud;
	private String imei;
	private String nombreAmelista;
	private String horaInicio;
	private String horaFin;
	private String mensaje;
	private String tiempoLocalizacion;
	private String imeiExcluido;
	private String fechaInicio;
	private String fechaFin;
	private String dias;
	
	private String is="str ";

	public GeoAmelissa(){
		latitud="";
		longitud="";
		imei="";
		nombreAmelista="";
		horaInicio="";
		horaFin="";
		mensaje="";
		tiempoLocalizacion="";
		imeiExcluido="";
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = is+latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = is+longitud;
	}
	public String getImeiDispositivo() {
		return imei;
	}
	public void setImeiDispositivo(String imeiDispositivo) {
		this.imei = is+imeiDispositivo;
	}
	public String getNombreAmelista() {
		return nombreAmelista;
	}
	public void setNombreAmelista(String nombreAmelista) {
		this.nombreAmelista = is+nombreAmelista;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = is+horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = is+horaFin;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = is+fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = is+fechaFin;
	}
	public String getDias(){
		return dias;
	}
	public void setDias(String dias){
		this.dias = is+dias;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = is+mensaje;
	}
	public String getTiempoLocalizacion() {
		return tiempoLocalizacion;
	}
	public void setTiempoLocalizacion(String tiempoLocalizacion) {
		this.tiempoLocalizacion = is+tiempoLocalizacion;
	}
	public String getImeiExcluido() {
		return imeiExcluido;
	}
	public void setImeiExcluido(String imeiExcluido) {
		this.imeiExcluido = is+imeiExcluido;
	}

	
}
