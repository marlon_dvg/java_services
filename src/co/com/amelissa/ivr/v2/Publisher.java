package co.com.amelissa.ivr.v2;

import javax.xml.ws.Endpoint;

public class Publisher{
	
	public static void main(String[] args) {
		
		Endpoint.publish("http://localhost:8080/amelissa_portal_war/", new IvrSoapAmelissaImpl());
    }
}
