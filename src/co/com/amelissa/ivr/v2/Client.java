package co.com.amelissa.ivr.v2;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client{
	
	public static void main(String[] args) throws Exception {
		
		//URL url = new URL("http://rep.amelissa.com:8081/amelissa_portal_war/IvrSoapAmelissaImplService");
		//URL url = new URL("http://172.17.23.36:8080/amelissa_portal_war/IvrSoapAmelissaImplService");
		URL url = new URL("http://amelissavirtual.amelissa.com:8084/amelissa_web/IvrSoapAmelissaImplService");
		//URL url = new URL("http://amelissavirtual.amelissa.com:8084/Services/IvrSoapAmelissaImplService");
		
        QName qname = new QName("http://v2.ivr.amelissa.com.co/", "IvrSoapAmelissaImplService");
        
        Service service = Service.create(url, qname);
        IvrSoapAmelissa mensaje = service.getPort(IvrSoapAmelissa.class);
        
        AmelistaDTO amelistaDTO = mensaje.getAmelistaIVR("43147902");
        
        // FECHA MOMENTO SIGUIENTE
        // Angelika Maria Buitrago
        
        //System.out.println("" + amelistaDTO.getTipoIngreso() );
        System.out.println("" + amelistaDTO.getTipoCedula() );
        System.out.println("" + amelistaDTO.getFechaVencimento() );
        
    }
}
