package co.com.amelissa.ivr.v2;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface IvrSoapAmelissa{
	
	@WebMethod
	public AmelistaDTO getAmelistaIVR(String cedula);
	
	@WebMethod
	public boolean setPasswordAmelista(String cedula, String Clave);
	
}