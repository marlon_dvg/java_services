package co.com.amelissa.ivr.v2;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.sql.DataSource;

@WebService(endpointInterface = "co.com.amelissa.ivr.v2.IvrSoapAmelissa")
public class IvrSoapAmelissaImpl implements IvrSoapAmelissa{
	
	@Override
	public AmelistaDTO getAmelistaIVR(String cedula) {
		
		InitialContext ic = null;
		DataSource dataSource = null;
		ResultSet resultSet = null;
		Connection connection = null;
		CallableStatement callableStatement = null; 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		AmelistaDTO amelistaDTO = null;
		
		try{
			
			ic = new InitialContext();
			dataSource = (DataSource)ic.lookup("SQLServer2005_raptor");
			
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall("EXECUTE ws.Proc_Ivr_getSaldoAmelista ?,? ");
			callableStatement.setString("cedula", cedula);
			callableStatement.setString("codigoCatalogo", "01");
			
			resultSet = callableStatement.executeQuery();
			
			while(resultSet.next()){
				
				amelistaDTO = new AmelistaDTO();
				
				amelistaDTO.setSaldoAmelista(resultSet.getString("SALDO_AMELISTA"));
				amelistaDTO.setCedulaAmelista(resultSet.getString("CEDULA_AMELISTA"));
				
				amelistaDTO.setNombreAmelista(resultSet.getString("NOMBRE_AMELISTA"));
				
				try {
					amelistaDTO.setFechaVencimento(sdf.format(resultSet.getDate("FECHA_VENCIMIENTO")));
				} catch (Exception e) {
					amelistaDTO.setFechaVencimento("");
					e.printStackTrace();
				}
				
				amelistaDTO.setCedulaGerenteZona(resultSet.getString("CEDULA_GERENTE_ZONA"));
				
				amelistaDTO.setNombreGerenteZona(resultSet.getString("NOMBRE_GERENTE_ZONA"));
				amelistaDTO.setPrimerTelefonoGerenteZona(resultSet.getString("PRIMER_TELEFONO_GERENTE_ZONA"));
				amelistaDTO.setSegundoTelefonoGerenteZona(resultSet.getString("SEGUNDO_TELEFONO_GERENTE_ZONA"));
				amelistaDTO.setPrimerCelularGerenteZona(resultSet.getString("PRIMER_CELULAR_GERENTE_ZONA"));
				amelistaDTO.setSegundoCelularGerenteZona(resultSet.getString("SEGUNDO_CELULAR_GERENTE_ZONA"));
				
				try {
					amelistaDTO.setFechaMomentos(sdf.format(resultSet.getDate("FECHA_MOMENTOS")));
				} catch (Exception e) {
					amelistaDTO.setFechaMomentos("");
					e.printStackTrace();
				}
				
				try {
					amelistaDTO.setFechaCambiosDevolucuiones(sdf.format(resultSet.getDate("FECHA_CAMBIOS_DEVOLUCIONES")));
				} catch (Exception e) {
					amelistaDTO.setFechaCambiosDevolucuiones("");
					e.printStackTrace();
				}
				
				amelistaDTO.setTipoCedula(resultSet.getString("TIPO_CEDULA"));
				amelistaDTO.setEstadoAmelista(resultSet.getString("ESTADO_AMELISTA"));
				amelistaDTO.setSaldoFinanciamiento(resultSet.getString("SALDO_FINANCIACION"));
				amelistaDTO.setPuntaje(resultSet.getString("PUNTAJE"));
				
				try {
					amelistaDTO.setFechaMomentosSiguiente(sdf.format(resultSet.getDate("FECHA_MOMENTOS_SIGUIENTE")));
				} catch (Exception e) {
					amelistaDTO.setFechaMomentosSiguiente("");
					e.printStackTrace();
				}
				
				amelistaDTO.setTipoIngreso(resultSet.getString("TIPO_INGRESO"));
				amelistaDTO.setEstadoPedido(resultSet.getString("ESTADO_PEDIDO"));
				
				try {
					amelistaDTO.setFechaEntregaPedido(sdf.format(resultSet.getDate("FECHA_ENTREGA_PEDIDO")));
				} catch (Exception e) {
					amelistaDTO.setFechaEntregaPedido("");
					e.printStackTrace();
				}
				try {
					amelistaDTO.setFechaIngresoProximoPedido(sdf.format(resultSet.getDate("FECHA_INGRESO_PROXIMO_PEDIDO")));
				} catch (Exception e) {
					amelistaDTO.setFechaIngresoProximoPedido("");
					e.printStackTrace();
				}
			}
			
			if (amelistaDTO == null) {
				throw new Exception("Ingreso");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(resultSet != null){
					resultSet.close();
				}
			}catch(Exception e){}
			
			try{
				if(callableStatement != null){
					callableStatement.close();
				}
			}catch(Exception e){}
			
			try{
				if(connection != null){
					connection.close();
				}
			}catch(Exception e){}
		}
		
		return amelistaDTO;
	}
	
	@Override
	public boolean setPasswordAmelista(String cedula, String clave) {
		
		/*
		InitialContext ic     = null;
		DataSource dataSource = null;
		Connection connection = null;
		CallableStatement callableStatement = null;
		boolean exito = false;
		
		EncryptMD5 encrypt = new EncryptMD5();
		byte[] datos       = clave.getBytes();
		String data        = encrypt.hexadecimal(datos);
		
		try{
			
			ic = new InitialContext();
			dataSource = (DataSource)ic.lookup("SQLServer2005_raptor");
			
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall("EXECUTE web.Proc_RecordarPasswordUsuarioLoginWeb ?,?,? ");
			callableStatement.setString("login", cedula);
			callableStatement.setString("password", data);
			callableStatement.setBoolean("cambio_clave", true);
			
			callableStatement.execute();
			
			exito = true;
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(callableStatement != null){
					callableStatement.close();
				}
			}catch(Exception e){}
			
			try{
				if(connection != null){
					connection.close();
				}
			}catch(Exception e){}
		}
		*/
		return true;
	}
	
	
	/*
	@Override
	public String getConsecutivoReservaTablet(String cedula) {
		
		InitialContext ic = null;
		DataSource dataSource = null;
		StringBuilder datosAmelista = new StringBuilder();
		ResultSet resultSet = null;
		Connection connection = null;
		CallableStatement callableStatement = null; 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		try{
			
			String codigo = RandomStringUtils.randomAlphanumeric(5).toUpperCase();
			codigo = "T" + codigo;
			
			ic = new InitialContext();
			dataSource = (DataSource)ic.lookup("SQLServer2005_raptor");
			
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall("EXECUTE ws.Proc_Ivr_getConsecutivoReservaTablet ?,?,?,? ");
			callableStatement.setString("cedula", cedula);
			callableStatement.setString("codigo_reserva", codigo);
			callableStatement.setDate("fecha_vencimiento_reserva", new java.sql.Date(new Date().getTime()));
			callableStatement.setString("usuario_creacion", "IVR");
			
			resultSet = callableStatement.executeQuery();
			
			while(resultSet.next()) {
				
				datosAmelista.append(resultSet.getString("ESTADO"));
				datosAmelista.append(",");
				datosAmelista.append(resultSet.getString("CUMPLE"));
				datosAmelista.append(",");
				datosAmelista.append(resultSet.getString("MENSAJE"));
				datosAmelista.append(",");
				datosAmelista.append(resultSet.getString("CODIGO"));
				datosAmelista.append(",");
				datosAmelista.append( sdf.format(resultSet.getDate("FECHA_LIMITE")));
				//datosAmelista.append(",");
				//datosAmelista.append(",");
				
			}
			
			if (datosAmelista.toString().trim().length() < 5) {
				datosAmelista.append("C�dula no hallada");
			}
			
		}catch(Exception e){
			datosAmelista.append(e.getMessage());
			e.printStackTrace();
		}finally{
			try{
				if(resultSet != null){
					resultSet.close();
				}
			}catch(Exception e){}
			
			try{
				if(callableStatement != null){
					callableStatement.close();
				}
			}catch(Exception e){}
			
			try{
				if(connection != null){
					connection.close();
				}
			}catch(Exception e){}
		}
		
		return datosAmelista.toString();
	}
	*/
	
	/*public static void main(String[] args) throws Exception {
		
		//IvrAmelissaImpl ivr = new IvrAmelissaImpl();
		//System.out.println(ivr.prueba("23139286"));
		
		System.out.println(RandomStringUtils.randomAlphanumeric(5).toUpperCase());
		
	}*/
	
	
	/*
	@Override
	public AmelistaWS getAmelistaAsesoriaImagen(String cedula) {
		
		InitialContext ic = null;
		DataSource dataSource = null;
		ResultSet resultSet = null;
		Connection connection = null;
		CallableStatement callableStatement = null; 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		AmelistaWS amelistaWS = new AmelistaWS();
		amelistaWS.setCedula("0");
		
		try{
			
			ic = new InitialContext();
			dataSource = (DataSource)ic.lookup("SQLServer2005_raptor");
			
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall("EXECUTE ws.Proc_Get_DatosBasicosAmelista ?,? ");
			callableStatement.setString("cedula", cedula);
			callableStatement.setString("codigoCatalogo", "01");
			
			resultSet = callableStatement.executeQuery();
			
			while(resultSet.next()) {
				
				amelistaWS.setCodigo(resultSet.getLong("CODIGO"));
				amelistaWS.setCedula(resultSet.getString("CEDULA_AMELISTA"));
				amelistaWS.setPrimerTelefono(resultSet.getString("PRIMER_TELEFONO"));
				amelistaWS.setPrimerCelular(resultSet.getString("PRIMER_CELULAR"));
				amelistaWS.setDireccionResidencia(resultSet.getString("DIRECCION_RESIDENCIA"));
				amelistaWS.setEmail(resultSet.getString("EMAIL"));
				try {
					amelistaWS.setFechaNacimiento(sdf.format(resultSet.getDate("FECHA_NACIMIENTO")));
				} catch (Exception e) {
					e.printStackTrace();
					amelistaWS.setFechaNacimiento("Fecha no encontrada");
				}
				amelistaWS.setPrimerNombre(resultSet.getString("PRIMER_NOMBRE"));
				amelistaWS.setSegundoNombre(resultSet.getString("SEGUNDO_NOMBRE"));
				amelistaWS.setPrimerApellido(resultSet.getString("PRIMER_APELLIDO"));
				amelistaWS.setSegundoApellido(resultSet.getString("SEGUNDO_APELLIDO"));
				amelistaWS.setDivipola(resultSet.getString("DIVIPOLA"));
				amelistaWS.setCodigoEstado(resultSet.getInt("CODIGO_ESTADO"));
				amelistaWS.setNombreEstado(resultSet.getString("NOMBRE_ESTADO"));
				amelistaWS.setNombreDepartamento(resultSet.getString("NOMBRE_DEPARTAMENTO"));
				amelistaWS.setNombreMunicipio(resultSet.getString("NOMBRE_MUNICIPIO"));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(resultSet != null){
					resultSet.close();
				}
			}catch(Exception e){}
			
			try{
				if(callableStatement != null){
					callableStatement.close();
				}
			}catch(Exception e){}
			
			try{
				if(connection != null){
					connection.close();
				}
			}catch(Exception e){}
		}
		
		return amelistaWS;
	}
	*/
}