package co.com.amelissa.closer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Send {

  private final static String QUEUE_NAME = "pedidos-enviados-amelissa-respuesta";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    //factory.setHost("localhost");
    //factory.setUri("amqp://userName:password@hostName:portNumber/virtualHost");
    //factory.setHost("amqp://Jz31bGkd:JQ5igRZHYTrojiaT9Vgk7zSyQWYu9prR@sad-holly-909.bigwig.lshift.net:10300/XLBP7hk-UPf5");
    factory.setUri("amqp://Jz31bGkd:JQ5igRZHYTrojiaT9Vgk7zSyQWYu9prR@sad-holly-909.bigwig.lshift.net:10300/XLBP7hk-UPf5");
    
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();
    
    channel.queueDeclare("pedidos-enviados-amelissa-respuesta", false, false, false, null);
    //channel.queueDeclare("hello", false, false, false, null);
    String message = "{idMessage: 000004, status: OK}";
    //String message = "Closer";
    channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
    System.out.println(" [x] Sent '" + message + "'");
    
    channel.close();
    connection.close();
  }
}
