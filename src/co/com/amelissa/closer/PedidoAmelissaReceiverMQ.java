package co.com.amelissa.closer;

import co.com.amelissa.closer.dto.Detalle;
import co.com.amelissa.closer.dto.Pedido;
import co.com.ecommerce.amelissa.facturacion.dto.Items;
import co.com.ecommerce.amelissa.facturacion.dto.OrdenDTO;
import co.com.util.ConnectionBD;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.sql.CallableStatement;
//import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

import org.codehaus.jackson.map.ObjectMapper;

public class PedidoAmelissaReceiverMQ {

  //private final static String QUEUE_NAME = "hello";
  
  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    //factory.setHost("localhost");
    //factory.setHost("amqp://Jz31bGkd:JQ5igRZHYTrojiaT9Vgk7zSyQWYu9prR@sad-holly-909.bigwig.lshift.net:10301/XLBP7hk-UPf5");
    factory.setUri("amqp://Jz31bGkd:JQ5igRZHYTrojiaT9Vgk7zSyQWYu9prR@sad-holly-909.bigwig.lshift.net:10301/XLBP7hk-UPf5");
    
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();
    
    //channel.queueDeclare("hello", false, false, false, null);
    channel.queueDeclare("pedidos-enviados-closer", false, false, false, null);
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
    
    Consumer consumer = new DefaultConsumer(channel) {
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
          throws IOException {
        String message = new String(body, "UTF-8");
        System.out.println(" [x] Received '" + message + "'");
        
        ObjectMapper JSON_MAPPER = new ObjectMapper();
        
        Pedido pedido  = JSON_MAPPER.readValue(message, Pedido.class);
        
        System.out.println(pedido.getIdAssesor());
        System.out.println(pedido.getIdCampana());
        System.out.println(pedido.getIdCatalogo());
        System.out.println(pedido.getIdMessage());
        Detalle[] detalle = pedido.getItems();
        
        for (int i = 0; i < detalle.length; i++) {
			System.out.println("  " + detalle[i].getIdSku());
			System.out.println("  " + detalle[i].getCantidad());
			System.out.println("  " + detalle[i].getPagina());
			System.out.println("   *****   ");
		}
        
        //////////////////////////////////////////////////////////////////
        
        java.sql.Connection connection = null;
		long codigoAmelista = 0;
		long zonaAmelista = 0;
		int identity = 0;
		
		ConnectionBD conn = new ConnectionBD();
		//connection = conn.getConnectionSQLServerAmelissa();
		connection = conn.getConnectionSQLServerAmelissaPruebas();
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		
        try{
        	
		    try {
	        	
	        	callableStatement = connection.prepareCall(" EXECUTE amelissa.getAmelista ?,?");
	        	callableStatement.setString("cedula", pedido.getIdAssesor());
				callableStatement.setString("codigoCatalogo", "00");
				callableStatement.execute();
				resultSet = callableStatement.getResultSet();
				
				while(resultSet.next()) {
					codigoAmelista = resultSet.getLong("CODIGO_AMELISTA");
					zonaAmelista = resultSet.getLong("CODIGO_ZONA");
				}
				
			} catch (Exception e) {
				System.out.println("ERROR EL CONSULTAR CAMPANA");
				e.printStackTrace();
			}
		    
		    
			callableStatement = connection.prepareCall("EXECUTE facturacion.Proc_Enc_Grabar_Pedido_Digitado ?,?,?,?,?,?,?,?,?,?,? ");
			
			callableStatement.setLong("PnumeroPedido", 0);
			callableStatement.setLong("Pconsecutivo", 0);
			callableStatement.setLong("PcodigoAmelista", codigoAmelista);
			callableStatement.setString("PusuarioIngreso", pedido.getIdAssesor());
			callableStatement.setLong("Pzona", zonaAmelista);
			callableStatement.setString("PmedioIngreso","CV");
			callableStatement.setString("CAMPAMA", pedido.getIdCampana());
			callableStatement.setString("CODIGO_CATALOGO", pedido.getIdCatalogo());
			callableStatement.setString("tipoEnvio", "1");
			callableStatement.registerOutParameter("Pmensaje", Types.VARCHAR);
			callableStatement.registerOutParameter("ID", Types.INTEGER);
			
			callableStatement.execute();
			String mensaje = callableStatement.getString("Pmensaje");
			identity = callableStatement.getInt("ID");
			
			if(message != null) {
				throw new Exception(message);
			}
			
			callableStatement = connection.prepareCall("EXECUTE facturacion.Proc_Det_Grabar_Pedido_Digitado ?,?,?,?,?,? ");
			
			for (int i = 0; i < detalle.length; i++) {
				Detalle detalleDTO = detalle[i];
				
				callableStatement.setLong("PidEncabezado", identity);
				callableStatement.setLong("PcodigoAmelista", codigoAmelista);
				callableStatement.setString("Pproducto", detalleDTO.getIdSku());
				callableStatement.setLong("pcantidad", Long.valueOf(detalleDTO.getCantidad()));
				callableStatement.setString("CAMPAMA", pedido.getIdCampana());
				callableStatement.registerOutParameter("Pmensaje", Types.VARCHAR);
				callableStatement.execute();
				
				mensaje = callableStatement.getString("Pmensaje");
				
				if(message != null) {
					throw new Exception(message);
				}
			}
			
			connection.commit();
			
			try {
				
				ConnectionFactory factory = new ConnectionFactory();
			    factory.setUri("amqp://Jz31bGkd:JQ5igRZHYTrojiaT9Vgk7zSyQWYu9prR@sad-holly-909.bigwig.lshift.net:10300/XLBP7hk-UPf5");
			    
			    Connection connectionSend = factory.newConnection();
			    Channel channel = connectionSend.createChannel();
			    
			    channel.queueDeclare("pedidos-enviados-amelissa-respuesta", false, false, false, null);
			    String messageSend = "{idMessage: 000004, status: OK}";
			    //channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
			    channel.basicPublish("", "pedidos-enviados-amelissa-respuesta", null, message.getBytes("UTF-8"));
			    
			    System.out.println(" [x] Sent '" + message + "'");
			    
			    channel.close();
			    connection.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}catch(Exception e){
			try{
				connection.rollback(); 
			}catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			
			try{
				if(callableStatement!=null){
					callableStatement.close();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				if(connection!=null){
					connection.close();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
        
		/*
		connection = conn.getConnectionSQLServer2012();
    	try {
        	callableStatement = connection.prepareCall(" EXECUTE amelissa.Proc_Crud_Orden_No_Guardada ? ");
        	callableStatement.setLong("id_orden",identity de la orden vtex);
            callableStatement.execute();
            connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
    	/////////////
        
        
        
      }
    };
    channel.basicConsume("hello", true, consumer);
  }
  
  /*
  private static void addPedidosDigitadosDetalle(long codigoAmelista, Object orden, String campanaActual, CallableStatement  callableStatement) throws Exception {
		
		try{
			
			Items[] itemsPedidos = orden.getItems();
			
			for (int i = 0; i < itemsPedidos.length; i++) {
				Items item = itemsPedidos[i];
				
				callableStatement.setLong("PidEncabezado", identity);
				callableStatement.setLong("PcodigoAmelista", codigoAmelista);
				callableStatement.setString("Pproducto", item.getRefId());
				callableStatement.setLong("pcantidad", Long.valueOf(item.getQuantity()));
				callableStatement.setString("CAMPAMA", campanaActual);
				callableStatement.registerOutParameter("Pmensaje", Types.VARCHAR);
				callableStatement.execute();
				
				String message = callableStatement.getString("Pmensaje");
				
				if(message != null) {
					throw new Exception(message);
				}
			}
		}catch(Exception exception){
			throw new Exception(exception);
		}finally{
			if(callableStatement!=null){
				try{
					callableStatement.close();
				}catch(Exception exception2){
					exception2.printStackTrace();
				}
			}
		}
	}
  */
  
}

