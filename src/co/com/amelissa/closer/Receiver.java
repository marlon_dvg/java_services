package co.com.amelissa.closer;

import com.rabbitmq.client.*;

import java.io.IOException;

public class Receiver {

  private final static String QUEUE_NAME = "hello";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    //factory.setHost("localhost");
    //factory.setHost("amqp://Jz31bGkd:JQ5igRZHYTrojiaT9Vgk7zSyQWYu9prR@sad-holly-909.bigwig.lshift.net:10301/XLBP7hk-UPf5");
    factory.setUri("amqp://Jz31bGkd:JQ5igRZHYTrojiaT9Vgk7zSyQWYu9prR@sad-holly-909.bigwig.lshift.net:10301/XLBP7hk-UPf5");
    
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();
    
    //channel.queueDeclare("hello", false, false, false, null);
    channel.queueDeclare("pedidos-enviados-closer", false, false, false, null);
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
    
    Consumer consumer = new DefaultConsumer(channel) {
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
          throws IOException {
        String message = new String(body, "UTF-8");
        System.out.println(" [x] Received '" + message + "'");
      }
    };
    channel.basicConsume("hello", true, consumer);
  }
}

