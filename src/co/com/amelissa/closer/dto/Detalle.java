package co.com.amelissa.closer.dto;

public class Detalle {
	
	public String idSku;
	public String cantidad;
	public String pagina;
	
	public Detalle() {
		// TODO Auto-generated constructor stub
	}

	public String getIdSku() {
		return idSku;
	}

	public void setIdSku(String idSku) {
		this.idSku = idSku;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
	
}
