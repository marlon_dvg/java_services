package co.com.amelissa.closer.dto;

public class Pedido {
	
	public String idMessage;
	public String idCampana;
	public String idCatalogo;
	public String idAssesor;
	public Detalle [] items;
	
	public Pedido() {
		// TODO Auto-generated constructor stub
	}

	public String getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}

	public String getIdCampana() {
		return idCampana;
	}

	public void setIdCampana(String idCampana) {
		this.idCampana = idCampana;
	}

	public String getIdCatalogo() {
		return idCatalogo;
	}

	public void setIdCatalogo(String idCatalogo) {
		this.idCatalogo = idCatalogo;
	}

	public String getIdAssesor() {
		return idAssesor;
	}

	public void setIdAssesor(String idAssesor) {
		this.idAssesor = idAssesor;
	}

	public Detalle[] getItems() {
		return items;
	}

	public void setItems(Detalle[] items) {
		this.items = items;
	}
	
}
