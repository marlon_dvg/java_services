package co.com.amelissa.ws.chatbot;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Producto {
	private String codigoProducto;
	private String campana;
	private String codigoCatalogo;
	private String digitoChequeo;
	private String descripcion;
	//private long codigo;
	private String precioCatalogo;
	private String pagina;
	private String claseProducto;
	private String disponible;
	private String cantidadDigitada;
	//private long codigoBase;
	private Boolean existe;
	
	public String getCodigoProducto() {
		return codigoProducto;
	}
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	public String getCampana() {
		return campana;
	}
	public void setCampana(String campana) {
		this.campana = campana;
	}
	public String getCodigoCatalogo() {
		return codigoCatalogo;
	}
	public void setCodigoCatalogo(String codigoCatalogo) {
		this.codigoCatalogo = codigoCatalogo;
	}
	public String getDigitoChequeo() {
		return digitoChequeo;
	}
	public void setDigitoChequeo(String digitoChequeo) {
		this.digitoChequeo = digitoChequeo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/*public long getCodigo() {
		return codigo;
	}
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}*/
	public String getPrecioCatalogo() {
		return precioCatalogo;
	}
	public void setPrecioCatalogo(String precioCatalogo) {
		this.precioCatalogo = precioCatalogo;
	}
	public String getPagina() {
		return pagina;
	}
	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
	public String getClaseProducto() {
		return claseProducto;
	}
	public void setClaseProducto(String claseProducto) {
		this.claseProducto = claseProducto;
	}
	public String getDisponible() {
		return disponible;
	}
	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}
	/*public long getCodigoBase() {
		return codigoBase;
	}
	public void setCodigoBase(long codigoBase) {
		this.codigoBase = codigoBase;
	}*/
	public Boolean isExiste() {
		return existe;
	}
	public void setExiste(Boolean existe) {
		this.existe = existe;
	}
	public String getCantidadDigitada() {
		return cantidadDigitada;
	}
	public void setCantidadDigitada(String cantidadDigitada) {
		this.cantidadDigitada = cantidadDigitada;
	}		
}
