package co.com.amelissa.ws.chatbot;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pedido {
	private String cedulaAmelista;
	private String codigoAmelista;
	private String numeroPedido;
	private String consecutivoDigitacion;
	private String estadoPedido;
	private String valorFacturado;
	private String codigoZona;
	private String campana;
	private List<Producto> detallePedido;

	public String getCodigoAmelista() {
		return codigoAmelista;
	}

	public void setCodigoAmelista(String codigoAmelista) {
		this.codigoAmelista = codigoAmelista;
	}

	public String getNumeroPedido() {
		return numeroPedido;
	}

	public String getConsecutivoDigitacion() {
		return consecutivoDigitacion;
	}

	public void setConsecutivoDigitacion(String consecutivoDigitacion) {
		this.consecutivoDigitacion = consecutivoDigitacion;
	}

	public void setNumeroPedido(String numeroPedido) {
		this.numeroPedido = numeroPedido;
	}

	public String getEstadoPedido() {
		return estadoPedido;
	}

	public void setEstadoPedido(String estadoPedido) {
		this.estadoPedido = estadoPedido;
	}

	public String getValorFacturado() {
		return valorFacturado;
	}

	public void setValorFacturado(String valorFacturado) {
		this.valorFacturado = valorFacturado;
	}

	public String getCodigoZona() {
		return codigoZona;
	}

	public void setCodigoZona(String codigoZona) {
		this.codigoZona = codigoZona;
	}

	public List<Producto> getDetallePedido() {
		return detallePedido;
	}

	public void setDetallePedido(List<Producto> detallePedido) {
		this.detallePedido = detallePedido;
	}

	public String getCampana() {
		return campana;
	}

	public void setCampana(String campana) {
		this.campana = campana;
	}

	public String getCedulaAmelista() {
		return cedulaAmelista;
	}

	public void setCedulaAmelista(String cedulaAmelista) {
		this.cedulaAmelista = cedulaAmelista;
	}
}
