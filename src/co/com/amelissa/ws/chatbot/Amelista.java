package co.com.amelissa.ws.chatbot;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Amelista {
	private long codigo;
	private String cedula;
	private String primerNombre;
	private String segundoNombre;
	private String primerApellido;
	private String segundoApellido;
	private String primerTelefono;
	private String segundoTelefono;
	private String primerCelular;
	private String segundoCelular;
	private String direccionResidencia;
	private String barrioResidencia;
	private String email;
	private String tipo;
	private String nombreTipo;
	private String saldo;
	private String saldoSinCastigo;
	private String tope;
	private String seccion;
	private String fechaNacimiento;
	private String sexo;
	private String ultimaCampana;
	private String poblado;
	private String municipio;
	private String departamento;
	private String codigoDivipola;
	private String codigoZona;
	private String codigoEstado;
	private String nombreEstado;
	private String codigoBloqueo;
	private String estadoBloqueo;
	private String codigoDivision;
	private String fechaLimitePago;
	private String idIngreso;
	private String tipoIngreso;
	private String valorCupo;

	private String campanaActual;
	private String fechaFinalDigitacionInternet;
	private String fechaConferencia;
	private String pedidoMinimo;
	private String tipoPedido;

	private String mensaje;
	private boolean existe;

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getPrimerTelefono() {
		return primerTelefono;
	}

	public void setPrimerTelefono(String primerTelefono) {
		this.primerTelefono = primerTelefono;
	}

	public String getSegundoTelefono() {
		return segundoTelefono;
	}

	public void setSegundoTelefono(String segundoTelefono) {
		this.segundoTelefono = segundoTelefono;
	}

	public String getPrimerCelular() {
		return primerCelular;
	}

	public void setPrimerCelular(String primerCelular) {
		this.primerCelular = primerCelular;
	}

	public String getSegundoCelular() {
		return segundoCelular;
	}

	public void setSegundoCelular(String segundoCelular) {
		this.segundoCelular = segundoCelular;
	}

	public String getDireccionResidencia() {
		return direccionResidencia;
	}

	public void setDireccionResidencia(String direccionResidencia) {
		this.direccionResidencia = direccionResidencia;
	}

	public String getBarrioResidencia() {
		return barrioResidencia;
	}

	public void setBarrioResidencia(String barrioResidencia) {
		this.barrioResidencia = barrioResidencia;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombreTipo() {
		return nombreTipo;
	}

	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	public String getSaldoSinCastigo() {
		return saldoSinCastigo;
	}

	public void setSaldoSinCastigo(String saldoSinCastigo) {
		this.saldoSinCastigo = saldoSinCastigo;
	}

	public String getTope() {
		return tope;
	}

	public void setTope(String tope) {
		this.tope = tope;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getUltimaCampana() {
		return ultimaCampana;
	}

	public void setUltimaCampana(String ultimaCampana) {
		this.ultimaCampana = ultimaCampana;
	}

	public String getPoblado() {
		return poblado;
	}

	public void setPoblado(String poblado) {
		this.poblado = poblado;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getCodigoDivipola() {
		return codigoDivipola;
	}

	public void setCodigoDivipola(String codigoDivipola) {
		this.codigoDivipola = codigoDivipola;
	}

	public String getCodigoZona() {
		return codigoZona;
	}

	public void setCodigoZona(String codigoZona) {
		this.codigoZona = codigoZona;
	}

	public String getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public String getNombreEstado() {
		return nombreEstado;
	}

	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}

	public String getCodigoBloqueo() {
		return codigoBloqueo;
	}

	public void setCodigoBloqueo(String codigoBloqueo) {
		this.codigoBloqueo = codigoBloqueo;
	}

	public String getEstadoBloqueo() {
		return estadoBloqueo;
	}

	public void setEstadoBloqueo(String estadoBloqueo) {
		this.estadoBloqueo = estadoBloqueo;
	}

	public String getCodigoDivision() {
		return codigoDivision;
	}

	public void setCodigoDivision(String codigoDivision) {
		this.codigoDivision = codigoDivision;
	}

	public String getFechaLimitePago() {
		return fechaLimitePago;
	}

	public void setFechaLimitePago(String fechaLimitePago) {
		this.fechaLimitePago = fechaLimitePago;
	}

	public String getIdIngreso() {
		return idIngreso;
	}

	public void setIdIngreso(String idIngreso) {
		this.idIngreso = idIngreso;
	}

	public String getTipoIngreso() {
		return tipoIngreso;
	}

	public void setTipoIngreso(String tipoIngreso) {
		this.tipoIngreso = tipoIngreso;
	}

	public String getValorCupo() {
		return valorCupo;
	}

	public void setValorCupo(String valorCupo) {
		this.valorCupo = valorCupo;
	}

	public String getCampanaActual() {
		return campanaActual;
	}

	public void setCampanaActual(String campana) {
		this.campanaActual = campana;
	}

	public String getFechaFinalDigitacionInternet() {
		return fechaFinalDigitacionInternet;
	}

	public void setFechaFinalDigitacionInternet(String fechaFinalDigitacionInternet) {
		this.fechaFinalDigitacionInternet = fechaFinalDigitacionInternet;
	}

	public String getFechaConferencia() {
		return fechaConferencia;
	}

	public void setFechaConferencia(String fechaConferencia) {
		this.fechaConferencia = fechaConferencia;
	}

	public String getPedidoMinimo() {
		return pedidoMinimo;
	}

	public void setPedidoMinimo(String pedidoMinimo) {
		this.pedidoMinimo = pedidoMinimo;
	}

	public String getTipoPedido() {
		return tipoPedido;
	}

	public void setTipoPedido(String tipoPedido) {
		this.tipoPedido = tipoPedido;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean getExiste() {
		return existe;
	}

	public void setExiste(boolean existe) {
		this.existe = existe;
	}
}
