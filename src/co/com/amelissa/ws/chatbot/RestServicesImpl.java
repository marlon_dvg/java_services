package co.com.amelissa.ws.chatbot;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("amelissa")
public class RestServicesImpl {

	@GET
	@Path("amelista/{cedula}")
	@Produces(MediaType.APPLICATION_JSON)
	public Amelista obtenerAmelista(@PathParam("cedula") String cedula, @HeaderParam("token-cliente") String token)
			throws Exception {

		token = (token == null ? "" : token);

		if (!token.equals("E9B52464F025C2987881C49D7FC32A68D01A090B900DD3D81833BC32897F4A65")) {
			throw new Exception("Token inv�lido");
		} else {
			InitialContext ic = null;
			DataSource dataSource = null;
			Connection connection = null;
			CallableStatement callableStatement = null;
			CallableStatement callableStatement2 = null;
			ResultSet resultSet = null;
			ResultSet resultSet2 = null;

			Amelista amelista = new Amelista();
			String zona = "";
			String codigoAmelista = "";

			try {
				ic = new InitialContext();
				dataSource = (DataSource) ic.lookup("SQLServer2005_pruebas");

				connection = dataSource.getConnection();
				callableStatement = connection.prepareCall("EXECUTE amelissa.getAmelista ?,? ");
				callableStatement.setString("cedula", cedula);
				callableStatement.setString("codigoCatalogo", "00");

				resultSet = callableStatement.executeQuery();

				if (resultSet.next()) {
					zona = resultSet.getString("CODIGO_ZONA");
					codigoAmelista = resultSet.getString("CODIGO_AMELISTA");

					amelista.setCodigo(resultSet.getLong("CODIGO_AMELISTA"));
					amelista.setCedula(resultSet.getString("CEDULA_AMELISTA"));
					amelista.setPrimerNombre(resultSet.getString("PRIMER_NOMBRE_AMELISTA"));
					amelista.setSegundoNombre(resultSet.getString("SEGUNDO_NOMBRE_AMELISTA"));
					amelista.setPrimerApellido(resultSet.getString("PRIMER_APELLIDO_AMELISTA"));
					amelista.setSegundoApellido(resultSet.getString("SEGUNDO_APELLIDO_AMELISTA"));
					amelista.setPrimerTelefono(resultSet.getString("PRIMER_TELEFONO_AMELISTA"));
					amelista.setSegundoTelefono(resultSet.getString("SEGUNDO_TELEFONO_AMELISTA"));
					amelista.setPrimerCelular(resultSet.getString("PRIMER_CELULAR_AMELISTA"));
					amelista.setSegundoCelular(resultSet.getString("SEGUNDO_CELULAR_AMELISTA"));
					amelista.setDireccionResidencia(resultSet.getString("DIRECCION_RESIDENCIA_AMELISTA"));
					amelista.setBarrioResidencia(resultSet.getString("BARRIO_RESIDENCIA_AMELISTA"));
					amelista.setEmail(resultSet.getString("EMAIL_AMELISTA"));
					// amelista.setTipo(resultSet.getString("TIPO_AMELISTA"));
					amelista.setNombreTipo(resultSet.getString("NOMBRE_TIPO_AMELISTA"));
					amelista.setSaldo(resultSet.getString("SALDO_AMELISTA"));
					// amelista.setSaldoSinCastigo(resultSet.getString("SALDO_SIN_CASTIGOS_AMELISTA"));
					amelista.setTope(resultSet.getString("TOPE_AMELISTA"));
					amelista.setSeccion(resultSet.getString("SECCION_AMELISTA"));
					amelista.setFechaNacimiento(resultSet.getString("FECHA_NACIMIENTO_AMELISTA"));
					amelista.setSexo(resultSet.getString("SEXO_AMELISTA"));
					amelista.setUltimaCampana(resultSet.getString("ULTIMA_CAMPANA_AMELISTA"));
					amelista.setPoblado(resultSet.getString("POBLADO_AMELISTA"));
					amelista.setMunicipio(resultSet.getString("MUNICIPIO_AMELISTA"));
					amelista.setDepartamento(resultSet.getString("DEPARTAMENTO_AMELISTA"));
					amelista.setCodigoDivipola(resultSet.getString("CODIGO_DIVIPOLA"));
					amelista.setCodigoZona(zona);
					// amelista.setCodigoEstado(resultSet.getString("CODIGO_ESTADO_AMELISTA"));
					amelista.setNombreEstado(resultSet.getString("ESTADO_AMELISTA"));
					// amelista.setCodigoBloqueo(resultSet.getString("CODIGO_BLOQUEO_AMELISTA"));
					amelista.setEstadoBloqueo(resultSet.getString("ESTADO_BLOQUEO_AMELISTA"));
					amelista.setCodigoDivision(resultSet.getString("CODIGO_DIVISION"));
					amelista.setFechaLimitePago(resultSet.getString("FECHA_LIMITE_PAGO"));
					// amelista.setIdIngreso(resultSet.getString("ID_INGRESO"));
					amelista.setTipoIngreso(resultSet.getString("TIPO_INGRESO"));
					amelista.setValorCupo(resultSet.getString("VALOR_CUPO"));

					callableStatement2 = connection.prepareCall("EXEC ws.Proc_Get_Fechas_Amelista ?,?");
					callableStatement2.setString("CODIGO_ZONA", zona);
					callableStatement2.setString("CODIGO_AMELISTA", codigoAmelista);

					resultSet2 = callableStatement2.executeQuery();

					if (resultSet2.next()) {
						amelista.setCampanaActual(resultSet2.getString("CAMPANA_ACTUAL"));
						amelista.setFechaFinalDigitacionInternet(
								resultSet2.getString("FECHA_FINAL_DIGITACION_INTERNET"));
						amelista.setFechaConferencia(resultSet2.getString("FECHA_CONFERENCIA"));
						amelista.setPedidoMinimo(resultSet2.getString("PEDIDO_MINIMO"));
						amelista.setTipoPedido(resultSet2.getString("TIPO_PEDIDO"));
						amelista.setMensaje(resultSet2.getString("MENSAJE"));
					}

					amelista.setExiste(true);
				} else {
					amelista.setCedula(cedula);
					amelista.setMensaje("LA AMELISTA NO EXISTE");
					amelista.setExiste(false);
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
				} catch (Exception e) {
				}
				
				try {
					if (resultSet2 != null) {
						resultSet2.close();
					}
				} catch (Exception e) {
				}
				
				try {
					if (callableStatement != null) {
						callableStatement.close();
					}
				} catch (Exception e) {
				}
				
				try {
					if (callableStatement2 != null) {
						callableStatement2.close();
					}
				} catch (Exception e) {
				}

				try {
					if (connection != null) {
						connection.close();
					}
				} catch (Exception e) {
				}
			}
			
			return amelista;
		}
	}

	@GET
	@Path("productos/{campana}/{zona}/{codigos}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Producto> obtenerListaProductos(@PathParam("campana") String campana, @PathParam("zona") String zona,
			@PathParam("codigos") String codigos, @HeaderParam("token-cliente") String token) throws Exception {

		token = (token == null ? "" : token);

		if (!token.equals("E9B52464F025C2987881C49D7FC32A68D01A090B900DD3D81833BC32897F4A65")) {
			throw new Exception("Token inv�lido");
		} else {
			InitialContext ic = null;
			DataSource dataSource = null;
			ResultSet resultSet = null;
			Connection connection = null;
			CallableStatement callableStatement = null;

			List<String> lstCodigos = Arrays.asList(codigos.split("\\s*,\\s*"));
			List<Producto> listaProductos = new ArrayList<>();
			Producto producto;

			try {
				ic = new InitialContext();
				dataSource = (DataSource) ic.lookup("SQLServer2005_pruebas");
				connection = dataSource.getConnection();

				for (final String codigo : lstCodigos) {

					callableStatement = connection.prepareCall("EXEC facturacion.Get_Codigos_Toma_Pedidos ?,?,?,?,?");
					callableStatement.setString("Pcodigo", codigo.trim().substring(0, 5));
					callableStatement.setString("PdigitoChequeo", codigo.trim().substring(5, 6));
					callableStatement.setString("Pcatalogo", "00");
					callableStatement.setString("Pcampana", campana);
					callableStatement.setString("Pzona", zona);
					resultSet = callableStatement.executeQuery();

					producto = new Producto();

					if (resultSet.next()) {
						producto.setCodigoProducto(codigo);
						producto.setDescripcion(resultSet.getString("DESCRIPCION_PRODUCTO"));
						producto.setExiste(true);
						// producto.setCampana(resultSet.getString("CAMPANA"));
						// producto.setCodigoCatalogo(resultSet.getString("CODIGO_CATALOGO"));
						// producto.setDigitoChequeo(resultSet.getString("DIGITO_CHEQUEO"));
						// producto.setCodigo(resultSet.getLong("CODIGO"));
						// producto.setPrecioCatalogo(resultSet.getString("PRECIO_CATALOGO"));
						// producto.setPagina(resultSet.getString("PAGINA"));
						// producto.setClaseProducto(resultSet.getString("CLASE_PRODCUTO"));
						// producto.setDisponible(resultSet.getString("DISPONIBLE"));
						// producto.setCodigoBase(resultSet.getLong("CODIGO_BASE"));
					} else {
						producto.setCodigoProducto(codigo);
						producto.setDescripcion("EL PRODUCTO NO EXISTE");
						producto.setExiste(false);
					}

					listaProductos.add(producto);
				}

			} catch (Exception e) {
				throw new Exception(e);
			} finally {

				try {
					if (resultSet != null) {
						resultSet.close();
					}
				} catch (Exception e) {
				}

				try {
					if (callableStatement != null) {
						callableStatement.close();
					}
				} catch (Exception e) {
				}

				try {
					if (connection != null) {
						connection.close();
					}
				} catch (Exception e) {
				}

			}

			return listaProductos;
		}
	}

	@GET
	@Path("pedido/{codigo-amelista}/{campana}")
	@Produces(MediaType.APPLICATION_JSON)
	public Pedido obtenerPedido(@PathParam("codigo-amelista") String codigoAmelista,
			@PathParam("campana") String campana, @HeaderParam("token-cliente") String token) throws Exception {

		token = (token == null ? "" : token);

		if (!token.equals("E9B52464F025C2987881C49D7FC32A68D01A090B900DD3D81833BC32897F4A65")) {
			throw new Exception("Token inv�lido");
		} else {
			InitialContext ic = null;
			DataSource dataSource = null;
			Connection connection = null;
			CallableStatement callableStatement = null;
			CallableStatement callableStatement2 = null;
			ResultSet resultSet = null;
			ResultSet resultSet2 = null;

			Pedido pedido = new Pedido();

			Producto producto;

			try {
				ic = new InitialContext();
				dataSource = (DataSource) ic.lookup("SQLServer2005_pruebas");
				connection = dataSource.getConnection();

				callableStatement = connection.prepareCall("EXEC facturacion.Proc_Get_Pedidos_Por_Amelista ?,?,?");
				callableStatement.setString("codigoAmelista", codigoAmelista);
				callableStatement.setString("campana", campana);
				callableStatement.setString("codigoCatalogo", "00");

				resultSet = callableStatement.executeQuery();

				if (resultSet.next()) {
					//pedido.setCodigoAmelista(codigoAmelista);
					//pedido.setNumeroPedido(resultSet.getString("NUMERO_PEDIDO"));
					//pedido.setValorFacturado(resultSet.getString("VALOR_FACTURADO"));
					pedido.setEstadoPedido(resultSet.getString("ESTADO"));
					
					String numeroPedido = resultSet.getString("NUMERO_PEDIDO");

					callableStatement2 = connection.prepareCall("EXEC facturacion.Get_Items_Pedido ?,?,?");
					callableStatement2.setString("PnumeroPedido", numeroPedido);
					callableStatement2.setString("Pcampana", campana);
					callableStatement2.setString("codigoAmelista", codigoAmelista);

					resultSet2 = callableStatement2.executeQuery();

					List<Producto> listaProductos = new ArrayList<>();

					while (resultSet2.next()) {
						producto = new Producto();
						producto.setCodigoProducto(resultSet2.getString("CODIGO_PRODUCTO").trim()+resultSet2.getString("DIGITO_CHEQUEO").trim());
						producto.setDescripcion(resultSet2.getString("DESCRIPCION_PRODUCTO"));
						producto.setCantidadDigitada(resultSet2.getString("CANTIDAD_DIGITADA"));
						//producto.setExiste(true);
						//producto.setPrecioCatalogo(resultSet2.getString("PRECIO_CATALOGO"));

						listaProductos.add(producto);
					}

					pedido.setDetallePedido(listaProductos);
				} else {
					//pedido.setCodigoAmelista(codigoAmelista);
					pedido.setEstadoPedido("SIN PEDIDO");
				}

			} catch (Exception e) {
				throw new Exception(e);
			} finally {

				try {
					if (resultSet != null) {
						resultSet.close();
					}
				} catch (Exception e) {
				}

				try {
					if (resultSet2 != null) {
						resultSet2.close();
					}
				} catch (Exception e) {
				}

				try {
					if (callableStatement != null) {
						callableStatement.close();
					}
				} catch (Exception e) {
				}

				try {
					if (callableStatement2 != null) {
						callableStatement2.close();
					}
				} catch (Exception e) {
				}

				try {
					if (connection != null) {
						connection.close();
					}
				} catch (Exception e) {
				}

			}

			return pedido;
		}
	}

	@POST
	@Path("guardar_pedido")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String guardarPedido(Pedido pedido, @HeaderParam("token-cliente") String token) throws Exception {

		token = (token == null ? "" : token);

		if (!token.equals("E9B52464F025C2987881C49D7FC32A68D01A090B900DD3D81833BC32897F4A65")) {
			throw new Exception("Token inv�lido");
		} else {
			InitialContext ic = null;
			DataSource dataSource = null;
			Connection connection = null;
			CallableStatement callableStatement = null;
			CallableStatement callableStatement2 = null;
			ResultSet resultSet = null;
			ResultSet resultSet2 = null;

			int identity;
			String mensaje = "";

			// Encabezado
			try {
				ic = new InitialContext();
				dataSource = (DataSource) ic.lookup("SQLServer2005_pruebas");
				connection = dataSource.getConnection();

				callableStatement = connection
						.prepareCall("EXEC facturacion.Proc_Enc_Grabar_Pedido_Digitado ?,?,?,?,?,?,?,?,?,?,?");
				callableStatement.setString("PnumeroPedido", "0");
				callableStatement.setString("Pconsecutivo", "0");
				callableStatement.setString("PcodigoAmelista", pedido.getCodigoAmelista());
				callableStatement.setString("PusuarioIngreso", pedido.getCedulaAmelista());
				callableStatement.setString("Pzona", pedido.getCodigoZona());
				callableStatement.setString("PmedioIngreso", "WHA");
				callableStatement.setString("CAMPAMA", pedido.getCampana());
				callableStatement.setString("CODIGO_CATALOGO", "00");
				callableStatement.setString("tipoEnvio", "1");
				callableStatement.registerOutParameter("Pmensaje", Types.VARCHAR);
				callableStatement.registerOutParameter("ID", Types.INTEGER);

				callableStatement.execute();

				String mensajeEncabezado = callableStatement.getString("Pmensaje");
				identity = callableStatement.getInt("ID");

				// Detalle
				try {
					List<Producto> itemsPedidos = pedido.getDetallePedido();

					for (Producto itemPedido : itemsPedidos) {
						callableStatement2 = connection
								.prepareCall("EXEC facturacion.Proc_Det_Grabar_Pedido_Digitado ?,?,?,?,?,?");
						callableStatement2.setLong("PidEncabezado", identity);
						callableStatement2.setString("PcodigoAmelista", pedido.getCodigoAmelista());
						callableStatement2.setString("Pproducto",  itemPedido.getCodigoProducto().trim().substring(0,5));
						callableStatement2.setString("pcantidad", itemPedido.getCantidadDigitada());
						callableStatement2.setString("CAMPAMA", pedido.getCampana());
						callableStatement2.registerOutParameter("Pmensaje", Types.VARCHAR);
						callableStatement2.execute();

						String mensajeDetalle = callableStatement2.getString("Pmensaje");

					}
				} catch (Exception e) {
					throw new Exception(e);
				} finally {
					if (callableStatement != null) {
						try {
							callableStatement.close();
						} catch (Exception exception2) {

						}
					}
				}

				connection.commit();

				if (mensajeEncabezado == null || mensajeEncabezado.equals("")) {
					mensaje = "Pedido guardado exitosamente.";
				}

			} catch (Exception e) {
				throw new Exception(e);
			} finally {

				try {
					if (resultSet != null) {
						resultSet.close();
					}
				} catch (Exception e) {
				}

				try {
					if (resultSet2 != null) {
						resultSet2.close();
					}
				} catch (Exception e) {
				}

				try {
					if (callableStatement != null) {
						callableStatement.close();
					}
				} catch (Exception e) {
				}

				try {
					if (callableStatement2 != null) {
						callableStatement2.close();
					}
				} catch (Exception e) {
				}

				try {
					if (connection != null) {
						connection.close();
					}
				} catch (Exception e) {
				}

			}

			return mensaje;
		}
	}
}
