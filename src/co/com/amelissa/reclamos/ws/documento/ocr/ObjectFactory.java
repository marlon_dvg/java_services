
package co.com.amelissa.reclamos.ws.documento.ocr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.com.amelissa.reclamos.ws.documento.ocr package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GuardarNombramientoResponse_QNAME = new QName("tns:www.amelissa.com", "guardarNombramientoResponse");
    private final static QName _GuardarPedidoResponse_QNAME = new QName("tns:www.amelissa.com", "guardarPedidoResponse");
    private final static QName _GuardarPedido_QNAME = new QName("tns:www.amelissa.com", "guardarPedido");
    private final static QName _GuardarNombramiento_QNAME = new QName("tns:www.amelissa.com", "guardarNombramiento");
    private final static QName _ReclamoException_QNAME = new QName("tns:www.amelissa.com", "ReclamoException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.com.amelissa.reclamos.ws.documento.ocr
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GuardarPedidoResponse }
     * 
     */
    public GuardarPedidoResponse createGuardarPedidoResponse() {
        return new GuardarPedidoResponse();
    }

    /**
     * Create an instance of {@link ReclamoException }
     * 
     */
    public ReclamoException createReclamoException() {
        return new ReclamoException();
    }

    /**
     * Create an instance of {@link GuardarNombramiento }
     * 
     */
    public GuardarNombramiento createGuardarNombramiento() {
        return new GuardarNombramiento();
    }

    /**
     * Create an instance of {@link GuardarPedido }
     * 
     */
    public GuardarPedido createGuardarPedido() {
        return new GuardarPedido();
    }

    /**
     * Create an instance of {@link PedidoWS }
     * 
     */
    public PedidoWS createPedidoWS() {
        return new PedidoWS();
    }

    /**
     * Create an instance of {@link NombramientoWS }
     * 
     */
    public NombramientoWS createNombramientoWS() {
        return new NombramientoWS();
    }

    /**
     * Create an instance of {@link GuardarNombramientoResponse }
     * 
     */
    public GuardarNombramientoResponse createGuardarNombramientoResponse() {
        return new GuardarNombramientoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarNombramientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "tns:www.amelissa.com", name = "guardarNombramientoResponse")
    public JAXBElement<GuardarNombramientoResponse> createGuardarNombramientoResponse(GuardarNombramientoResponse value) {
        return new JAXBElement<GuardarNombramientoResponse>(_GuardarNombramientoResponse_QNAME, GuardarNombramientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarPedidoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "tns:www.amelissa.com", name = "guardarPedidoResponse")
    public JAXBElement<GuardarPedidoResponse> createGuardarPedidoResponse(GuardarPedidoResponse value) {
        return new JAXBElement<GuardarPedidoResponse>(_GuardarPedidoResponse_QNAME, GuardarPedidoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarPedido }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "tns:www.amelissa.com", name = "guardarPedido")
    public JAXBElement<GuardarPedido> createGuardarPedido(GuardarPedido value) {
        return new JAXBElement<GuardarPedido>(_GuardarPedido_QNAME, GuardarPedido.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarNombramiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "tns:www.amelissa.com", name = "guardarNombramiento")
    public JAXBElement<GuardarNombramiento> createGuardarNombramiento(GuardarNombramiento value) {
        return new JAXBElement<GuardarNombramiento>(_GuardarNombramiento_QNAME, GuardarNombramiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReclamoException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "tns:www.amelissa.com", name = "ReclamoException")
    public JAXBElement<ReclamoException> createReclamoException(ReclamoException value) {
        return new JAXBElement<ReclamoException>(_ReclamoException_QNAME, ReclamoException.class, null, value);
    }

}
