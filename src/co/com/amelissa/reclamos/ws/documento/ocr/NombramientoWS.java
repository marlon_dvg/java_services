
package co.com.amelissa.reclamos.ws.documento.ocr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for nombramientoWS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nombramientoWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="barrio1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barrio1RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barrio1RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barrio2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barrio2RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barrio2RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barrioCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="brasier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buscaEmpleo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="camisa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="campana" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="casaPropia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cedula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cedulaCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cedulaRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="celular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="celularCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciudad1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciudad1RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciudad1RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciudad2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciudad2RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciudad2RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciudadCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoBarrio1" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoBarrio2" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="correo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="correoCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="departamento1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="departamento2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depto1RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depto1RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depto2RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depto2RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deptoCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccion1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccion1RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccion1RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccion2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccion2RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccion2RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccionCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divipola1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divipola1RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divipola1RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divipola2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divipola2RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divipola2RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divipolaCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="estadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estudia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaAfiliacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inconsistencia1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inconsistencia2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inconsistencia3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inconsistencia4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inconsistencia5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jubilado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nom1RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nom1RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nom2RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nom2RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pantalon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="panty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentesco1RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentesco1RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentesco2RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentesco2RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentescoCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poblacion1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poblacion2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poblacionCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono1Cod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono1RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono1RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono2Cod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono2RefFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono2RefPer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trabaja" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zonaTransmision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_0_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_13_17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_18_mas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_6_12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firmaAmelistaSegundaDireccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreGzSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firmaGzSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firmaPagare" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="huellaPagare" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firmaCarta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firmaAutorizaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="copiaCedula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="copiaCuentaServicios" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="credito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pagoAnticipado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="consecutivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ce" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreConyuge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ccConyuge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="banco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="corriente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ahorros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/> 
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nombramientoWS", propOrder = {
    "barrio1",
    "barrio1RefFam",
    "barrio1RefPer",
    "barrio2",
    "barrio2RefFam",
    "barrio2RefPer",
    "barrioCod",
    "brasier",
    "buscaEmpleo",
    "camisa",
    "campana",
    "casaPropia",
    "cedula",
    "cedulaCod",
    "cedulaRef",
    "celular",
    "celularCod",
    "ciudad1",
    "ciudad1RefFam",
    "ciudad1RefPer",
    "ciudad2",
    "ciudad2RefFam",
    "ciudad2RefPer",
    "ciudadCod",
    "codigoBarrio1",
    "codigoBarrio2",
    "correo",
    "correoCod",
    "departamento1",
    "departamento2",
    "depto1RefFam",
    "depto1RefPer",
    "depto2RefFam",
    "depto2RefPer",
    "deptoCod",
    "direccion1",
    "direccion1RefFam",
    "direccion1RefPer",
    "direccion2",
    "direccion2RefFam",
    "direccion2RefPer",
    "direccionCod",
    "divipola1",
    "divipola1RefFam",
    "divipola1RefPer",
    "divipola2",
    "divipola2RefFam",
    "divipola2RefPer",
    "divipolaCod",
    "documento",
    "estadoCivil",
    "estrato",
    "estudia",
    "fechaAfiliacion",
    "fechaNacimiento",
    "inconsistencia1",
    "inconsistencia2",
    "inconsistencia3",
    "inconsistencia4",
    "inconsistencia5",
    "jubilado",
    "nom1RefFam",
    "nom1RefPer",
    "nom2RefFam",
    "nom2RefPer",
    "nombreArchivo",
    "nombreCod",
    "nombreRef",
    "otro",
    "pApellido",
    "pNombre",
    "pantalon",
    "panty",
    "parentesco1RefFam",
    "parentesco1RefPer",
    "parentesco2RefFam",
    "parentesco2RefPer",
    "parentescoCod",
    "poblacion1",
    "poblacion2",
    "poblacionCod",
    "sApellido",
    "sNombre",
    "seccion",
    "sexo",
    "telefono",
    "telefono1Cod",
    "telefono1RefFam",
    "telefono1RefPer",
    "telefono2",
    "telefono2Cod",
    "telefono2RefFam",
    "telefono2RefPer",
    "trabaja",
    "zona",
    "zonaTransmision",
    "_05",
    "_1317",
    "_18Mas",
    "_612",
    "firmaAmelistaSegundaDireccion",
    "nombreGzSolicitud",
    "firmaGzSolicitud",
    "firmaPagare",
    "huellaPagare",
    "firmaCarta",
    "firmaAutorizaciones",
    "copiaCedula",
    "copiaCuentaServicios",
    "credito",
    "pagoAnticipado",
    "consecutivo",
    "cc",
    "ce",
    "nombreConyuge",
    "ccConyuge",
    "banco",
    "corriente",
    "ahorros",
    "cuenta",
    "sector"
})


public class NombramientoWS {

    protected String barrio1;
    protected String barrio1RefFam;
    protected String barrio1RefPer;
    protected String barrio2;
    protected String barrio2RefFam;
    protected String barrio2RefPer;
    protected String barrioCod;
    protected String brasier;
    protected String buscaEmpleo;
    protected String camisa;
    protected String campana;
    protected String casaPropia;
    protected String cedula;
    protected String cedulaCod;
    protected String cedulaRef;
    protected String celular;
    protected String celularCod;
    protected String ciudad1;
    protected String ciudad1RefFam;
    protected String ciudad1RefPer;
    protected String ciudad2;
    protected String ciudad2RefFam;
    protected String ciudad2RefPer;
    protected String ciudadCod;
    protected long codigoBarrio1;
    protected long codigoBarrio2;
    protected String correo;
    protected String correoCod;
    protected String departamento1;
    protected String departamento2;
    protected String depto1RefFam;
    protected String depto1RefPer;
    protected String depto2RefFam;
    protected String depto2RefPer;
    protected String deptoCod;
    protected String direccion1;
    protected String direccion1RefFam;
    protected String direccion1RefPer;
    protected String direccion2;
    protected String direccion2RefFam;
    protected String direccion2RefPer;
    protected String direccionCod;
    protected String divipola1;
    protected String divipola1RefFam;
    protected String divipola1RefPer;
    protected String divipola2;
    protected String divipola2RefFam;
    protected String divipola2RefPer;
    protected String divipolaCod;
    protected long documento;
    protected String estadoCivil;
    protected String estrato;
    protected String estudia;
    protected String fechaAfiliacion;
    protected String fechaNacimiento;
    protected String inconsistencia1;
    protected String inconsistencia2;
    protected String inconsistencia3;
    protected String inconsistencia4;
    protected String inconsistencia5;
    protected String jubilado;
    protected String nom1RefFam;
    protected String nom1RefPer;
    protected String nom2RefFam;
    protected String nom2RefPer;
    protected String nombreArchivo;
    protected String nombreCod;
    protected String nombreRef;
    protected String otro;
    @XmlElement(name = "PApellido")
    protected String pApellido;
    @XmlElement(name = "PNombre")
    protected String pNombre;
    protected String pantalon;
    protected String panty;
    protected String parentesco1RefFam;
    protected String parentesco1RefPer;
    protected String parentesco2RefFam;
    protected String parentesco2RefPer;
    protected String parentescoCod;
    protected String poblacion1;
    protected String poblacion2;
    protected String poblacionCod;
    @XmlElement(name = "SApellido")
    protected String sApellido;
    @XmlElement(name = "SNombre")
    protected String sNombre;
    protected String seccion;
    protected String sexo;
    protected String telefono;
    protected String telefono1Cod;
    protected String telefono1RefFam;
    protected String telefono1RefPer;
    protected String telefono2;
    protected String telefono2Cod;
    protected String telefono2RefFam;
    protected String telefono2RefPer;
    protected String trabaja;
    protected String zona;
    protected String zonaTransmision;
    @XmlElement(name = "_0_5")
    protected String _05;
    @XmlElement(name = "_13_17")
    protected String _1317;
    @XmlElement(name = "_18_mas")
    protected String _18Mas;
    @XmlElement(name = "_6_12")
    protected String _612;
    
    
    protected String firmaAmelistaSegundaDireccion;
    protected String nombreGzSolicitud;
    protected String firmaGzSolicitud;
    protected String firmaPagare;
    protected String huellaPagare;
    protected String firmaCarta;
    protected String firmaAutorizaciones;
    protected String copiaCedula;
    protected String copiaCuentaServicios;

    protected String credito;
    protected String pagoAnticipado;
    protected String consecutivo;
    protected String cc;
    protected String ce;
    protected String nombreConyuge;
    protected String ccConyuge;
    protected String banco;
    protected String corriente;
    protected String ahorros;
    protected String cuenta;
    protected long sector;

	public long getSector() {
		return sector;
	}

	public void setSector(long sector) {
		this.sector = sector;
	}

	public String getCredito() {
		return credito;
	}

	public void setCredito(String credito) {
		this.credito = credito;
	}

	public String getPagoAnticipado() {
		return pagoAnticipado;
	}

	public void setPagoAnticipado(String pagoAnticipado) {
		this.pagoAnticipado = pagoAnticipado;
	}

	public String getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getCe() {
		return ce;
	}

	public void setCe(String ce) {
		this.ce = ce;
	}

	public String getNombreConyuge() {
		return nombreConyuge;
	}

	public void setNombreConyuge(String nombreConyuge) {
		this.nombreConyuge = nombreConyuge;
	}

	public String getCcConyuge() {
		return ccConyuge;
	}

	public void setCcConyuge(String ccConyuge) {
		this.ccConyuge = ccConyuge;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getCorriente() {
		return corriente;
	}

	public void setCorriente(String corriente) {
		this.corriente = corriente;
	}

	public String getAhorros() {
		return ahorros;
	}

	public void setAhorros(String ahorros) {
		this.ahorros = ahorros;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
     * Gets the value of the barrio1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarrio1() {
        return barrio1;
    }

    /**
     * Sets the value of the barrio1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarrio1(String value) {
        this.barrio1 = value;
    }

    /**
     * Gets the value of the barrio1RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarrio1RefFam() {
        return barrio1RefFam;
    }

    /**
     * Sets the value of the barrio1RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarrio1RefFam(String value) {
        this.barrio1RefFam = value;
    }

    /**
     * Gets the value of the barrio1RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarrio1RefPer() {
        return barrio1RefPer;
    }

    /**
     * Sets the value of the barrio1RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarrio1RefPer(String value) {
        this.barrio1RefPer = value;
    }

    /**
     * Gets the value of the barrio2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarrio2() {
        return barrio2;
    }

    /**
     * Sets the value of the barrio2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarrio2(String value) {
        this.barrio2 = value;
    }

    /**
     * Gets the value of the barrio2RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarrio2RefFam() {
        return barrio2RefFam;
    }

    /**
     * Sets the value of the barrio2RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarrio2RefFam(String value) {
        this.barrio2RefFam = value;
    }

    /**
     * Gets the value of the barrio2RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarrio2RefPer() {
        return barrio2RefPer;
    }

    /**
     * Sets the value of the barrio2RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarrio2RefPer(String value) {
        this.barrio2RefPer = value;
    }

    /**
     * Gets the value of the barrioCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarrioCod() {
        return barrioCod;
    }

    /**
     * Sets the value of the barrioCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarrioCod(String value) {
        this.barrioCod = value;
    }

    /**
     * Gets the value of the brasier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrasier() {
        return brasier;
    }

    /**
     * Sets the value of the brasier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrasier(String value) {
        this.brasier = value;
    }

    /**
     * Gets the value of the buscaEmpleo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuscaEmpleo() {
        return buscaEmpleo;
    }

    /**
     * Sets the value of the buscaEmpleo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuscaEmpleo(String value) {
        this.buscaEmpleo = value;
    }

    /**
     * Gets the value of the camisa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCamisa() {
        return camisa;
    }

    /**
     * Sets the value of the camisa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCamisa(String value) {
        this.camisa = value;
    }

    /**
     * Gets the value of the campana property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampana() {
        return campana;
    }

    /**
     * Sets the value of the campana property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampana(String value) {
        this.campana = value;
    }

    /**
     * Gets the value of the casaPropia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCasaPropia() {
        return casaPropia;
    }

    /**
     * Sets the value of the casaPropia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCasaPropia(String value) {
        this.casaPropia = value;
    }

    /**
     * Gets the value of the cedula property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * Sets the value of the cedula property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedula(String value) {
        this.cedula = value;
    }

    /**
     * Gets the value of the cedulaCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaCod() {
        return cedulaCod;
    }

    /**
     * Sets the value of the cedulaCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaCod(String value) {
        this.cedulaCod = value;
    }

    /**
     * Gets the value of the cedulaRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaRef() {
        return cedulaRef;
    }

    /**
     * Sets the value of the cedulaRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaRef(String value) {
        this.cedulaRef = value;
    }

    /**
     * Gets the value of the celular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelular() {
        return celular;
    }

    /**
     * Sets the value of the celular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelular(String value) {
        this.celular = value;
    }

    /**
     * Gets the value of the celularCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelularCod() {
        return celularCod;
    }

    /**
     * Sets the value of the celularCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelularCod(String value) {
        this.celularCod = value;
    }

    /**
     * Gets the value of the ciudad1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad1() {
        return ciudad1;
    }

    /**
     * Sets the value of the ciudad1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad1(String value) {
        this.ciudad1 = value;
    }

    /**
     * Gets the value of the ciudad1RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad1RefFam() {
        return ciudad1RefFam;
    }

    /**
     * Sets the value of the ciudad1RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad1RefFam(String value) {
        this.ciudad1RefFam = value;
    }

    /**
     * Gets the value of the ciudad1RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad1RefPer() {
        return ciudad1RefPer;
    }

    /**
     * Sets the value of the ciudad1RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad1RefPer(String value) {
        this.ciudad1RefPer = value;
    }

    /**
     * Gets the value of the ciudad2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad2() {
        return ciudad2;
    }

    /**
     * Sets the value of the ciudad2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad2(String value) {
        this.ciudad2 = value;
    }

    /**
     * Gets the value of the ciudad2RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad2RefFam() {
        return ciudad2RefFam;
    }

    /**
     * Sets the value of the ciudad2RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad2RefFam(String value) {
        this.ciudad2RefFam = value;
    }

    /**
     * Gets the value of the ciudad2RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad2RefPer() {
        return ciudad2RefPer;
    }

    /**
     * Sets the value of the ciudad2RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad2RefPer(String value) {
        this.ciudad2RefPer = value;
    }

    /**
     * Gets the value of the ciudadCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadCod() {
        return ciudadCod;
    }

    /**
     * Sets the value of the ciudadCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadCod(String value) {
        this.ciudadCod = value;
    }

    /**
     * Gets the value of the codigoBarrio1 property.
     * 
     */
    public long getCodigoBarrio1() {
        return codigoBarrio1;
    }

    /**
     * Sets the value of the codigoBarrio1 property.
     * 
     */
    public void setCodigoBarrio1(long value) {
        this.codigoBarrio1 = value;
    }

    /**
     * Gets the value of the codigoBarrio2 property.
     * 
     */
    public long getCodigoBarrio2() {
        return codigoBarrio2;
    }

    /**
     * Sets the value of the codigoBarrio2 property.
     * 
     */
    public void setCodigoBarrio2(long value) {
        this.codigoBarrio2 = value;
    }

    /**
     * Gets the value of the correo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Sets the value of the correo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreo(String value) {
        this.correo = value;
    }

    /**
     * Gets the value of the correoCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoCod() {
        return correoCod;
    }

    /**
     * Sets the value of the correoCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoCod(String value) {
        this.correoCod = value;
    }

    /**
     * Gets the value of the departamento1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartamento1() {
        return departamento1;
    }

    /**
     * Sets the value of the departamento1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartamento1(String value) {
        this.departamento1 = value;
    }

    /**
     * Gets the value of the departamento2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartamento2() {
        return departamento2;
    }

    /**
     * Sets the value of the departamento2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartamento2(String value) {
        this.departamento2 = value;
    }

    /**
     * Gets the value of the depto1RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepto1RefFam() {
        return depto1RefFam;
    }

    /**
     * Sets the value of the depto1RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepto1RefFam(String value) {
        this.depto1RefFam = value;
    }

    /**
     * Gets the value of the depto1RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepto1RefPer() {
        return depto1RefPer;
    }

    /**
     * Sets the value of the depto1RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepto1RefPer(String value) {
        this.depto1RefPer = value;
    }

    /**
     * Gets the value of the depto2RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepto2RefFam() {
        return depto2RefFam;
    }

    /**
     * Sets the value of the depto2RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepto2RefFam(String value) {
        this.depto2RefFam = value;
    }

    /**
     * Gets the value of the depto2RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepto2RefPer() {
        return depto2RefPer;
    }

    /**
     * Sets the value of the depto2RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepto2RefPer(String value) {
        this.depto2RefPer = value;
    }

    /**
     * Gets the value of the deptoCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeptoCod() {
        return deptoCod;
    }

    /**
     * Sets the value of the deptoCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeptoCod(String value) {
        this.deptoCod = value;
    }

    /**
     * Gets the value of the direccion1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion1() {
        return direccion1;
    }

    /**
     * Sets the value of the direccion1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion1(String value) {
        this.direccion1 = value;
    }

    /**
     * Gets the value of the direccion1RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion1RefFam() {
        return direccion1RefFam;
    }

    /**
     * Sets the value of the direccion1RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion1RefFam(String value) {
        this.direccion1RefFam = value;
    }

    /**
     * Gets the value of the direccion1RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion1RefPer() {
        return direccion1RefPer;
    }

    /**
     * Sets the value of the direccion1RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion1RefPer(String value) {
        this.direccion1RefPer = value;
    }

    /**
     * Gets the value of the direccion2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion2() {
        return direccion2;
    }

    /**
     * Sets the value of the direccion2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion2(String value) {
        this.direccion2 = value;
    }

    /**
     * Gets the value of the direccion2RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion2RefFam() {
        return direccion2RefFam;
    }

    /**
     * Sets the value of the direccion2RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion2RefFam(String value) {
        this.direccion2RefFam = value;
    }

    /**
     * Gets the value of the direccion2RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion2RefPer() {
        return direccion2RefPer;
    }

    /**
     * Sets the value of the direccion2RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion2RefPer(String value) {
        this.direccion2RefPer = value;
    }

    /**
     * Gets the value of the direccionCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionCod() {
        return direccionCod;
    }

    /**
     * Sets the value of the direccionCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionCod(String value) {
        this.direccionCod = value;
    }

    /**
     * Gets the value of the divipola1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivipola1() {
        return divipola1;
    }

    /**
     * Sets the value of the divipola1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivipola1(String value) {
        this.divipola1 = value;
    }

    /**
     * Gets the value of the divipola1RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivipola1RefFam() {
        return divipola1RefFam;
    }

    /**
     * Sets the value of the divipola1RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivipola1RefFam(String value) {
        this.divipola1RefFam = value;
    }

    /**
     * Gets the value of the divipola1RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivipola1RefPer() {
        return divipola1RefPer;
    }

    /**
     * Sets the value of the divipola1RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivipola1RefPer(String value) {
        this.divipola1RefPer = value;
    }

    /**
     * Gets the value of the divipola2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivipola2() {
        return divipola2;
    }

    /**
     * Sets the value of the divipola2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivipola2(String value) {
        this.divipola2 = value;
    }

    /**
     * Gets the value of the divipola2RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivipola2RefFam() {
        return divipola2RefFam;
    }

    /**
     * Sets the value of the divipola2RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivipola2RefFam(String value) {
        this.divipola2RefFam = value;
    }

    /**
     * Gets the value of the divipola2RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivipola2RefPer() {
        return divipola2RefPer;
    }

    /**
     * Sets the value of the divipola2RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivipola2RefPer(String value) {
        this.divipola2RefPer = value;
    }

    /**
     * Gets the value of the divipolaCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivipolaCod() {
        return divipolaCod;
    }

    /**
     * Sets the value of the divipolaCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivipolaCod(String value) {
        this.divipolaCod = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     */
    public long getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     */
    public void setDocumento(long value) {
        this.documento = value;
    }

    /**
     * Gets the value of the estadoCivil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Sets the value of the estadoCivil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoCivil(String value) {
        this.estadoCivil = value;
    }

    /**
     * Gets the value of the estrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstrato() {
        return estrato;
    }

    /**
     * Sets the value of the estrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstrato(String value) {
        this.estrato = value;
    }

    /**
     * Gets the value of the estudia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstudia() {
        return estudia;
    }

    /**
     * Sets the value of the estudia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstudia(String value) {
        this.estudia = value;
    }

    /**
     * Gets the value of the fechaAfiliacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAfiliacion() {
        return fechaAfiliacion;
    }

    /**
     * Sets the value of the fechaAfiliacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAfiliacion(String value) {
        this.fechaAfiliacion = value;
    }

    /**
     * Gets the value of the fechaNacimiento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Sets the value of the fechaNacimiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaNacimiento(String value) {
        this.fechaNacimiento = value;
    }

    /**
     * Gets the value of the inconsistencia1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInconsistencia1() {
        return inconsistencia1;
    }

    /**
     * Sets the value of the inconsistencia1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInconsistencia1(String value) {
        this.inconsistencia1 = value;
    }

    /**
     * Gets the value of the inconsistencia2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInconsistencia2() {
        return inconsistencia2;
    }

    /**
     * Sets the value of the inconsistencia2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInconsistencia2(String value) {
        this.inconsistencia2 = value;
    }

    /**
     * Gets the value of the inconsistencia3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInconsistencia3() {
        return inconsistencia3;
    }

    /**
     * Sets the value of the inconsistencia3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInconsistencia3(String value) {
        this.inconsistencia3 = value;
    }

    /**
     * Gets the value of the inconsistencia4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInconsistencia4() {
        return inconsistencia4;
    }

    /**
     * Sets the value of the inconsistencia4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInconsistencia4(String value) {
        this.inconsistencia4 = value;
    }

    /**
     * Gets the value of the inconsistencia5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInconsistencia5() {
        return inconsistencia5;
    }

    /**
     * Sets the value of the inconsistencia5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInconsistencia5(String value) {
        this.inconsistencia5 = value;
    }

    /**
     * Gets the value of the jubilado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJubilado() {
        return jubilado;
    }

    /**
     * Sets the value of the jubilado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJubilado(String value) {
        this.jubilado = value;
    }

    /**
     * Gets the value of the nom1RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom1RefFam() {
        return nom1RefFam;
    }

    /**
     * Sets the value of the nom1RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom1RefFam(String value) {
        this.nom1RefFam = value;
    }

    /**
     * Gets the value of the nom1RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom1RefPer() {
        return nom1RefPer;
    }

    /**
     * Sets the value of the nom1RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom1RefPer(String value) {
        this.nom1RefPer = value;
    }

    /**
     * Gets the value of the nom2RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom2RefFam() {
        return nom2RefFam;
    }

    /**
     * Sets the value of the nom2RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom2RefFam(String value) {
        this.nom2RefFam = value;
    }

    /**
     * Gets the value of the nom2RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom2RefPer() {
        return nom2RefPer;
    }

    /**
     * Sets the value of the nom2RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom2RefPer(String value) {
        this.nom2RefPer = value;
    }

    /**
     * Gets the value of the nombreArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * Sets the value of the nombreArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreArchivo(String value) {
        this.nombreArchivo = value;
    }

    /**
     * Gets the value of the nombreCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCod() {
        return nombreCod;
    }

    /**
     * Sets the value of the nombreCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCod(String value) {
        this.nombreCod = value;
    }

    /**
     * Gets the value of the nombreRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreRef() {
        return nombreRef;
    }

    /**
     * Sets the value of the nombreRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreRef(String value) {
        this.nombreRef = value;
    }

    /**
     * Gets the value of the otro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtro() {
        return otro;
    }

    /**
     * Sets the value of the otro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtro(String value) {
        this.otro = value;
    }

    /**
     * Gets the value of the pApellido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPApellido() {
        return pApellido;
    }

    /**
     * Sets the value of the pApellido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPApellido(String value) {
        this.pApellido = value;
    }

    /**
     * Gets the value of the pNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNombre() {
        return pNombre;
    }

    /**
     * Sets the value of the pNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNombre(String value) {
        this.pNombre = value;
    }

    /**
     * Gets the value of the pantalon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPantalon() {
        return pantalon;
    }

    /**
     * Sets the value of the pantalon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPantalon(String value) {
        this.pantalon = value;
    }

    /**
     * Gets the value of the panty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPanty() {
        return panty;
    }

    /**
     * Sets the value of the panty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPanty(String value) {
        this.panty = value;
    }

    /**
     * Gets the value of the parentesco1RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentesco1RefFam() {
        return parentesco1RefFam;
    }

    /**
     * Sets the value of the parentesco1RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentesco1RefFam(String value) {
        this.parentesco1RefFam = value;
    }

    /**
     * Gets the value of the parentesco1RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentesco1RefPer() {
        return parentesco1RefPer;
    }

    /**
     * Sets the value of the parentesco1RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentesco1RefPer(String value) {
        this.parentesco1RefPer = value;
    }

    /**
     * Gets the value of the parentesco2RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentesco2RefFam() {
        return parentesco2RefFam;
    }

    /**
     * Sets the value of the parentesco2RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentesco2RefFam(String value) {
        this.parentesco2RefFam = value;
    }

    /**
     * Gets the value of the parentesco2RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentesco2RefPer() {
        return parentesco2RefPer;
    }

    /**
     * Sets the value of the parentesco2RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentesco2RefPer(String value) {
        this.parentesco2RefPer = value;
    }

    /**
     * Gets the value of the parentescoCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentescoCod() {
        return parentescoCod;
    }

    /**
     * Sets the value of the parentescoCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentescoCod(String value) {
        this.parentescoCod = value;
    }

    /**
     * Gets the value of the poblacion1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoblacion1() {
        return poblacion1;
    }

    /**
     * Sets the value of the poblacion1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoblacion1(String value) {
        this.poblacion1 = value;
    }

    /**
     * Gets the value of the poblacion2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoblacion2() {
        return poblacion2;
    }

    /**
     * Sets the value of the poblacion2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoblacion2(String value) {
        this.poblacion2 = value;
    }

    /**
     * Gets the value of the poblacionCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoblacionCod() {
        return poblacionCod;
    }

    /**
     * Sets the value of the poblacionCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoblacionCod(String value) {
        this.poblacionCod = value;
    }

    /**
     * Gets the value of the sApellido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSApellido() {
        return sApellido;
    }

    /**
     * Sets the value of the sApellido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSApellido(String value) {
        this.sApellido = value;
    }

    /**
     * Gets the value of the sNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNombre() {
        return sNombre;
    }

    /**
     * Sets the value of the sNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNombre(String value) {
        this.sNombre = value;
    }

    /**
     * Gets the value of the seccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * Sets the value of the seccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeccion(String value) {
        this.seccion = value;
    }

    /**
     * Gets the value of the sexo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * Sets the value of the sexo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSexo(String value) {
        this.sexo = value;
    }

    /**
     * Gets the value of the telefono property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Sets the value of the telefono property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Gets the value of the telefono1Cod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono1Cod() {
        return telefono1Cod;
    }

    /**
     * Sets the value of the telefono1Cod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono1Cod(String value) {
        this.telefono1Cod = value;
    }

    /**
     * Gets the value of the telefono1RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono1RefFam() {
        return telefono1RefFam;
    }

    /**
     * Sets the value of the telefono1RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono1RefFam(String value) {
        this.telefono1RefFam = value;
    }

    /**
     * Gets the value of the telefono1RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono1RefPer() {
        return telefono1RefPer;
    }

    /**
     * Sets the value of the telefono1RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono1RefPer(String value) {
        this.telefono1RefPer = value;
    }

    /**
     * Gets the value of the telefono2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono2() {
        return telefono2;
    }

    /**
     * Sets the value of the telefono2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono2(String value) {
        this.telefono2 = value;
    }

    /**
     * Gets the value of the telefono2Cod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono2Cod() {
        return telefono2Cod;
    }

    /**
     * Sets the value of the telefono2Cod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono2Cod(String value) {
        this.telefono2Cod = value;
    }

    /**
     * Gets the value of the telefono2RefFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono2RefFam() {
        return telefono2RefFam;
    }

    /**
     * Sets the value of the telefono2RefFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono2RefFam(String value) {
        this.telefono2RefFam = value;
    }

    /**
     * Gets the value of the telefono2RefPer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono2RefPer() {
        return telefono2RefPer;
    }

    /**
     * Sets the value of the telefono2RefPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono2RefPer(String value) {
        this.telefono2RefPer = value;
    }

    /**
     * Gets the value of the trabaja property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrabaja() {
        return trabaja;
    }

    /**
     * Sets the value of the trabaja property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrabaja(String value) {
        this.trabaja = value;
    }

    /**
     * Gets the value of the zona property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZona() {
        return zona;
    }

    /**
     * Sets the value of the zona property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZona(String value) {
        this.zona = value;
    }

    /**
     * Gets the value of the zonaTransmision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZonaTransmision() {
        return zonaTransmision;
    }

    /**
     * Sets the value of the zonaTransmision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZonaTransmision(String value) {
        this.zonaTransmision = value;
    }

    /**
     * Gets the value of the 05 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get05() {
        return _05;
    }

    /**
     * Sets the value of the 05 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set05(String value) {
        this._05 = value;
    }

    /**
     * Gets the value of the 1317 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get1317() {
        return _1317;
    }

    /**
     * Sets the value of the 1317 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set1317(String value) {
        this._1317 = value;
    }

    /**
     * Gets the value of the 18Mas property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get18Mas() {
        return _18Mas;
    }

    /**
     * Sets the value of the 18Mas property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set18Mas(String value) {
        this._18Mas = value;
    }

    /**
     * Gets the value of the 612 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get612() {
        return _612;
    }

    /**
     * Sets the value of the 612 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set612(String value) {
        this._612 = value;
    }

	public String getFirmaAmelistaSegundaDireccion() {
		return firmaAmelistaSegundaDireccion;
	}

	public void setFirmaAmelistaSegundaDireccion(
			String firmaAmelistaSegundaDireccion) {
		this.firmaAmelistaSegundaDireccion = firmaAmelistaSegundaDireccion;
	}

	public String getNombreGzSolicitud() {
		return nombreGzSolicitud;
	}

	public void setNombreGzSolicitud(String nombreGzSolicitud) {
		this.nombreGzSolicitud = nombreGzSolicitud;
	}

	public String getFirmaGzSolicitud() {
		return firmaGzSolicitud;
	}

	public void setFirmaGzSolicitud(String firmaGzSolicitud) {
		this.firmaGzSolicitud = firmaGzSolicitud;
	}

	public String getFirmaPagare() {
		return firmaPagare;
	}

	public void setFirmaPagare(String firmaPagare) {
		this.firmaPagare = firmaPagare;
	}

	public String getHuellaPagare() {
		return huellaPagare;
	}

	public void setHuellaPagare(String huellaPagare) {
		this.huellaPagare = huellaPagare;
	}

	public String getFirmaCarta() {
		return firmaCarta;
	}

	public void setFirmaCarta(String firmaCarta) {
		this.firmaCarta = firmaCarta;
	}

	public String getFirmaAutorizaciones() {
		return firmaAutorizaciones;
	}

	public void setFirmaAutorizaciones(String firmaAutorizaciones) {
		this.firmaAutorizaciones = firmaAutorizaciones;
	}

	public String getCopiaCedula() {
		return copiaCedula;
	}

	public void setCopiaCedula(String copiaCedula) {
		this.copiaCedula = copiaCedula;
	}

	public String getCopiaCuentaServicios() {
		return copiaCuentaServicios;
	}

	public void setCopiaCuentaServicios(String copiaCuentaServicios) {
		this.copiaCuentaServicios = copiaCuentaServicios;
	}

    
}
