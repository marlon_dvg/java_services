
package co.com.amelissa.reclamos.ws.documento.ocr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pedidoWS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pedidoWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="campana" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cedula" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inconsistencia1" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="inconsistencia2" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="inconsistencia3" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="inconsistencia4" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="inconsistencia5" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="lin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pagina" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="seccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zonaTransmision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pedidoWS", propOrder = {
    "campana",
    "cantidad",
    "cedula",
    "codigo",
    "documento",
    "fecha",
    "hora",
    "inconsistencia1",
    "inconsistencia2",
    "inconsistencia3",
    "inconsistencia4",
    "inconsistencia5",
    "lin",
    "nombreArchivo",
    "pagina",
    "seccion",
    "zona",
    "zonaTransmision"
})
public class PedidoWS {

    protected String campana;
    protected String cantidad;
    protected String cedula;
    protected String codigo;
    protected long documento;
    protected String fecha;
    protected String hora;
    protected long inconsistencia1;
    protected long inconsistencia2;
    protected long inconsistencia3;
    protected long inconsistencia4;
    protected long inconsistencia5;
    protected String lin;
    protected String nombreArchivo;
    protected String pagina;
    protected String seccion;
    protected String zona;
    protected String zonaTransmision;

    /**
     * Gets the value of the campana property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampana() {
        return campana;
    }

    /**
     * Sets the value of the campana property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampana(String value) {
        this.campana = value;
    }



    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     */
    public long getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     */
    public void setDocumento(long value) {
        this.documento = value;
    }

    /**
     * Gets the value of the fecha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Sets the value of the fecha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the hora property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHora() {
        return hora;
    }

    /**
     * Sets the value of the hora property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHora(String value) {
        this.hora = value;
    }

    /**
     * Gets the value of the inconsistencia1 property.
     * 
     */
    public long getInconsistencia1() {
        return inconsistencia1;
    }

    /**
     * Sets the value of the inconsistencia1 property.
     * 
     */
    public void setInconsistencia1(long value) {
        this.inconsistencia1 = value;
    }

    /**
     * Gets the value of the inconsistencia2 property.
     * 
     */
    public long getInconsistencia2() {
        return inconsistencia2;
    }

    /**
     * Sets the value of the inconsistencia2 property.
     * 
     */
    public void setInconsistencia2(long value) {
        this.inconsistencia2 = value;
    }

    /**
     * Gets the value of the inconsistencia3 property.
     * 
     */
    public long getInconsistencia3() {
        return inconsistencia3;
    }

    /**
     * Sets the value of the inconsistencia3 property.
     * 
     */
    public void setInconsistencia3(long value) {
        this.inconsistencia3 = value;
    }

    /**
     * Gets the value of the inconsistencia4 property.
     * 
     */
    public long getInconsistencia4() {
        return inconsistencia4;
    }

    /**
     * Sets the value of the inconsistencia4 property.
     * 
     */
    public void setInconsistencia4(long value) {
        this.inconsistencia4 = value;
    }

    /**
     * Gets the value of the inconsistencia5 property.
     * 
     */
    public long getInconsistencia5() {
        return inconsistencia5;
    }

    /**
     * Sets the value of the inconsistencia5 property.
     * 
     */
    public void setInconsistencia5(long value) {
        this.inconsistencia5 = value;
    }

    /**
     * Gets the value of the lin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLin() {
        return lin;
    }

    /**
     * Sets the value of the lin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLin(String value) {
        this.lin = value;
    }

    /**
     * Gets the value of the nombreArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * Sets the value of the nombreArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreArchivo(String value) {
        this.nombreArchivo = value;
    }

    /**
     * Gets the value of the pagina property.
     * 
     */


    /**
     * Gets the value of the seccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * Sets the value of the seccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeccion(String value) {
        this.seccion = value;
    }

    /**
     * Gets the value of the zona property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZona() {
        return zona;
    }

    /**
     * Sets the value of the zona property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZona(String value) {
        this.zona = value;
    }

    /**
     * Gets the value of the zonaTransmision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZonaTransmision() {
        return zonaTransmision;
    }

    /**
     * Sets the value of the zonaTransmision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZonaTransmision(String value) {
        this.zonaTransmision = value;
    }

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
    
    

}
